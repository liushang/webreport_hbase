
package cn.net.greenet.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


//读取配置文件（属性文件）的工具类
public class ConfigManager {
	private static ConfigManager configManager;
	//properties.load(InputStream);读取属性文件
	private static Properties properties;

	private ConfigManager(){
		String configFile="../conf/configuration.properties";
		properties=new Properties();
		InputStream in = null;
		try {
			System.out.println(new File(".").getAbsolutePath()); ;
			in= new FileInputStream(configFile);
			properties.load(in);
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(in!=null) try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static ConfigManager getInstance(){
		if(configManager==null){
			configManager=new ConfigManager();
		}
		return configManager;
	}

	public String getString(String key){
		return properties.getProperty(key);
	}
}