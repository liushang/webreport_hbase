/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.net.greenet.service;

import cn.net.greenet.dao.BaseDao;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Ambimmort
 */
public class DataStatByDay extends BaseDao{

    public void dataStatTraff() throws  SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(new Date());
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        Date today_s = dayEnd.getTime();
        dayEnd.add(dayEnd.DATE,-1);
        Date yesterday_s = dayEnd.getTime();
        
//1、 生成昨天的流量流向汇总数据 先删除数据 然后汇总
        String sql1 = " delete from traffdir_ana_day where date_format(R_StatDate,'%Y/%m/%d') =?" ;
        Object[] params = { sdf.format(yesterday_s) };
        super.executeUpdate(sql1,params);
       
        //新增昨天汇总的记录
        String sql = " insert into traffdir_ana_day  select date_format(R_StartTime,'%Y/%m/%d') startdate,SRC_AREAGROUP_ID,DEST_AREAGROUP_ID,AppType,AppID,AppNameLength, AppName, \n" +
                " sum(AppTraffic_UP)  AppTraffic_UP, sum(AppTraffic_DN)  AppTraffic_DN from  traffdirect_ana ana inner join traffdirect_log lg on ana.Traffdirect_id =lg.Traffdirect_id  where  R_StartTime >=?   and R_EndTime <=?  \n" +
                " group by startdate, SRC_AREAGROUP_ID,DEST_AREAGROUP_ID,AppType,AppID,AppNameLength, AppName  ;" ;
        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setTimestamp(1, new Timestamp(yesterday_s.getTime()));
        pst.setTimestamp(2, new Timestamp(today_s.getTime()));
        pst.executeUpdate();
 
//2、  生成WEB类流量统计的汇总数据 先删除数据 然后汇总
        String sql1_web_flow = " delete from web_flow_stat_day where date_format(R_StatDate,'%Y/%m/%d') =?" ;
        Object[] params_web_flow = { sdf.format(yesterday_s) };
        super.executeUpdate(sql1_web_flow,params_web_flow);
       
        //新增昨天汇总的记录
        String sql_web_flow = " insert into web_flow_stat_day SELECT date_format(R_StartTime,'%Y/%m/%d'),UserGroupNo , sum(Site_Num_Hit),SiteNameLength ,SiteName,SiteType ,\n" +
        "SUM(SiteHitFreq) , SUM(SiteTraffic_Up) ,SUM(SiteTraffic_Dn) FROM web_flow_stat where  R_StartTime >=?   and R_EndTime <=?  \n" +
        "GROUP BY date_format(R_StartTime,'%Y/%m/%d'),UserGroupNo , SiteNameLength ,SiteName,SiteType ; " ;
        PreparedStatement pst_web_flow = super.conn.prepareStatement(sql_web_flow);
        pst_web_flow.setTimestamp(1, new Timestamp(yesterday_s.getTime()));
        pst_web_flow.setTimestamp(2, new Timestamp(today_s.getTime()));
        pst_web_flow.executeUpdate(); 
        
//3、  生成cpsp_resser_day汇总数据 先删除数据 然后汇总
        String sql1_cpsp = " delete from cpsp_resser_day where date_format(R_StatDate,'%Y/%m/%d') =?" ;
        Object[] params_cpsp = { sdf.format(yesterday_s) };
        super.executeUpdate(sql1_cpsp ,params_cpsp );
// System.out.println("yesterday_s.getTime()======"+yesterday_s.toLocaleString());
        //新增昨天汇总的记录
        String sql_cpsp = "INSERT into cpsp_resser_day \n" +
            "select date_format(R_StartTime,'%Y/%m/%d'), cs.DestinationIP_Len , cs.DestinationIP ,cs.ProtocolType ,cs.PortNo ,\n" +
            "AppType,AppID,AppName_Length,AppName,SUM(HitFreq) from  cpsp_resserver cs where  R_StartTime >=?   and R_EndTime <=?  \n" +
            "group by date_format(R_StartTime,'%Y/%m/%d'), cs.DestinationIP_Len , cs.DestinationIP ,cs.ProtocolType ,cs.PortNo ,\n" +
            "AppType,AppID,AppName_Length,AppName ; " ;
        PreparedStatement pst_cpsp = super.conn.prepareStatement(sql_cpsp );
        pst_cpsp .setTimestamp(1, new Timestamp(yesterday_s.getTime()));
        pst_cpsp .setTimestamp(2, new Timestamp(today_s.getTime()));
        pst_cpsp .executeUpdate();     
        
        super.closeResource();
        System.out.println("today_s.getTime()======"+today_s.toLocaleString());
    }
    
    
     public static void main(String[] agrs) throws InterruptedException, SQLException, ParseException {
         
        DataStatByDay ds = new DataStatByDay();
        
        ds.dataStatTraff(); 
       
//        Calendar dayEnd = Calendar.getInstance();
//        dayEnd.setTime(new Date());
//        dayEnd.set(Calendar.HOUR_OF_DAY, 1);
//        dayEnd.set(Calendar.MINUTE, 0);
//        dayEnd.set(Calendar.SECOND, 0);
//
//        Timer  timer=new Timer();
//        MyTask myTask=new MyTask();  
//        timer.schedule(myTask, dayEnd.getTime(), 24*3600*1000); 
        
    }

//    static class MyTask extends TimerTask {  
//        public void run() {  
//            try {
//                DataStatByDay ds = new DataStatByDay();
//                ds.dataStatTraff();
//            } catch (SQLException ex) {
//                Logger.getLogger(DataStatByDay.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (ParseException ex) {
//                Logger.getLogger(DataStatByDay.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }  
//     }
     
}
