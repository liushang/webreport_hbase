/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.net.greenet.service;

import cn.net.greenet.dao.BaseDao;
import cn.net.greenet.util.MapOperate;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 *
 * @author Ambimmort
 */
public class DataService extends BaseDao {
    //获取某个网站某天的点击率  WEB类流量统计

    public JSONArray getWebFlowData(String day, String siteName, String userGroupNo) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date = sdf.parse(day);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);

        super.getConnection();
        String sql = "select  R_StartTime , R_EndTime ,sum(Site_Num_Hit)  as  Site_Num_Hit  from web_flow_stat web "
                + " inner join web_flow_log weblg on web.web_id = weblg.web_id "
                + "where  R_StartTime >=?   and R_EndTime <=?  and  SiteName = ? and UserGroupNo =?  "
                + "group by   R_StartTime , R_EndTime order by  R_StartTime , R_EndTime ";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setTimestamp(1, new Timestamp(date.getTime()));
        pst.setTimestamp(2, new Timestamp(dayEnd.getTime().getTime()));
        pst.setString(3, siteName);
        pst.setString(4, userGroupNo);

        ResultSet rs = pst.executeQuery();

        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("R_StartTime").substring(11, 16) + "--" + rs.getString("R_EndTime").substring(11, 16));
//            arr1_2.add(rs.getTimestamp("R_StartTime").getHours());
            arr1_2.add(rs.getLong("Site_Num_Hit"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1===" + arr1.toString());
        return arr1;
    }

    //获取WEB类流向分析趋势 5分钟粒度
    public JSONArray getWebFlowL2Data(String date1, String UserGNo, String AppType, String AppTraffic_filed, String devid) throws ClassNotFoundException, SQLException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_2 = sdf.parse(date1);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());
        super.getConnection();
        String sql = "SELECT ls.name,date_format(R_EndTime,'%Y-%m-%d %H:%i') R_EndTime ,sum(SiteTraffic_Up) SiteTraffic_Up, sum(SiteTraffic_Dn) SiteTraffic_Dn FROM web_flow_stat web   \n" 
                + " inner join web_flow_log weblg on web.web_id = weblg.web_id "
                + " inner join webtype_list ls on ls.Code = weblg.SiteType \n"
                + " left join dpiendpointbean dpi on dpi.masterIp = web.EquipIp \n"
                + "WHERE R_StartTime >='" + date1 + "' AND R_EndTime <='" + dayEnd_S + "' and  UserGroupNo ='" + UserGNo + "' " ;
        if (!AppType.equals("ALL")) {
            sql = sql + "and  weblg.SiteType = '" + AppType + "' " ;
        }
        if(!devid.equals("ALL")) sql = sql+ " and dpi.id ="+devid+" ";
        sql = sql + " group by  ls.name, date_format(R_EndTime,'%Y-%m-%d %H:%i') order by name,R_EndTime;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
//        pst.setString(1, date1);
//        pst.setString(2, UserGNo);        
 System.out.println("sql=====" + sql ); 
        ResultSet rs = pst.executeQuery();
        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        MapOperate mp = new MapOperate();
        while (rs.next()) {
            String AppName = rs.getString("name");
            if (flowDataMap.containsKey(AppName)) {
                Map<String, Double> dataMap = flowDataMap.get(AppName);
                String EndTime = rs.getString("R_EndTime");
                String StdEndTime = getStdMinByTime(EndTime);
                //将时间转换到最近的整5分钟
                if (dataMap.containsKey(StdEndTime)) {
                    double result = mp.add(rs.getDouble(AppTraffic_filed), dataMap.get(StdEndTime));
                    dataMap.put(StdEndTime, result);
                } else {
                    dataMap.put(StdEndTime, rs.getDouble(AppTraffic_filed));
                }
                flowDataMap.put(AppName, dataMap);
            } else {
                Map<String, Double> dataMap2 = new HashMap<String, Double>();
                dataMap2.put(getStdMinByTime(rs.getString("R_EndTime")), rs.getDouble(AppTraffic_filed));
                flowDataMap.put(AppName, dataMap2);
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        Map<String, Map<String, Double>> fillDataMap = mp.fillMapDataByMin(date1, flowDataMap);
        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(entry.getKey());
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            arr1.add(arr1_2);
        }
        System.out.println("arr1===" + arr1.toString());
        return arr1;
    }

    //获取WEB类流向分析趋势 天粒度
    public JSONArray getWebFlowL2_DData(String date1, String date2, String UserGNo, String AppType, String AppTraffic_filed, String devid) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        super.getConnection();
        String sql = "SELECT ls.name,date_format(R_StatDate,'%Y-%m-%d') R_StatDate ,sum(SiteTraffic_Up) SiteTraffic_Up, sum(SiteTraffic_Dn) SiteTraffic_Dn FROM web_flow_stat_day web   \n"
                + "inner join webtype_list ls on ls.Code = web.SiteType \n"
                + "left join dpiendpointbean dpi on dpi.masterIp = web.EquipIp \n"
                + "where R_StatDate >= ? and  R_StatDate <= ?  and  UserGroupNo = ? \n";
        if (!AppType.equals("ALL")) {
            sql = sql + "and  web.SiteType = '" + AppType + "' " ;
        }
        if(!devid.equals("ALL"))   sql = sql+ " and dpi.id ="+devid+" ";
        
        sql = sql + " group by  ls.name, date_format(R_StatDate,'%Y-%m-%d') order by name,R_StatDate ; ";
 System.out.println("sql=====" + sql ); 
        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setString(1, date1);
        pst.setString(2, date2);
        pst.setString(3, UserGNo);        
        ResultSet rs = pst.executeQuery();
        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        while (rs.next()) {
            String AppName = rs.getString("name");
            if (flowDataMap.containsKey(AppName)) {
                Map<String, Double> dataMap = flowDataMap.get(AppName);
                dataMap.put(rs.getString("R_StatDate"), rs.getDouble(AppTraffic_filed));
                flowDataMap.put(AppName, dataMap);
            } else {
                Map<String, Double> dataMap2 = new HashMap<String, Double>();
                dataMap2.put(rs.getString("R_StatDate"), rs.getDouble(AppTraffic_filed));
                flowDataMap.put(AppName, dataMap2);
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        MapOperate mp = new MapOperate();
        Map<String, Map<String, Double>> fillDataMap = mp.fillMapDataBydate(date_1, dayEnd.getTime(), flowDataMap);
        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(entry.getKey());
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            arr1.add(arr1_2);
        }
        System.out.println("arr1===" + arr1.toString());
        return arr1;
    }

    //获取WEB类流向分析占比 5分钟粒度
    public JSONArray getWebFlowPieData(String date1, String UserGNo, String AppType, String AppTraffic_filed, String devid) throws ClassNotFoundException, SQLException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_2 = sdf.parse(date1);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());
        
        super.getConnection();
        String sql = "SELECT ls.name,sum(SiteTraffic_Up) SiteTraffic_Up, sum(SiteTraffic_Dn) SiteTraffic_Dn FROM web_flow_stat web   \n"
                + " inner join web_flow_log weblg on web.web_id = weblg.web_id "
                + "inner join webtype_list ls on ls.Code = weblg.SiteType \n"
                + "left join dpiendpointbean dpi on dpi.masterIp = web.EquipIp "
                + "WHERE R_StartTime >='" + date1 + "' AND R_EndTime <='" + dayEnd_S + "' and  UserGroupNo ='" + UserGNo + "' " ;
        if(!devid.equals("ALL")) sql = sql+ " and dpi.id ="+devid+" ";
        sql = sql + " group by  ls.name order by name ; ";  
        PreparedStatement pst = super.conn.prepareStatement(sql);
//        pst.setString(1, date1);
//        pst.setString(2, UserGNo);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
         System.out.println("sql=====" + sql ); 
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("name"));
            arr1_2.add(rs.getDouble(AppTraffic_filed));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1===" + arr1.toString());
        return arr1;
    }

    //获取WEB类流向分析占比 一天粒度
    public JSONArray getWebFlowPie_DData(String date1, String date2, String UserGNo, String AppType, String AppTraffic_filed, String devid) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        super.getConnection();
        String sql = " SELECT ls.name,sum(SiteTraffic_Up) SiteTraffic_Up, sum(SiteTraffic_Dn) SiteTraffic_Dn FROM web_flow_stat_day web   \n"
                + "inner join webtype_list ls on ls.Code = web.SiteType \n"
                + " left join dpiendpointbean dpi on dpi.masterIp = web.EquipIp "
                + " where R_StatDate >= ? and  R_StatDate <= ?  and  UserGroupNo = ? " ;
        if(!devid.equals("ALL")) sql = sql+ " and dpi.id ="+devid+" ";        
               sql = sql + " group by  ls.name order by name ; ";
        super.getConnection();
        System.out.println("sql=====" + sql ); 
        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setString(1, date1);
        pst.setString(2, date2);
        pst.setString(3, UserGNo);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("name"));
            arr1_2.add(rs.getDouble(AppTraffic_filed));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1===" + arr1.toString());
        return arr1;
    }

    //获取某天某个应用的用户数  通用类流量统计
    public JSONArray getGeneralFlowData(String day, String appId, String userGroupNo) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date = sdf.parse(day);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);

        super.getConnection();
        String sql = "select  R_StartTime , R_EndTime ,sum(AppUserNum)  as  AppUserNum  from general_flow_stat "
                + "where  R_StartTime >=?   and R_EndTime <=?  and  appId = ?  and UserGroupNo =? "
                + "group by  R_StartTime , R_EndTime order by  R_StartTime , R_EndTime ";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setTimestamp(1, new Timestamp(date.getTime()));
        pst.setTimestamp(2, new Timestamp(dayEnd.getTime().getTime()));
        pst.setString(3, appId);
        pst.setString(4, userGroupNo);
 System.out.println("sql=====" + sql ); 
        ResultSet rs = pst.executeQuery();

        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("R_StartTime").substring(11, 16) + "--" + rs.getString("R_EndTime").substring(11, 16));
            arr1_2.add(rs.getLong("AppUserNum"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;
    }

    //获取App_Attack 总信息
    public JSONArray getAppAttackTotalData(String userGroupNo, String date1, String date2, String AppAttType, String devName) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());    
        super.getConnection();
        
        String sql = "select  sum(AttackNum) AttackNum , count(distinct  PUserGrougNo) PUserGrougNo , cast(sum( AttackTraffic )/1024 as decimal(18,2)) AttackTraffic \n"
                + " from  app_attack_stat  app_att   inner join attackarealog   area_log on app_att.Attack_id = area_log.Attack_id "
                + " left join dpiendpointbean dpi on dpi.masterIp = app_att.EquipIp "
                + " where Attack_StartTime >='" + date1 + "' and Attack_EndTime <='" + dayEnd_S + "' ";
        if ((!userGroupNo.equals("null")) && (!userGroupNo.equals(""))) {
            sql = sql + "and app_att.PUserGrougNo ='" + userGroupNo + "' ";
        } 
        if ((!AppAttType.equals("null")) && (!AppAttType.equals(""))) {
            sql = sql + "and app_att.AppAttackType ='" + AppAttType + "' ";
        }
        if (!devName.equals("ALL")) {
            sql = sql + "and  dpi.name ='" + devName + "' ";
        }
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();   
        JSONArray arr1 = new JSONArray();
        System.out.println("getAppAttackTotalData sql::==="+sql );
        
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            long AttackNum = rs.getLong("AttackNum");
            if (rs.getLong("PUserGrougNo") < 1) {
                AttackNum = 0;
            }
            arr1_2.add(AttackNum);
            arr1_2.add(rs.getLong("PUserGrougNo"));
            arr1_2.add(rs.getDouble("AttackTraffic"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;
    }

    //获取 PUserGroug 用户组列表
    public JSONArray getPUserGrougListData() throws ClassNotFoundException, SQLException {
        super.getConnection();
        String sql = " select   id  ,  name   from dpiusergroup  PG  order by  id;";
        PreparedStatement pst;
        pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("id"));
            arr1_2.add(rs.getString("name"));
            arr1.add(arr1_2);
        }
        super.closeResource();

        return arr1;
    }

    //获取 应用层攻击类型 列表
    public JSONArray getAppAttTypeListData() throws ClassNotFoundException, SQLException {
        super.getConnection();
        String sql = "select distinct AttackType_Code , AttackType_Name  \n"
                + "from  AttackType_List order by AttackType_Code ;";
        PreparedStatement pst;
        pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("AttackType_Code"));
            arr1_2.add(rs.getString("AttackType_Name"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;
    }

    //获取某个时间段内攻击流量趋势
    public JSONArray getAttackChartData(String PUserGrougNo, String AttackFiled, String date1, String date2, String AppAttType, String devName) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);

        super.getConnection();
        String sql = "select  date_format(Attack_StartTime,'%Y-%c-%e')  as  Attack_StartTime ,  avg(AppAttackTraffic)  as  AppAttackTraffic , sum( AppAttackRate )  as AppAttackRate "
                + " from  app_attack_stat app left join dpiendpointbean dpi on dpi.masterIp = app.EquipIp  where PUserGrougNo =? and  Attack_StartTime >=?  and  Attack_EndTime <=?  \n"
                + " and AppAttackType=? " ;
        if (!devName.equals("ALL")){
            sql = sql + " and dpi.name='" + devName + "' ";
        }
        sql =sql + " group by   date_format(Attack_StartTime,'%Y-%c-%e')  order by  Attack_StartTime ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setString(1, PUserGrougNo);
        pst.setTimestamp(2, new Timestamp(date_1.getTime()));
        pst.setTimestamp(3, new Timestamp(dayEnd.getTime().getTime()));
        pst.setString(4, AppAttType);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("Attack_StartTime"));
            arr1_2.add(rs.getDouble(AttackFiled));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1::==="+arr1.toString());
   System.out.println("sql=====" + sql ); 
        return arr1;
    }

    //获取某个时间段内攻击流量趋势  5分钟一个段落
    public JSONArray getAttackChart5MData(String PUserGrougNo, String AttackFiled, String date1, String date2, String AppAttType, String devName) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);

        super.getConnection();
        String sql = "select  date_format(Attack_EndTime,'%Y-%m-%d %H:%i') EndTime,  sum(AppAttackTraffic)  as  AppAttackTraffic , sum( AppAttackRate )  as AppAttackRate "
                + " from  app_attack_stat app left join dpiendpointbean dpi on dpi.masterIp = app.EquipIp where PUserGrougNo =? and  Attack_StartTime >=?  and  Attack_EndTime <=?  \n"
                + " and AppAttackType=? ";
        if (!devName.equals("ALL")) {
            sql = sql + " and  dpi.name ='" + devName + "' ";
        }
        sql = sql + " group by  date_format(Attack_EndTime,'%Y-%m-%d %H:%i') order by  EndTime ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setString(1, PUserGrougNo);
        pst.setTimestamp(2, new Timestamp(date_1.getTime()));
        pst.setTimestamp(3, new Timestamp(dayEnd.getTime().getTime()));
        pst.setString(4, AppAttType);
        ResultSet rs = pst.executeQuery();
    System.out.println("sql=====" + sql ); 
        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        while (rs.next()) {
            String EndTime = rs.getString("EndTime");
            String StdEndTime = EndTime;
            //将时间转换到最近的整5分钟
            SimpleDateFormat sdf_hm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date date_tmp = sdf_hm.parse(EndTime);
            int addmin = 5 - date_tmp.getMinutes() % 5;
            if (addmin < 5) {
                Date afterDate = new Date(date_tmp.getTime() + addmin * 1000 * 60);
                StdEndTime = sdf_hm.format(afterDate);
            }
            if (flowDataMap.containsKey(StdEndTime)) {
                Map<String, Double> dataMap = flowDataMap.get(StdEndTime);
                dataMap.put(EndTime, rs.getDouble(AttackFiled));
                flowDataMap.put(StdEndTime, dataMap);
            } else {
                Map<String, Double> dataMap2 = new HashMap<String, Double>();
                dataMap2.put(EndTime, rs.getDouble(AttackFiled));
                flowDataMap.put(StdEndTime, dataMap2);
            }
        }
        int sum_avg = 1;
        if (AttackFiled.equals("AppAttackRate")){
            sum_avg = 2;
        }

        MapOperate mp = new MapOperate();
        Map<String, Double> fillDataMap = mp.fillMapKeyByMin(date1, flowDataMap, sum_avg);
        super.closeResource();

        JSONArray arr1 = new JSONArray();
        JSONArray arr1_2 = new JSONArray();
        arr1_2.add(mp.getStrByMap(fillDataMap));
        arr1.add(arr1_2);
        System.out.println("arr1::==="+arr1.toString());
        return arr1;
    }

    //获取某个时间段内每个源区域攻击次数
    public JSONObject getAttackAreaData(String userGroupNo, String date1, String date2, String AppAttType,String devName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());    
        super.getConnection();
        
        String sql = "select   area_log.attackareaname ,sum(AttackNum) as AttackNum, sum(AttackTraffic) as AttackTraffic \n"
                + "from app_attack_stat  app_att   inner join attackarealog   area_log on app_att.Attack_id = area_log.Attack_id \n"
                + " left join dpiendpointbean dpi on dpi.masterIp = app_att.EquipIp "
                + " where Attack_StartTime >='" + date1 + "' and Attack_EndTime <='" + dayEnd_S + "' ";
        String sql1 = "select count(distinct area_log.attackareaname) as total_num \n"
                + "from app_attack_stat  app_att   inner join attackarealog   area_log on app_att.Attack_id = area_log.Attack_id \n"
                + " left join dpiendpointbean dpi on dpi.masterIp = app_att.EquipIp "
                + " where Attack_StartTime >='" + date1 + "' and Attack_EndTime <='" + dayEnd_S + "' ";
        
        if ((!userGroupNo.equals("null")) && (!userGroupNo.equals(""))) {
            sql = sql + "and app_att.PUserGrougNo ='" + userGroupNo + "' ";
            sql1 = sql1 + "and app_att.PUserGrougNo ='" + userGroupNo + "' ";
        } 
        if ((!AppAttType.equals("null")) && (!AppAttType.equals(""))) {
            sql = sql + "and app_att.AppAttackType ='" + AppAttType + "' ";
            sql1 = sql1 + "and app_att.AppAttackType ='" + AppAttType + "' ";
        }
        if (!devName.equals("ALL")) {
            sql = sql + "and  dpi.name ='" + devName + "' ";
            sql1 = sql1 + "and  dpi.name ='" + devName + "' ";
        }
        sql = sql + "group by  area_log.attackareaname order by attackareaname limit ?,?; ";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setInt(1, iDisplayStart);
        pst.setInt(2, iDisplayLength);
 System.out.println("sql=====" + sql ); 
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray(); 
            String areaName = rs.getString(1);
            arr1_2.add("<a href=\"#\" onclick=\"load_AreaLog('" + areaName + "')\">" + areaName +" </a>");
            arr1_2.add(rs.getString("AttackNum"));
            arr1_2.add(rs.getString("AttackTraffic"));
            arr1.add(arr1_2);
        }
        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }
        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数

        System.out.println(DataObj);
        super.closeResource();
        return DataObj;
    }

    //获取某个时间段内攻击日志详情
    public JSONObject getAttackLogDetailsData(String userGroupNo, String date1, String date2, String AppAttType,String devName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime()); 
        
        super.getConnection();
        String sql = "select dpi.name ,date_format(Attack_StartTime,'%Y-%m-%d %H:%i:%s') as Attack_StartTime, date_format(Attack_EndTime,'%Y-%m-%d %H:%i:%s') as  Attack_EndTime ,gp.name UserGroupNo , ls.AttackType_Name as AppAttackType,AppAttackTraffic, AppAttackRate,  \n"
                + "AttackAreaNum from app_attack_stat  app_att  " 
                + "left join AttackType_List ls  on app_att.AppAttackType = ls.AttackType_code "
                + "left join dpiusergroup gp on gp.id = app_att.PUserGrougNo  "
                + "left join dpiendpointbean dpi on dpi.masterIp = app_att.EquipIp where Attack_StartTime >=' " + date1 + "' and Attack_EndTime <='" + dayEnd_S + "' ";
        String sql1 = " select  count(*) as total_num  from app_attack_stat  app_att  "
                + "left join AttackType_List ls  on app_att.AppAttackType = ls.AttackType_code "
                + "left join dpiusergroup gp on gp.id = app_att.PUserGrougNo  "
                + "left join dpiendpointbean dpi on dpi.masterIp = app_att.EquipIp where Attack_StartTime >=' " + date1 + "' and Attack_EndTime <='" + dayEnd_S + "' ";
        if ((!userGroupNo.equals("null")) && (!userGroupNo.equals(""))) {
            sql = sql + "and app_att.PUserGrougNo ='" + userGroupNo + "' ";
            sql1 = sql1 + "and app_att.PUserGrougNo ='" + userGroupNo + "' ";
        } 
        if ((!AppAttType.equals("null")) && (!AppAttType.equals(""))) {
            sql = sql + "and app_att.AppAttackType ='" + AppAttType + "' ";
            sql1 = sql1 + "and app_att.AppAttackType ='" + AppAttType + "' ";
        }
        if (!devName.equals("ALL")) {
            sql = sql + "and  dpi.name ='" + devName + "' ";
            sql1 = sql1 + "and  dpi.name ='" + devName + "' ";
        }
        sql = sql + " order by  Attack_StartTime ,Attack_EndTime limit "+ iDisplayStart + " ," + iDisplayLength ; 
     System.out.println("sql=====" + sql );      
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("name"));
            arr1_2.add(rs.getString("Attack_StartTime"));
            arr1_2.add(rs.getString("Attack_EndTime"));
            arr1_2.add(rs.getString("UserGroupNo"));
            arr1_2.add(rs.getString("AppAttackType"));
            arr1_2.add(rs.getString("AppAttackTraffic"));
            arr1_2.add(rs.getString("AppAttackRate"));
            arr1_2.add(rs.getInt("AttackAreaNum"));
            arr1.add(arr1_2);
        }
        
        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }

        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数
        System.out.println(DataObj);
        super.closeResource();
        return DataObj;
    }

    //获取某个时间段内攻击源日志详情
    public JSONObject getAttackAreaLogData(String userGroupNo, String date1, String date2, String AppAttType,String devName,String areaName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime()); 

        super.getConnection();
        String sql = "select dpi.name ,date_format(Attack_StartTime,'%Y-%m-%d %H:%i:%s') as Attack_StartTime, date_format(Attack_EndTime,'%Y-%m-%d %H:%i:%s') as  Attack_EndTime ,gp.name UserGroupNo , ls.AttackType_Name as AppAttackType,  \n"
                + "AttackAreaName,AttackNum,AttackTraffic,SourceIP_Num from app_attack_stat  app_att "
                + "inner join attackarealog att_lg on app_att.Attack_id = att_lg.Attack_id " 
                + "inner join AttackType_List ls  on app_att.AppAttackType = ls.AttackType_code "
                + "left join dpiusergroup gp on gp.id = app_att.PUserGrougNo  "
                + "left join dpiendpointbean dpi on dpi.masterIp = app_att.EquipIp where Attack_StartTime >=' " + date1 + "' and Attack_EndTime <='" + dayEnd_S + "' AND att_lg.AttackAreaName='" + areaName + "'" ;
        String sql1 = " select  count(*) as total_num  from app_attack_stat  app_att  "
                + "inner join attackarealog att_lg on app_att.Attack_id = att_lg.Attack_id " 
                + "inner join AttackType_List ls  on app_att.AppAttackType = ls.AttackType_code "
                + "left join dpiusergroup gp on gp.id = app_att.PUserGrougNo  "
                + "left join dpiendpointbean dpi on dpi.masterIp = app_att.EquipIp where Attack_StartTime >=' " + date1 + "' and Attack_EndTime <='" + dayEnd_S + "' AND att_lg.AttackAreaName='" + areaName + "'" ;
        if ((!userGroupNo.equals("null")) && (!userGroupNo.equals(""))) {
            sql = sql + "and app_att.PUserGrougNo ='" + userGroupNo + "' ";
            sql1 = sql1 + "and app_att.PUserGrougNo ='" + userGroupNo + "' ";
        } 
        if ((!AppAttType.equals("null")) && (!AppAttType.equals(""))) {
            sql = sql + "and app_att.AppAttackType ='" + AppAttType + "' ";
            sql1 = sql1 + "and app_att.AppAttackType ='" + AppAttType + "' ";
        }
        if (!devName.equals("ALL")) {
            sql = sql + "and  dpi.name ='" + devName + "' ";
            sql1 = sql1 + "and  dpi.name ='" + devName + "' ";
        }
        sql = sql + " order by  Attack_StartTime ,Attack_EndTime limit "+ iDisplayStart + " ," + iDisplayLength ; 
         
        PreparedStatement pst = super.conn.prepareStatement(sql);
   System.out.println("sql=====" + sql ); 
        
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("name"));
            arr1_2.add(rs.getString("Attack_StartTime"));
            arr1_2.add(rs.getString("Attack_EndTime"));
            arr1_2.add(rs.getString("UserGroupNo"));
            arr1_2.add(rs.getString("AppAttackType"));  
            arr1_2.add(rs.getString("AttackAreaName"));
            arr1_2.add(rs.getString("AttackNum"));
            arr1_2.add(rs.getString("AttackTraffic"));
            arr1_2.add(rs.getString("SourceIP_Num"));
            arr1.add(arr1_2);
        }
        
        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }

        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数
        System.out.println(DataObj);
        super.closeResource();
        return DataObj;
    }
    
    //获取某个时间段内通用流量详情
    public JSONObject getGeneralFlowDetailsData(String userGroupNo, String date1, String date2, String appType, String appName, String devName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());
        
        super.getConnection();

        String sql = "select dpi.name , date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') as R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') as  R_EndTime ,gp.name UserGroupNo,big.typeName AppType,AppName, AppUserNum, \n"
                + " AppTraffic_Up , AppTraffic_Dn ,AppPacketsNum , AppSessionsNum , AppNewSessionNum from general_flow_stat  ana  "
                + " inner join apptype big on big.id =ana.AppType left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                + " left join dpiusergroup gp on gp.id = ana.UserGroupNo where R_StartTime >='" + date1 + "' and R_EndTime <='" + dayEnd_S + "' ";
        if (!appType.equals("ALL")) {
            sql = sql + "and  ana.AppType ='" + appType + "' ";
            if (!appName.equals("ALL")) {
                sql = sql + "and  ana.AppName ='" + appName + "' ";
            }
        }
        if (!devName.equals("ALL")) {
            sql = sql + "and  dpi.name ='" + devName + "' ";
        }
        if ((!userGroupNo.equals("null")) && (!userGroupNo.equals(""))) {
            sql = sql + "and ana.UserGroupNo ='" + userGroupNo + "' ";
        }        
        sql = sql + " limit " + iDisplayStart + ","+ iDisplayLength ;
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        System.out.println("sql=====" + sql );
        
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("name"));
            arr1_2.add(rs.getString("R_StartTime"));
            arr1_2.add(rs.getString("R_EndTime"));
            arr1_2.add(rs.getString("UserGroupNo"));
            arr1_2.add(rs.getString("AppType"));
            arr1_2.add(rs.getString("AppName"));
            arr1_2.add(rs.getString("AppUserNum"));
            arr1_2.add(rs.getString("AppTraffic_Up"));
            arr1_2.add(rs.getString("AppTraffic_Dn"));
            arr1_2.add(rs.getInt("AppPacketsNum"));
            arr1_2.add(rs.getInt("AppSessionsNum"));
            arr1_2.add(rs.getInt("AppNewSessionNum"));
            arr1.add(arr1_2);
        }
  
        String sql1 = "select count(*) as total_num from general_flow_stat  ana  "
                + " inner join apptype big on big.id =ana.AppType left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                + " left join dpiusergroup gp on gp.id = ana.UserGroupNo where R_StartTime >='" + date1 + "' and R_EndTime <='" + dayEnd_S + "' ";
        if (!appType.equals("ALL")) {
            sql1 = sql1 + "and  ana.AppType ='" + appType + "' ";
            if (!appName.equals("ALL")) {
                sql1 = sql1 + "and  ana.AppName ='" + appName + "' ";
            }
        }
        if (!devName.equals("ALL")) {
            sql1 = sql1 + "and  dpi.name ='" + devName + "' ";
        }
        if ((!userGroupNo.equals("null")) && (!userGroupNo.equals(""))) {
            sql1 = sql1 + "and ana.UserGroupNo ='" + userGroupNo + "' ";
        }

        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }

        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数

        System.out.println(DataObj);
        super.closeResource();
        return DataObj;
    }

    //获取某个时间段内通用流量详情
    public JSONObject getGeneralFlow_SQL(String reportName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {
     
        super.getConnection();
        String sql = "";
        String sql1 = "select count(*) as total_num from " ;
        if("traffic".equals(reportName)){
            sql = "select date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') as R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') as  R_EndTime,SRC_AREAGROUP_ID,DEST_AREAGROUP_ID,App_Num from  traffdirect_ana ";
            sql1 = sql1 + "traffdirect_ana";
        }else if("general".equals(reportName)){
            sql = "select date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') as R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') as  R_EndTime, UserGroupNo, AppType, AppID, AppNameLength, AppName, AppUserNum, AppTraffic_Up, AppTraffic_Dn, AppPacketsNum, AppSessionsNum, AppNewSessionNum from  general_flow_stat ";
            sql1 = sql1 + "general_flow_stat";
        }else if("web".equals(reportName)){
            sql = "select date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') as R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') as  R_EndTime,UserGroupNo,Site_Num_Hit from web_flow_stat ";
            sql1 = sql1 + "web_flow_stat";
        }else if("illegal".equals(reportName)){
            sql = "select date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') as R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') as  R_EndTime, NodeIP_Length, NodeIP, NodeInTraffic, NodeOutTraffic, CP_Length, CP from illegal_route_detect ";
            sql1 = sql1 + "illegal_route_detect";
        }else if("visit".equals(reportName)){
            sql = "select date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') as R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') as  R_EndTime, AppType, AppID, AppNameLength, AppName, UserNum from visit_app_user_stat ";
            sql1 = sql1 + "visit_app_user_stat";
        }else if("app_attack".equals(reportName)){
            sql = "select date_format(Attack_StartTime,'%Y-%m-%d %H:%i:%s') as R_StartTime, date_format(Attack_EndTime,'%Y-%m-%d %H:%i:%s') as  R_EndTime, PUserGrougNo, AppAttackType, AppAttackTraffic, AppAttackRate, AttackAreaNum from app_attack_stat ";
            sql1 = sql1 + "app_attack_stat";
        }else if("cpsp".equals(reportName)){
            sql = "select date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') as R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') as  R_EndTime, DestinationIP_Len, DestinationIP, ProtocolType, PortNo, AppType, AppID, AppName_Length, AppName, HitFreq from cpsp_resserver ";
            sql1 = sql1 + "cpsp_resserver";
        }else if("voip".equals(reportName)){
            sql = "select date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') as R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') as  R_EndTime, UserGroupNo, Rtp_Media_Traffic, Other_Media_Traffic, H323_Sig_Traffic, SIP_Sig_Traffic, MGCP_Sig_Traffic, VoIPGWNum, VoIPGWKeeperNum from voip_flow_stat ";
            sql1 = sql1 + "voip_flow_stat";
        }else if("oneton".equals(reportName)){
            sql = "select date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') as R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') as  R_EndTime, UserAccount, UserIP, QQidcnt, IPNATcnt, Cookiecnt, Devcnt, OScnt from oneton_det_keyfield ";
            sql1 = sql1 + "oneton_det_keyfield";
        }
        String sql_log = sql;     
        sql_log = sql_log + " limit " + iDisplayStart + ","+ iDisplayLength ;
        PreparedStatement pst = super.conn.prepareStatement(sql_log);
        ResultSet rs = pst.executeQuery();
        System.out.println("sql_log=====" + sql_log );
        JSONArray arr1 = new JSONArray();
        if("traffic".equals(reportName)){
            while (rs.next()) {
                JSONArray arr1_2 = new JSONArray();
                arr1_2.add(rs.getString("R_StartTime"));
                arr1_2.add(rs.getString("R_EndTime"));
                arr1_2.add(rs.getString(3));
                arr1_2.add(rs.getString(4));
                arr1_2.add(rs.getString(5));
                arr1.add(arr1_2);
            }
        }else if("general".equals(reportName)){
            while (rs.next()) {
                JSONArray arr1_2 = new JSONArray();
                arr1_2.add(rs.getString("R_StartTime"));
                arr1_2.add(rs.getString("R_EndTime"));
                arr1_2.add(rs.getString("UserGroupNo"));
                arr1_2.add(rs.getString("AppType"));
                arr1_2.add(rs.getString("AppID"));
                arr1_2.add(rs.getString("AppNameLength"));
                arr1_2.add(rs.getString("AppName"));
                arr1_2.add(rs.getString("AppUserNum"));
                arr1_2.add(rs.getString("AppTraffic_Up"));
                arr1_2.add(rs.getString("AppTraffic_Dn"));
                arr1_2.add(rs.getInt("AppPacketsNum"));
                arr1_2.add(rs.getInt("AppSessionsNum"));
                arr1_2.add(rs.getInt("AppNewSessionNum"));
                arr1.add(arr1_2);
            }
        }else if("web".equals(reportName)){
            while (rs.next()) {
                JSONArray arr1_2 = new JSONArray();
                arr1_2.add(rs.getString("R_StartTime"));
                arr1_2.add(rs.getString("R_EndTime"));
                arr1_2.add(rs.getString(3));
                arr1_2.add(rs.getString(4));
                arr1.add(arr1_2);
            }
        }else if("illegal".equals(reportName)){
            while (rs.next()) {
                JSONArray arr1_2 = new JSONArray();
                arr1_2.add(rs.getString("R_StartTime"));
                arr1_2.add(rs.getString("R_EndTime"));
                arr1_2.add(rs.getString(3));
                arr1_2.add(rs.getString(4));
                arr1_2.add(rs.getString(5));
                arr1_2.add(rs.getString(6));
                arr1_2.add(rs.getString(7));
                arr1_2.add(rs.getString(8));
                arr1.add(arr1_2);
            }
        }else if("visit".equals(reportName)){
            while (rs.next()) {
                JSONArray arr1_2 = new JSONArray();
                arr1_2.add(rs.getString("R_StartTime"));
                arr1_2.add(rs.getString("R_EndTime"));
                arr1_2.add(rs.getString(3));
                arr1_2.add(rs.getString(4));
                arr1_2.add(rs.getString(5));
                arr1_2.add(rs.getString(6));
                arr1_2.add(rs.getString(7));
                arr1.add(arr1_2);
            }
        }else if("app_attack".equals(reportName)){
            while (rs.next()) {
                JSONArray arr1_2 = new JSONArray();
                arr1_2.add(rs.getString(1));
                arr1_2.add(rs.getString(2));
                arr1_2.add(rs.getString(3));
                arr1_2.add(rs.getString(4));
                arr1_2.add(rs.getString(5));
                arr1_2.add(rs.getString(6));
                arr1_2.add(rs.getString(7));
                arr1.add(arr1_2);
            }
        }else if("cpsp".equals(reportName)){
            while (rs.next()) {
                JSONArray arr1_2 = new JSONArray();
                arr1_2.add(rs.getString("R_StartTime"));
                arr1_2.add(rs.getString("R_EndTime"));
                arr1_2.add(rs.getString(3));
                arr1_2.add(rs.getString(4));
                arr1_2.add(rs.getString(5));
                arr1_2.add(rs.getString(6));
                arr1_2.add(rs.getString(7));
                arr1_2.add(rs.getString(8));
                arr1_2.add(rs.getString(9));
                arr1_2.add(rs.getString(10));
                arr1_2.add(rs.getString(11));
                arr1.add(arr1_2);
            }
        }else if("voip".equals(reportName)){
            while (rs.next()) {
                JSONArray arr1_2 = new JSONArray();
                arr1_2.add(rs.getString("R_StartTime"));
                arr1_2.add(rs.getString("R_EndTime"));
                arr1_2.add(rs.getString(3));
                arr1_2.add(rs.getString(4));
                arr1_2.add(rs.getString(5));
                arr1_2.add(rs.getString(6));
                arr1_2.add(rs.getString(7));
                arr1_2.add(rs.getString(8));
                arr1_2.add(rs.getString(9));
                arr1_2.add(rs.getString(10));
                arr1.add(arr1_2);
            }
        }else if("oneton".equals(reportName)){
            while (rs.next()) {
                JSONArray arr1_2 = new JSONArray();
                arr1_2.add(rs.getString("R_StartTime"));
                arr1_2.add(rs.getString("R_EndTime"));
                arr1_2.add(rs.getString(3));
                arr1_2.add(rs.getString(4));
                arr1_2.add(rs.getString(5));
                arr1_2.add(rs.getString(6));
                arr1_2.add(rs.getString(7));
                arr1_2.add(rs.getString(8));
                arr1_2.add(rs.getString(9));
                arr1.add(arr1_2);
            }
        };

        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }
        
        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数

        System.out.println(DataObj);
        super.closeResource();
        return DataObj;
    }
    
    //获取某个时间段内某个组各类应用流量占比
    public JSONArray getGenFlowPieData(String date1, String date2, String userGroupNo, String appType, String appName, String filed, String devName) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());
        String backName = "ana.AppName ";
        if (appType.equals("ALL")) {
            backName = "big.typeName ";
        }
        super.getConnection();
        String sql = "select " + backName + " AppName ,sum(AppTraffic_UP) AppTraffic_UP, sum(AppTraffic_DN) AppTraffic_DN FROM  general_flow_stat ana "
                + " inner join apptype big on big.id =ana.AppType left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                + " where R_StartTime >='" + date1 + "' and R_EndTime <='" + dayEnd_S + "' ";
         if (!appType.equals("ALL")) {
            sql = sql + "and  ana.AppType ='" + appType + "' ";
            if (!appName.equals("ALL")) {
                sql = sql + "and  ana.AppName ='" + appName + "' ";
            }
        }
        if (!devName.equals("ALL")) {
            sql = sql + "and  dpi.name ='" + devName + "' ";
        }
        if ((!userGroupNo.equals("null")) && (!userGroupNo.equals(""))) {
            sql = sql + "and ana.UserGroupNo ='" + userGroupNo + "' ";
        }

        sql = sql + " group by " + backName ;
        System.out.println(sql);
        
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("AppName"));
            arr1_2.add(rs.getDouble(filed));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;
    }

    public JSONArray getWlanTop10Data(String date1) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date1);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());    
        super.getConnection();
        
        String sql = " select DeviceType, count(*) as phnum from wlanreport wl "
                + " where AccessTime >='" + date1 + "' and AccessTime <='" + dayEnd_S + "' ";
        sql = sql + " group by DeviceType order by phnum desc limit 0,10 ;";
        System.out.println(sql);
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("DeviceType"));
            arr1_2.add(rs.getInt("phnum"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("getWlanTop10Data====================="+arr1.toString());
        return arr1;
    }
    
    public JSONArray getGenFlowTop10Data(String date1, String date2, String userGroupNo, String appType, String appName, String updown, String devName) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());    
        super.getConnection();
        
        String sql = "select big.typeName bigName , ana.AppName ,sum(AppTraffic_UP) AppTraffic_UP, sum(AppTraffic_DN) AppTraffic_DN from  general_flow_stat ana "
                + " inner join apptype big on big.id =ana.AppType left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                + " where R_StartTime >='" + date1 + "' and R_EndTime <='" + dayEnd_S + "' ";
        if (!appType.equals("ALL")) {
            sql = sql + "and  ana.AppType ='" + appType + "' ";
            if (!appName.equals("ALL")) {
                sql = sql + "and  ana.AppName ='" + appName + "' ";
            }
        }
        if (!devName.equals("ALL")) {
            sql = sql + "and  dpi.name ='" + devName + "' ";
        }
        if ((!userGroupNo.equals("null")) && (!userGroupNo.equals(""))) {
            sql = sql + "and ana.UserGroupNo ='" + userGroupNo + "' ";
        }
        sql = sql + " group by big.typeName,ana.AppName order by  " + updown + " desc limit 0,10 ; ";
        System.out.println(sql);
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("bigName"));
            arr1_2.add(rs.getString("AppName"));
            arr1_2.add(rs.getDouble(updown));
            arr1.add(arr1_2);
        }
        super.closeResource();
//        System.out.println("getGenFlowTop10Dataarr1====================="+arr1.toString());
        return arr1;
    }

    //获取某个时间段内某个组各类应用流量趋势
    public JSONArray getGenFlowL2Data(String date1, String date2, String timeType, String userGroupNo, String appType, String appName, String field, String devName) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());

        super.getConnection();
        String backName = "ana.AppName ";
        if (appType.equals("ALL")) {
            backName = "big.typeName ";
        }
        String backTime = "date_format(R_StartTime,'%Y-%m-%d')";
        if (timeType.equals("0")) {
            backTime = "date_format(R_EndTime,'%Y-%m-%d %H:%i')";
        }

        String sql = "select " + backName + " AppName, " + backTime + " R_EndTime  ,sum(AppTraffic_UP) AppTraffic_UP, sum(AppTraffic_DN) AppTraffic_DN,SUM(AppNewSessionNum) AppNewSessionNum  from  general_flow_stat ana "
                + " inner join apptype big on big.id =ana.AppType left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                + " where R_StartTime >='" + date1 + "' and R_EndTime <='" + dayEnd_S + "' ";

        if (!appType.equals("ALL")) {
            sql = sql + "and  ana.AppType ='" + appType + "' ";
            if (!appName.equals("ALL")) {
                sql = sql + "and  ana.AppName ='" + appName + "' ";
            }
        }
        if (!devName.equals("ALL")) {
            sql = sql + "and  dpi.name ='" + devName + "' ";
        }
        if ((!userGroupNo.equals("null")) && (!userGroupNo.equals(""))) {
            sql = sql + "and ana.UserGroupNo ='" + userGroupNo + "' ";
        }

        sql = sql + " group by " + backName + ", " + backTime + " order by  AppName,R_EndTime ;";
        System.out.println(sql);
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        while (rs.next()) {
            String AppName = rs.getString("AppName");
            //粒度为5分钟
            if (timeType.equals("0")) {
                if (flowDataMap.containsKey(AppName)) {
                    Map<String, Double> dataMap = flowDataMap.get(AppName);
                    String EndTime = rs.getString("R_EndTime");
                    String StdEndTime = getStdMinByTime(EndTime);
                    //将时间转换到最近的整5分钟
                    if (dataMap.containsKey(StdEndTime)) {
                        dataMap.put(StdEndTime, rs.getDouble(field) + dataMap.get(StdEndTime));
                    } else {
                        dataMap.put(StdEndTime, rs.getDouble(field));
                    }
                    flowDataMap.put(AppName, dataMap);
                } else {
                    Map<String, Double> dataMap2 = new HashMap<String, Double>();
                    dataMap2.put(getStdMinByTime(rs.getString("R_EndTime")), rs.getDouble(field));
                    flowDataMap.put(AppName, dataMap2);
                }
            } else {  //粒度为天
                if (flowDataMap.containsKey(AppName)) {
                    Map<String, Double> dataMap = flowDataMap.get(AppName);
                    dataMap.put(rs.getString("R_EndTime"), rs.getDouble(field));
                    flowDataMap.put(AppName, dataMap);
                } else {
                    Map<String, Double> dataMap2 = new HashMap<String, Double>();
                    dataMap2.put(rs.getString("R_EndTime"), rs.getDouble(field));
                    flowDataMap.put(AppName, dataMap2);
                }
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        MapOperate mp = new MapOperate();
        Map<String, Map<String, Double>> fillDataMap = new HashMap<String, Map<String, Double>>();
        if (timeType.equals("0")) {   //粒度为5分钟       
            fillDataMap = mp.fillMapDataByMin(date1, flowDataMap);
        } else {
            fillDataMap = mp.fillMapDataBydate(date_1, dayEnd.getTime(), flowDataMap);
        }
        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(entry.getKey());
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            arr1.add(arr1_2);
        }
        System.out.println("arr1:::==" + arr1.toString());
        return arr1;
    }

    //将时间转换到最近的整5分钟
    public String getStdMinByTime(String EndTime) throws ParseException {
        String StdEndTime = EndTime;
        SimpleDateFormat sdf_hm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date_tmp = sdf_hm.parse(EndTime);
        int addmin = 5 - date_tmp.getMinutes() % 5;
        if (addmin < 5) {
            Date afterDate = new Date(date_tmp.getTime() + addmin * 1000 * 60);
            StdEndTime = sdf_hm.format(afterDate);
        }
        return StdEndTime;
    }

    //获取CP/SP资源服务器分析趋势 天粒度
    public JSONArray getCpSpResL2_DData(String date1, String date2, String ProtType, String AppType, String SM_AppType, String devid) throws ClassNotFoundException, SQLException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        super.getConnection();

        String sql = "";
        if (AppType.equals("ALL")) {
            sql = "SELECT big.typename AppName, date_format(R_StatDate,'%Y-%m-%d') R_StatDate ,sum(HitFreq)  HitFreq FROM cpsp_resser_day cp \n"
                    + "inner join apptype big on big.id =cp.AppType  \n"
                    + "left join dpiendpointbean dpi on dpi.masterIp = cp.EquipIp "
                    + "WHERE R_StatDate >= ? and  R_StatDate <= ? AND ProtocolType=? \n" ;
                    if (!devid.equals("ALL")) {
                        sql = sql + "and  dpi.id =" + devid + " ";
                    }
                    sql = sql + " group by big.typename, date_format(R_StatDate,'%Y-%m-%d')  order by  AppName,R_StatDate ; ";
        } else {
            sql = "SELECT cp.AppName,date_format(R_StatDate,'%Y-%m-%d') R_StatDate , sum(HitFreq)  HitFreq FROM cpsp_resser_day cp \n"
                    + "INNER JOIN apptype big on big.id =cp.AppType  \n"
                    + "left join dpiendpointbean dpi on dpi.masterIp = cp.EquipIp "
                    + " WHERE R_StatDate >= ? and  R_StatDate <= ? AND ProtocolType=? AND cp.AppType = ? ";
            if (!SM_AppType.equals("ALL")){
                sql = sql + " AND cp.AppName =? ";
            }
            if (!devid.equals("ALL")){
                sql = sql + " and dpi.id =" + devid + " ";
            }
            sql = sql + " group by  cp.AppName, date_format(R_StatDate,'%Y-%m-%d') order by  AppName,R_StatDate; ";
        }

        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setString(1, date1);
        pst.setString(2, date2);
        pst.setString(3, ProtType);
        if (!AppType.equals("ALL")) {
            pst.setString(4, AppType);
            if (!SM_AppType.equals("ALL")) {
                pst.setString(5, SM_AppType);
            }
        }
        ResultSet rs = pst.executeQuery();
        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        while (rs.next()) {
            String AppName = rs.getString("AppName");
            if (flowDataMap.containsKey(AppName)) {
                Map<String, Double> dataMap = flowDataMap.get(AppName);
                dataMap.put(rs.getString("R_StatDate"), rs.getDouble("HitFreq"));
                flowDataMap.put(AppName, dataMap);
            } else {
                Map<String, Double> dataMap2 = new HashMap<String, Double>();
                dataMap2.put(rs.getString("R_StatDate"), rs.getDouble("HitFreq"));
                flowDataMap.put(AppName, dataMap2);
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        MapOperate mp = new MapOperate();
        Map<String, Map<String, Double>> fillDataMap = mp.fillMapDataBydate(date_1, dayEnd.getTime(), flowDataMap);
        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()){
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(entry.getKey());
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            arr1.add(arr1_2);
        }
        System.out.println("arr1===" + arr1.toString());
        return arr1;
    }

    //获取CP/SP资源服务器分析趋势 5分钟粒度
    public JSONArray getCpSpResL2Data(String date1, String ProtType, String AppType, String SM_AppType, String devid) throws ClassNotFoundException, SQLException, ParseException {

        super.getConnection();
        String sql = "";
        if (AppType.equals("ALL")) {
            sql = " SELECT big.typename AppName, date_format(R_EndTime,'%Y-%m-%d %H:%i') R_EndTime  ,sum(HitFreq)  HitFreq FROM cpsp_resserver cp \n"
                    + " inner join apptype big on big.id =cp.AppType  \n" 
                    + " left join dpiendpointbean dpi on dpi.masterIp = cp.EquipIp  \n"
                    + " WHERE date_format(R_StartTime,'%Y/%m/%d') =?  AND ProtocolType=? " ;
            if (!devid.equals("ALL")) {
                sql = sql + " and  dpi.id =" + devid + " ";
            }
            sql= sql + " group by big.typename, date_format(R_EndTime,'%Y-%m-%d %H:%i') order by AppName,R_EndTime ; ";
        } else {
            sql = "SELECT cp.AppName, date_format(R_EndTime,'%Y-%m-%d %H:%i') R_EndTime , sum(HitFreq)  HitFreq FROM cpsp_resserver cp \n"
                    + "INNER JOIN apptype big on big.id =cp.AppType  \n"
                    + "left join dpiendpointbean dpi on dpi.masterIp = cp.EquipIp \n"
                    + "WHERE date_format(R_StartTime,'%Y/%m/%d') =?  AND ProtocolType=? AND cp.AppType = ? ";
            if (!SM_AppType.equals("ALL")) {
                sql = sql + "AND cp.AppName = '" + SM_AppType + "' ";
            }
            if (!devid.equals("ALL")) {
                sql = sql + " and  dpi.id =" + devid + " ";
            }
            sql = sql + " group by  cp.AppName, date_format(R_EndTime,'%Y-%m-%d %H:%i') order by AppName,R_EndTime; ";
        }
        System.out.println("sql=====" + sql );
        
        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setString(1, date1);
        pst.setString(2, ProtType);
        if (!AppType.equals("ALL")) {
            pst.setString(3, AppType);
        }
        ResultSet rs = pst.executeQuery();
        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        MapOperate mp = new MapOperate();
        while (rs.next()) {
            String AppName = rs.getString("AppName");
            if (flowDataMap.containsKey(AppName)) {
                Map<String, Double> dataMap = flowDataMap.get(AppName);
                String EndTime = rs.getString("R_EndTime");
                String StdEndTime = getStdMinByTime(EndTime);
                //将时间转换到最近的整5分钟
                if (dataMap.containsKey(StdEndTime)) {
                    double result = mp.add(rs.getDouble("HitFreq"), dataMap.get(StdEndTime));
                    dataMap.put(StdEndTime, result);
                } else {
                    dataMap.put(StdEndTime, rs.getDouble("HitFreq"));
                }
                flowDataMap.put(AppName, dataMap);
            } else {
                Map<String, Double> dataMap2 = new HashMap<String, Double>();
                dataMap2.put(getStdMinByTime(rs.getString("R_EndTime")), rs.getDouble("HitFreq"));
                flowDataMap.put(AppName, dataMap2);
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();

        Map<String, Map<String, Double>> fillDataMap = mp.fillMapDataByMin(date1, flowDataMap);
        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(entry.getKey());
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            arr1.add(arr1_2);
        }
        System.out.println("arr1===" + arr1.toString());
        return arr1;
    }

    //获取CP/SP资源服务器请求占比 5分钟粒度
    public JSONArray getCpSpResPieData(String date1, String ProtType, String AppType, String SM_AppType, String devid) throws ClassNotFoundException, SQLException, ParseException {

        super.getConnection();
        String sql = "";
        if (AppType.equals("ALL")) {
            sql = "SELECT big.typename AppName ,sum(HitFreq)  HitFreq FROM cpsp_resserver cp \n"
                    + "inner join apptype big on big.id =cp.AppType  \n"
                    + "left join dpiendpointbean dpi on dpi.masterIp = cp.EquipIp "
                    + "WHERE date_format(R_StartTime,'%Y/%m/%d') =?  AND ProtocolType=? \n";
                  if (!devid.equals("ALL")) {
                      sql = sql + " and  dpi.id =" + devid + " ";
                  }  
                  sql = sql + "group by big.typename order by  AppName ;";
        } else {
            sql = "SELECT cp.AppName , sum(HitFreq)  HitFreq FROM cpsp_resserver cp \n"
                    + "INNER JOIN apptype big on big.id =cp.AppType  \n"
                    + "left join dpiendpointbean dpi on dpi.masterIp = cp.EquipIp "
                    + " WHERE date_format(R_StartTime,'%Y/%m/%d') =?  AND ProtocolType=? AND cp.AppType = ? ";
                  if (!devid.equals("ALL")) {
                      sql = sql + " and  dpi.id =" + devid + " ";
                  } 
                  sql = sql + "group by  cp.AppName  order by  AppName ; ";
        }
   System.out.println("sql=====" + sql ); 
        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setString(1, date1);
        pst.setString(2, ProtType);
        if (!AppType.equals("ALL")) {
            pst.setString(3, AppType);
        }
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("AppName"));
            arr1_2.add(rs.getDouble("HitFreq"));
            arr1.add(arr1_2);
        }
        super.closeResource();
    System.out.println("arr1===" + arr1.toString());
        return arr1;
    }

    //获取CP/SP资源服务器请求占比 天粒度
    public JSONArray getCpSpResPie_DData(String date1, String date2, String ProtType, String AppType, String SM_AppType, String devid) throws ClassNotFoundException, SQLException, ParseException {

        super.getConnection();
        String sql = "";
        if (AppType.equals("ALL")) {
            sql = "SELECT big.typename AppName ,sum(HitFreq)  HitFreq FROM cpsp_resser_day cp \n"
                    + "inner join apptype big on big.id =cp.AppType  \n"
                    + "left join dpiendpointbean dpi on dpi.masterIp = cp.EquipIp "
                    + "WHERE R_StatDate >= ? and  R_StatDate <= ?  AND ProtocolType=? \n" ;
             if (!devid.equals("ALL")){
                sql = sql + "and  dpi.id =" + devid + " ";
             }       
             sql= sql + "group by big.typename order by  AppName ;";
        } else {
            sql = "SELECT cp.AppName , sum(HitFreq)  HitFreq FROM cpsp_resser_day cp \n"
                    + "INNER JOIN apptype big on big.id =cp.AppType  \n"
                    + "left join dpiendpointbean dpi on dpi.masterIp = cp.EquipIp "
                    + "WHERE R_StatDate >= ? and  R_StatDate <= ? AND ProtocolType=? AND cp.AppType = ? ";
            if (!devid.equals("ALL")){
                sql = sql + "and  dpi.id =" + devid + " ";
            }
            sql= sql + "group by  cp.AppName order by AppName ; ";
        }

        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setString(1, date1);
        pst.setString(2, date2);
        pst.setString(3, ProtType);
        if (!AppType.equals("ALL")) {
            pst.setString(4, AppType);
        }
 System.out.println("sql=====" + sql ); 
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("AppName"));
            arr1_2.add(rs.getDouble("HitFreq"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1===" + arr1.toString());
        return arr1;
    }

    //获取流量流向分析流量趋势 5分钟粒度
    public JSONArray getTrafficDir_SQL(String sql) throws ClassNotFoundException, SQLException, ParseException {

        super.getConnection();
        
        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);

        super.closeResource();
        JSONArray arr1 = new JSONArray();

       
        System.out.println("arr1=====" + arr1.toString());
        return arr1;
    }
    
    
    //获取流量流向分析流量趋势 5分钟粒度
    public JSONArray getTraffDirL2Data(String date, String src, String dst, String appType, String appName, String appTraffic_filed, String devid) throws ClassNotFoundException, SQLException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_2 = sdf.parse(date);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());
        super.getConnection();
        String sql = "";
        String backName = "lg.AppName ";
        if (appType.equals("ALL")) {
            backName = "big.typeName ";
        }
        sql = "select " + backName + "AppName,  date_format(R_EndTime,'%Y-%m-%d %H:%i') R_EndTime  ,sum(AppTraffic_UP)  AppTraffic_UP, sum(AppTraffic_DN)  AppTraffic_DN  from  traffdirect_ana ana "
                + "inner join traffdirect_log lg on ana.Traffdirect_id =lg.Traffdirect_id inner join apptype big on big.id =lg.AppType  \n"
                + "left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                + "WHERE R_StartTime >='" + date + "' AND R_EndTime <='" + dayEnd_S  + "' ";
        if (!appType.equals("ALL")) {
            sql = sql + "and  lg.AppType ='" + appType + "' ";
            if (!appName.equals("ALL")) {
                sql = sql + "and  lg.AppName ='" + appName + "' ";
            }
        }
        if (!devid.equals("ALL")) {
            sql = sql + "and  dpi.id =" + devid + " ";
        }
        if ((!src.equals("null")) && (!src.equals(""))) {
            sql = sql + "and  ana.SRC_AREAGROUP_ID ='" + src + "' ";
        }
        if ((!dst.equals("null")) && (!dst.equals(""))) {
            sql = sql + "and  ana.DEST_AREAGROUP_ID ='" + dst + "' ";
        }
        sql = sql + " group by " + backName + ", date_format(R_EndTime,'%Y-%m-%d %H:%i')  order by  AppName,R_EndTime ; ";

        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);

        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        MapOperate mp = new MapOperate();
        while (rs.next()) {
            String AppName = rs.getString("AppName");
            if (flowDataMap.containsKey(AppName)) {
                Map<String, Double> dataMap = flowDataMap.get(AppName);
                String EndTime = rs.getString("R_EndTime");
                String StdEndTime = getStdMinByTime(EndTime);
                //将时间转换到最近的整5分钟
                if (dataMap.containsKey(StdEndTime)) {
                    double result = mp.add(rs.getDouble(appTraffic_filed), dataMap.get(StdEndTime));
                    dataMap.put(StdEndTime, result);
                } else {
                    dataMap.put(StdEndTime, rs.getDouble(appTraffic_filed));
                }
                flowDataMap.put(AppName, dataMap);
            } else {
                Map<String, Double> dataMap2 = new HashMap<String, Double>();
                dataMap2.put(getStdMinByTime(rs.getString("R_EndTime")), rs.getDouble(appTraffic_filed));
                flowDataMap.put(AppName, dataMap2);
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();

        Map<String, Map<String, Double>> fillDataMap = mp.fillMapDataByMin(date, flowDataMap);
        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(entry.getKey());
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            arr1.add(arr1_2);
        }
        System.out.println("arr1=====" + arr1.toString());
        return arr1;
    }

    
    
    
    //获取流量流向分析流量趋势 1天粒度
    public JSONArray getTraffDirL2_DData(String date1,String date2, String src, String dst, String appType, String appName, String appTraffic_filed, String devid) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        
        super.getConnection();
        String sql = "";
        String backName = "ana.AppName ";
        if (appType.equals("ALL")) {
            backName = "big.typeName ";
        }
        sql = "select " + backName + "AppName,  date_format(R_StatDate,'%Y-%m-%d') R_StatDate  ,sum(AppTraffic_UP)  AppTraffic_UP, sum(AppTraffic_DN)  AppTraffic_DN  from  traffdir_ana_day ana "
                + "inner join apptype big on big.id =ana.AppType  \n"
                + "left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                + "where R_StatDate >= '" + date1 + "' and  R_StatDate <= '" + date2 + "' ";
        if (!appType.equals("ALL")) {
            sql = sql + "and  ana.AppType ='" + appType + "' ";
            if (!appName.equals("ALL")) {
                sql = sql + "and  ana.AppName ='" + appName + "' ";
            }
        }
        if (!devid.equals("ALL")) {
            sql = sql + "and  dpi.id =" + devid + " ";
        }
        if ((!src.equals("null")) && (!src.equals(""))) {
            sql = sql + "and  ana.SRC_AREAGROUP_ID ='" + src + "' ";
        }
        if ((!dst.equals("null")) && (!dst.equals(""))) {
            sql = sql + "and  ana.DEST_AREAGROUP_ID ='" + dst + "' ";
        }
        sql = sql + " group by " + backName + ", date_format(R_StatDate,'%Y-%m-%d')  order by  AppName,R_StatDate ; ";

        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);

        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        while (rs.next()) {
            String AppName = rs.getString("AppName");
            if (flowDataMap.containsKey(AppName)) {
                Map<String, Double> dataMap = flowDataMap.get(AppName);
                dataMap.put(rs.getString("R_StatDate"), rs.getDouble(appTraffic_filed));
                flowDataMap.put(AppName, dataMap);
            } else {
                Map<String, Double> dataMap2 = new HashMap<String, Double>();
                dataMap2.put(rs.getString("R_StatDate"), rs.getDouble(appTraffic_filed));
                flowDataMap.put(AppName, dataMap2);
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        MapOperate mp = new MapOperate();
        Map<String, Map<String, Double>> fillDataMap = mp.fillMapDataBydate(date_1, dayEnd.getTime(), flowDataMap);

        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(entry.getKey());
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            arr1.add(arr1_2);
        }
        System.out.println("arr1=====" + arr1.toString());
        return arr1;
    }

    //获取流量流向分析各类应用流量占比
    public JSONArray getTraffDirPieData(String date, String src, String dst, String appType, String appName, String appTraffic_filed, String devid) throws ClassNotFoundException, SQLException, ParseException {
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_2 = sdf.parse(date);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());
        super.getConnection();
        String sql = "";
        String backName = "lg.AppName ";
        if (appType.equals("ALL")) {
            backName = "big.typeName ";
        }
        sql = "select " + backName + " AppName, sum(AppTraffic_UP)  AppTraffic_UP, sum(AppTraffic_DN)  AppTraffic_DN  from  traffdirect_ana ana "
                    + "inner join traffdirect_log lg on ana.Traffdirect_id =lg.Traffdirect_id  inner join apptype big on big.id =lg.AppType  \n"
                    + "left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                    + "WHERE R_StartTime >='" + date + "' AND R_EndTime <='" + dayEnd_S  + "' ";
        if (!appType.equals("ALL")) {
            sql = sql + "and  lg.AppType ='" + appType + "' ";
        }
        if (!devid.equals("ALL")) {
            sql = sql + "and  dpi.id =" + devid + " ";
        }
        if ((!src.equals("null")) && (!src.equals(""))) {
            sql = sql + "and  ana.SRC_AREAGROUP_ID ='" + src + "' ";
        }
        if ((!dst.equals("null")) && (!dst.equals(""))) {
            sql = sql + "and  ana.DEST_AREAGROUP_ID ='" + dst + "' ";
        }
            sql = sql + " group by " + backName + " order by " + appTraffic_filed + " desc ";
                   
        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);     
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("AppName"));
            arr1_2.add(rs.getDouble(appTraffic_filed));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1=====" + arr1.toString());
        return arr1;
    }

    //获取Wlan各类终端占比
    
    public JSONArray getWlanRepPieData(String date) throws ClassNotFoundException, SQLException, ParseException {
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_2 = sdf.parse(date);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());
        super.getConnection();
        String sql = " select DeviceType, count(*) as phnum from wlanreport wl "
                + " where AccessTime >='" + date + "' and AccessTime <='" + dayEnd_S + "' " ;
        
        sql = sql + " group by  DeviceType  order by  phnum desc ;";
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);     
        JSONArray arr1 = new JSONArray();
        int rs_num = 0;
        int other_num = 0;
        while (rs.next()) {
            rs_num ++ ;
            JSONArray arr1_2 = new JSONArray();
            if(rs_num<=5){
                arr1_2.add(rs.getString("DeviceType"));
                arr1_2.add(rs.getDouble("phnum"));
                arr1.add(arr1_2);
            }else{
                other_num = other_num + rs.getInt("phnum") ;
            }
        }
        if(rs_num>5){
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add("其它");
            arr1_2.add(other_num);
            arr1.add(arr1_2);
        }
        
        super.closeResource();
        System.out.println("arr1=====" + arr1.toString());
        return arr1;
    }
    
    //获取流量流向分析各类应用流量占比_按天汇总
    public JSONArray getTraffDirPie_DData(String date1,String date2, String src, String dst, String appType, String appName, String appTraffic_filed, String devid) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());
        
        super.getConnection();
        String sql = "";
        String backName = "ana.AppName ";
        if (appType.equals("ALL")) {
            backName = "big.typeName ";
        }
        sql = "select " + backName + " AppName, sum(AppTraffic_UP)  AppTraffic_UP, sum(AppTraffic_DN)  AppTraffic_DN  from traffdir_ana_day ana  "
                    + " inner join apptype big on big.id =ana.AppType  \n"
                    + "left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                    + " where R_StatDate >='" + date1 + "' AND R_StatDate <='" + dayEnd_S + "' ";
        
        if (!appType.equals("ALL")) {
            sql = sql + "and  ana.AppType ='" + appType + "' ";
        }
        if (!devid.equals("ALL")) {
            sql = sql + "and  dpi.id =" + devid + " ";
        }
        if ((!src.equals("null")) && (!src.equals(""))) {
            sql = sql + "and  ana.SRC_AREAGROUP_ID ='" + src + "' ";
        }
        if ((!dst.equals("null")) && (!dst.equals(""))) {
            sql = sql + "and  ana.DEST_AREAGROUP_ID ='" + dst + "' ";
        }
        sql = sql + " group by " + backName + " order by " + appTraffic_filed + " desc " ;
                   
        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);     
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("AppName"));
            arr1_2.add(rs.getDouble(appTraffic_filed));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1=====" + arr1.toString());
        return arr1;
    }

    //获取general_flow 总信息
    public JSONArray getGenFlowTotalInfoData() throws ClassNotFoundException, SQLException {
        super.getConnection();
        String sql = " select   cast(sum( AppTraffic_Up )/1024/1024  as decimal(18,4))  AppTraffic_Up ,  \n"
                + " cast(sum( AppTraffic_Dn )/1024/1024  as decimal(18,4))  AppTraffic_Dn   from  general_flow_stat  gen ; ";
        PreparedStatement pst;
        pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getDouble("AppTraffic_Up"));
            arr1_2.add(rs.getDouble("AppTraffic_Dn"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;
    }

    //获取源区域组ID-目的区域组ID
    public JSONArray getSRC_DESTData() throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "select distinct id , group_name from traffic_flow_group_area ORDER BY  id;";
        
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getInt(1));
            arr1_2.add(rs.getString(2));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;

    }

//    //获取源区域组ID-目的区域组ID 按天
//    public JSONArray getSRC_DEST_DData(String date1, String date2) throws ClassNotFoundException, SQLException, ParseException {
//
////        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
////        Date date_1 = sdf.parse(date1);
////        Date date_2 = sdf.parse(date2);
//        super.getConnection();
//        String sql = "select distinct concat(SRC_AREAGROUP_ID,'-',DEST_AREAGROUP_ID) src_dest from  traffdir_ana_day ana  where R_StatDate >= ? and  R_StatDate <= ? ; ";
//        PreparedStatement pst = super.conn.prepareStatement(sql);
//        pst.setString(1, date1);
//        pst.setString(2, date2);
//        ResultSet rs = pst.executeQuery();
//        JSONArray arr1 = new JSONArray();
//        while (rs.next()) {
//            JSONArray arr1_2 = new JSONArray();
//            arr1_2.add(rs.getString("src_dest"));
//            arr1.add(arr1_2);
//        }
//        super.closeResource();
//        return arr1;
//
//    }

    //获取所有DevId
    public JSONArray getDevId() throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "SELECT id ,name FROM dpiendpointbean ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("id"));
            arr1_2.add(rs.getString("name"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;
    }

    //获取所有应用类型大类
    public JSONArray getBigAppType() throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "SELECT id ,typeName FROM apptype ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("id"));
            arr1_2.add(rs.getString("typeName"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;
    }

    //获取所有应用类型大类
    private Map<String, String> appTypeMap() throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "SELECT id ,typeName FROM apptype ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        Map<String, String> appMap = new HashMap<String, String>();
        while (rs.next()) {
            appMap.put(rs.getString("id"), rs.getString("typeName"));
        }
        super.closeResource();
        return appMap;
    }

    //获取某个大类的所有小类
    public JSONArray getSMAppType(String BigType) throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "SELECT id, name FROM appdef WHERE AppType_ID =  ? ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setString(1, BigType);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getInt("id"));
            arr1_2.add(rs.getString("name"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;
    }

    //获取所有应用小类
    private Map<String, List<String>> appNameMap() throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "SELECT AppType_ID ,name FROM appdef ORDER BY AppType_ID ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        Map<String, List<String>> appMap = new HashMap<String, List<String>>();
        while (rs.next()) {
            String appID = rs.getString("AppType_ID");
            if (appMap.containsKey(appID)) {
                List<String> dataList = appMap.get(appID);
                dataList.add(rs.getString("name"));
                appMap.put(appID, dataList);
            } else {
                List<String> dataList2 = new ArrayList<String>();
                dataList2.add(rs.getString("name"));
                appMap.put(appID, dataList2);
            }
        }
        super.closeResource();
        return appMap;
    }

    //获取所有Web类型
    public JSONArray getWebType() throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "SELECT Code ,Name FROM webtype_list ORDER BY Code ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("Code"));
            arr1_2.add(rs.getString("Name"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1::::::::=" + arr1.toString());
        return arr1;
    }

    //获取一ToN 综合报告
    public JSONArray getOneToN_GenCht(String date1, String date2, String devid) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_s = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "SELECT date_format(R_StartTime,'%Y-%m-%d') R_StatDate,COUNT(DISTINCT UserAccount) UserNum FROM oneton_det_kf_res one_n \n"
                + "left join dpiendpointbean dpi on dpi.masterIp = one_n.EquipIp "
                + " where ShareTerminal_Num >=2 AND R_StartTime >= '" + date1 + "' and  R_EndTime < '" + dayEnd_s + "' " ;
        if(!devid.equals("ALL")){
                sql = sql+ "and  dpi.id ="+devid ;
        }
        sql=sql + " group by date_format(R_StartTime,'%Y-%m-%d') order by  R_StatDate ; ";

        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);

        Map<String, Double> dataMap2 = new HashMap<String, Double>();
        while (rs.next()) {
            dataMap2.put(rs.getString("R_StatDate"), rs.getDouble("UserNum"));
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        MapOperate mp = new MapOperate();
        Map<String, Double> fillDataMap = mp.fillMapData(date_1, dayEnd.getTime(), dataMap2);

        JSONArray arr1_2 = new JSONArray();
        arr1_2.add("共享用户趋势图");
        arr1_2.add(mp.getStrByMap(fillDataMap));
        arr1.add(arr1_2);
        System.out.println("arr1::::::::=" + arr1.toString());
        return arr1;
    }

    //获取一ToN 用户数统计
    public JSONArray getOneToN_User(String date1, String date2, String devid) throws ClassNotFoundException, SQLException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_s = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "SELECT '活跃用户数'  stat_item ,   date_format(R_StartTime,'%Y-%m-%d') R_StatDate,COUNT(DISTINCT UserAccount) UserNum FROM oneton_det_kf_res t1 \n"
                + " left join dpiendpointbean dpi on dpi.masterIp = t1.EquipIp "
                + " WHERE R_StartTime >= '" + date1 + "' and  R_EndTime < '" + dayEnd_s + "'" ;
        if(!devid.equals("ALL"))  sql = sql+ "and  dpi.id ="+devid+" ";    
            sql = sql +  " group by date_format(R_StartTime,'%Y-%m-%d') \n"
                + " UNION ALL SELECT  '共享用户数'  stat_item , date_format(R_StartTime,'%Y-%m-%d') R_StatDate,COUNT(DISTINCT UserAccount) UserNum FROM oneton_det_kf_res t2 \n"
                + " left join dpiendpointbean dpi2 on dpi2.masterIp = t2.EquipIp "
                + " WHERE R_StartTime >= '" + date1 + "' and  R_EndTime < '" + dayEnd_s + "'" ;
        if(!devid.equals("ALL"))  sql = sql+ "and  dpi2.id ="+devid+" ";    
            sql = sql + " AND t2.ShareTerminal_Num>1  group by date_format(R_StartTime,'%Y-%m-%d');";

        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);
        
        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        while (rs.next()) {
            String AppName = rs.getString("stat_item");
            if (flowDataMap.containsKey(AppName)) {
                Map<String, Double> dataMap = flowDataMap.get(AppName);
                dataMap.put(rs.getString("R_StatDate"), rs.getDouble("UserNum"));
                flowDataMap.put(AppName, dataMap);
            } else {
                Map<String, Double> dataMap2 = new HashMap<String, Double>();
                dataMap2.put(rs.getString("R_StatDate"), rs.getDouble("UserNum"));
                flowDataMap.put(AppName, dataMap2);
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        MapOperate mp = new MapOperate();
        Map<String, Map<String, Double>> fillDataMap = mp.fillMapDataBydate(date_1, dayEnd.getTime(), flowDataMap);
        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(entry.getKey());
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            arr1.add(arr1_2);
        }
        System.out.println("arr1::::::::=" + arr1.toString());
        return arr1;
    }

    //获取一ToN 共享分布
    public JSONArray getOneToN_Share(String date1, int minutes ,String devid) throws ClassNotFoundException, SQLException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date1);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_s = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "SELECT  Share_Num ,COUNT(UserAccount) UserNum FROM \n"
                + "( SELECT  CASE WHEN  MAX(ShareTerminal_Num) <=5 THEN  MAX(ShareTerminal_Num) WHEN   MAX(ShareTerminal_Num) >5  AND  MAX(ShareTerminal_Num) <=10  THEN '6-10' \n"
                + "               WHEN MAX(ShareTerminal_Num) >10   THEN 'OVER_10'  END Share_Num ,  UserAccount  FROM oneton_det_kf_res res left join dpiendpointbean dpi on dpi.masterIp = res.EquipIp \n"
                + " WHERE R_StartTime >= '" + date1 + "' AND R_EndTime <= '" + dayEnd_s + "' AND ShareTerminal_Num>1" ;
        if(!devid.equals("ALL")) sql = sql+ " and dpi.id ="+devid+" ";
               sql= sql + " GROUP BY UserAccount)  t2 group by  Share_Num ORDER BY  Share_Num ;";
        if (minutes > 0) {
            sql = "SELECT  Share_Num ,COUNT(UserAccount) UserNum FROM \n"
                    + "( SELECT  CASE WHEN  MAX(ShareTerminal_Num) <=5 THEN  MAX(ShareTerminal_Num) WHEN   MAX(ShareTerminal_Num) >5  AND  MAX(ShareTerminal_Num) <=10  THEN '6-10' \n"
                    + "               WHEN MAX(ShareTerminal_Num) >10   THEN 'OVER_10'  END Share_Num ,  UserAccount  FROM oneton_det_kf_res res left join dpiendpointbean dpi on dpi.masterIp = res.EquipIp \n"
                    + " WHERE R_EndTime > date_add(NOW(), interval -" + minutes + " minute) AND R_EndTime <= NOW() AND ShareTerminal_Num>1 ";
                if(!devid.equals("ALL")) sql = sql+ " and dpi.id ="+devid+" ";
                sql= sql+ " GROUP BY UserAccount)  t2 group by  Share_Num ORDER BY  Share_Num ;";
        }
        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("Share_Num") + "台");
            arr1_2.add(rs.getString("UserNum"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1::::::::=" + arr1.toString());
        return arr1;
    }

    //获取一ToN 检测日报
    public JSONObject getOneToN_Report(String UserAcc, String UserIP, String ShareTerm_S, String ShareTerm_E,
            String date1, String date2, String devid, int iDisplayStart, int iDisplayLength, int reportType ) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_s = sdf.format(dayEnd.getTime());
        String tableName = "oneton_det_keyfield" ;
        if(reportType==0){
            tableName = "oneton_det_keyfield";
        }else{
            tableName = "oneton_det_dayreport";
        }
        
        super.getConnection();
        String sql = "SELECT dpi.name , date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') AS R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') AS R_EndTime , UserAccount , UserIP , \n"
                + " QQidcnt , IPNATcnt, Cookiecnt, Devcnt , OScnt ,detnum FROM " + tableName + " one_n left join dpiendpointbean dpi on dpi.masterIp = one_n.EquipIp "
                + " WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_s + "'";
        String sql1 = "SELECT count(*) AS total_num   FROM " + tableName + " one_n left join dpiendpointbean dpi on dpi.masterIp = one_n.EquipIp WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_s + "'";
        if ((UserAcc != null) && !"".equals(UserAcc)) {
            sql = sql + " AND UserAccount LIKE '%" + UserAcc + "%'";
            sql1 = sql1 + " AND UserAccount LIKE '%" + UserAcc + "%'";
        }
        if ((UserIP != null) && !"".equals(UserIP)) {
            sql = sql + " AND UserIP LIKE '%" + UserIP + "%'";
            sql1 = sql1 + " AND UserIP LIKE '%" + UserIP + "%'";
        }
//        if ((ShareTerm_S != null) && !"".equals(ShareTerm_S)) {
//            int share_s = Integer.parseInt(ShareTerm_S);
//            sql = sql + " AND ShareTerminal_Num >=" + share_s;
//            sql1 = sql1 + " AND ShareTerminal_Num >=" + share_s;
//        }
//        if ((ShareTerm_E != null) && !"".equals(ShareTerm_E)) {
//            int share_e = Integer.parseInt(ShareTerm_E);
//            sql = sql + " AND ShareTerminal_Num <=" + share_e;
//            sql1 = sql1 + " AND ShareTerminal_Num <=" + share_e;
//        }
        if(!devid.equals("ALL")){
            sql = sql+ " and dpi.id ="+devid+" ";
            sql1 = sql1 + " AND dpi.id =" + devid ;
        }
        sql = sql + " limit " + iDisplayStart + "," + iDisplayLength;
        System.out.println("sql1111="+sql);
        System.out.println("sql1======"+sql1);
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("name"));
            arr1_2.add(rs.getString("R_StartTime"));
            arr1_2.add(rs.getString("R_EndTime"));
            arr1_2.add(rs.getString("UserAccount"));
            arr1_2.add(rs.getString("UserIP"));
//            arr1_2.add(rs.getString("ShareTerminal_Num"));
            arr1_2.add(rs.getString("QQidcnt"));
            arr1_2.add(rs.getString("IPNATcnt"));
            arr1_2.add(rs.getString("Cookiecnt"));
            arr1_2.add(rs.getString("Devcnt"));
            arr1_2.add(rs.getString("OScnt"));
            arr1_2.add(rs.getString("detnum"));
            arr1.add(arr1_2);
        }
        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }

        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数
        System.out.println(DataObj);
        super.closeResource();
        System.out.println("DataObj::::::::=" + DataObj.toString());
        return DataObj;
    }

    //访问指定应用的用户趋势
    public JSONArray getVisitAppUserTrd(String date1, String AppType, String SM_AppType, String devid) throws ClassNotFoundException, SQLException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_2 = sdf.parse(date1);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());
        super.getConnection();
        String sql = "";
        if (AppType.equals("ALL")) {
            sql = "select big.typename AppName, date_format(R_EndTime,'%Y-%m-%d %H:%i') R_EndTime ,COUNT(DISTINCT CONCAT(UserType,UserAccount)) UserNum  from  visit_App_User_Stat ana "
                    + "inner join visit_App_User_Log lg on ana.visit_App_Id =lg.visit_App_Id  \n"
                    + "inner join apptype big on big.id =ana.AppType  \n" 
                    + "left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                    + "WHERE R_StartTime >='" + date1 + "' AND R_EndTime <='" + dayEnd_S + "' ";
            if (!devid.equals("ALL")) {
                sql = sql + " and dpi.id =" + devid + " ";
            }
            sql = sql + " group by  big.typename, date_format(R_EndTime,'%Y-%m-%d %H:%i') order by  AppName,R_EndTime  ; ";
        } else {
            sql = "select ana.AppName, date_format(R_EndTime,'%Y-%m-%d %H:%i') R_EndTime ,COUNT(DISTINCT CONCAT(UserType,UserAccount)) UserNum  from  visit_App_User_Stat ana "
                    + "inner join visit_App_User_Log lg on ana.visit_App_Id =lg.visit_App_Id  \n"
                    + "inner join apptype big on big.id =ana.AppType  \n"
                    + "left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                    + "WHERE R_StartTime >='" + date1 + "' AND R_EndTime <='" + dayEnd_S + "' and ana.AppType ='" + AppType + "' ";
            if (!SM_AppType.equals("ALL")) {
                sql = sql + "and  ana.AppName ='" + SM_AppType + "'";
            }
            if (!devid.equals("ALL")) {
                sql = sql + " and dpi.id =" + devid + " ";
            }
            sql = sql + " group by  ana.AppName,  date_format(R_EndTime,'%Y-%m-%d %H:%i') order by AppName,R_EndTime ; ";
        }

        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);
        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        MapOperate mp = new MapOperate();
        while (rs.next()) {
            String AppName = rs.getString("AppName");
            if (flowDataMap.containsKey(AppName)) {
                Map<String, Double> dataMap = flowDataMap.get(AppName);
                String EndTime = rs.getString("R_EndTime");
                String StdEndTime = getStdMinByTime(EndTime);
                //将时间转换到最近的整5分钟
                if (dataMap.containsKey(StdEndTime)) {
                    double result = mp.add(rs.getDouble("UserNum"), dataMap.get(StdEndTime));
                    dataMap.put(StdEndTime, result);
                } else {
                    dataMap.put(StdEndTime, rs.getDouble("UserNum"));
                }
                flowDataMap.put(AppName, dataMap);
            } else {
                Map<String, Double> dataMap2 = new HashMap<String, Double>();
                dataMap2.put(getStdMinByTime(rs.getString("R_EndTime")), rs.getDouble("UserNum"));
                flowDataMap.put(AppName, dataMap2);
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        Map<String, Map<String, Double>> fillDataMap = mp.fillMapDataByMin(date1, flowDataMap);
        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(entry.getKey());
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            arr1.add(arr1_2);
        }
        return arr1;
    }

    //访问指定应用的用户趋势 1天粒度
    public JSONArray getVisitAppUserTrd_D(String date1, String date2, String AppType, String SM_AppType, String devid) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "";
        if (AppType.equals("ALL")) {
            sql = "select big.typename AppName, date_format(R_StartTime,'%Y-%m-%d') R_StatDate, COUNT(DISTINCT CONCAT(UserType,UserAccount)) UserNum  from  visit_App_User_Stat ana "
                    + "inner join visit_App_User_Log lg on ana.visit_App_Id =lg.visit_App_Id  \n"
                    + "inner join apptype big on big.id =ana.AppType  \n"
                    + "left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                    + "where R_StartTime >='" + date1 + "' AND R_EndTime <='" + dayEnd_S +"' ";
            if (!devid.equals("ALL")){
                sql = sql + " and  dpi.id =" + devid + " ";
            }
            sql =sql + " group by  big.typename, date_format(R_StartTime,'%Y-%m-%d') order by  AppName,R_StatDate; ";
        } else {
            sql = "select ana.AppName, date_format(R_StartTime,'%Y-%m-%d') R_StatDate, COUNT(DISTINCT CONCAT(UserType,UserAccount)) UserNum  from  visit_App_User_Stat ana "
                    + "inner join visit_App_User_Log lg on ana.visit_App_Id =lg.visit_App_Id  \n"
                    + "inner join apptype big on big.id =ana.AppType  \n"
                    + "left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                    + "where R_StartTime >='" + date1 + "' AND R_EndTime <='" + dayEnd_S + "' AND ana.AppType ='" + AppType + "' ";
            if (!SM_AppType.equals("ALL")){
                sql = sql + "AND ana.AppName ='" + SM_AppType + "'";
            }
            if (!devid.equals("ALL")){
                sql = sql + " and  dpi.id =" + devid + " ";
            }
            sql = sql + " group by  ana.AppName,date_format(R_StartTime,'%Y-%m-%d') order by AppName,R_StatDate ; ";
        }
        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);

        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        while (rs.next()) {
            String AppName = rs.getString("AppName");
            if (flowDataMap.containsKey(AppName)) {
                Map<String, Double> dataMap = flowDataMap.get(AppName);
                dataMap.put(rs.getString("R_StatDate"), rs.getDouble("UserNum"));
                flowDataMap.put(AppName, dataMap);
            } else {
                Map<String, Double> dataMap2 = new HashMap<String, Double>();
                dataMap2.put(rs.getString("R_StatDate"), rs.getDouble("UserNum"));
                flowDataMap.put(AppName, dataMap2);
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        MapOperate mp = new MapOperate();
        Map<String, Map<String, Double>> fillDataMap = mp.fillMapDataBydate(date_1, dayEnd.getTime(), flowDataMap);

        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(entry.getKey());
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            arr1.add(arr1_2);
        }
        System.out.println("arr1::::::::=" + arr1.toString());
        return arr1;
    }

    //访问指定应用的用户日志详情
    public JSONObject getVisitAppDetails(String date1, String date2, String appType, String sm_AppType, String devid,int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());

        super.getConnection();
        String  sql = "select dpi.name,date_format(R_StartTime,'%Y-%m-%d %H:%i:%s')  R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') R_EndTime,\n" +
               "big.typeName, ana.AppNameLength, ana.AppName,ana.UserNum from  visit_App_User_Stat ana "
                + "inner join apptype big on big.id =ana.AppType  \n"
                + "left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                + "where R_StartTime>='" + date1 + "' AND R_EndTime <='" + dayEnd_S +"' ";
        String  sql1 = "select count(*) as total_num from  visit_App_User_Stat ana "
                + "inner join apptype big on big.id =ana.AppType  \n"
                + "left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                + "where R_StartTime>='" + date1 + "' AND R_EndTime <='" + dayEnd_S +"' ";
        if (!devid.equals("ALL")){
            sql = sql + " and  dpi.id =" + devid + " ";
            sql1 = sql1 + " and  dpi.id =" + devid + " ";
        }
        if (!appType.equals("ALL")) {
            sql = sql + " and  ana.AppType ='" + appType + "' ";
            sql1 = sql1 + " and  ana.AppType ='" + appType + "' ";
            if (!sm_AppType.equals("ALL")) {
                sql = sql + " and  ana.AppName ='" + sm_AppType + "' ";
                sql1 = sql1 + " and  ana.AppName ='" + sm_AppType + "' ";
            }
        } 
        sql = sql + " limit " + iDisplayStart + "," + iDisplayLength;
        System.out.println("sql1111=" + sql);

        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString(1));        
            arr1_2.add(rs.getString("R_StartTime"));
            arr1_2.add(rs.getString("R_EndTime"));
            arr1_2.add(rs.getString(4));
            arr1_2.add(rs.getString(5));
            arr1_2.add(rs.getString(6));
            arr1_2.add(rs.getString(7));
            arr1.add(arr1_2);
        }
        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }
        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数
        System.out.println("DataObj::::::::=" + DataObj.toString());
        super.closeResource();
        System.out.println("DataObj:::==" + DataObj.toString());
        return DataObj;
    }
    ////访问指定应用的用户占比
    public JSONArray getVisitAppUserPie(String date1, String date2, String AppType, String devid) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "";
        if (AppType.equals("ALL")) {
            sql = "select big.typename AppName,COUNT(DISTINCT CONCAT(UserType,UserAccount)) UserNum  from  visit_App_User_Stat ana "
                    + "inner join visit_App_User_Log lg on ana.visit_App_Id =lg.visit_App_Id  \n"
                    + "inner join apptype big on big.id =ana.AppType  \n"
                    + " left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                    + "where R_StartTime >='" + date1 + "' AND R_EndTime <='" + dayEnd_S + "'";
                if(!devid.equals("ALL")) {
                    sql = sql + " and  dpi.id =" + devid + " ";
                }
              sql =sql + " group by  big.typename ORDER BY UserNum DESC  ; ";
        } else {
            sql = "select ana.AppName, COUNT(DISTINCT CONCAT(UserType,UserAccount)) UserNum  from  visit_App_User_Stat ana "
                    + "inner join visit_App_User_Log lg on ana.visit_App_Id =lg.visit_App_Id  \n"
                    + "inner join apptype big on big.id =ana.AppType left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp "
                    + "where R_StartTime >='" + date1 + "' AND R_EndTime <='" + dayEnd_S + "' AND ana.AppType ='" + AppType + "' " ;
              if(!devid.equals("ALL")){
                    sql = sql + " and  dpi.id =" + devid + " ";
               }
              sql =sql + " group by ana.AppName ORDER BY UserNum DESC  ;  ";
        }
        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("AppName"));
            arr1_2.add(rs.getDouble("UserNum"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1::::::::=" + arr1.toString());
        return arr1;
    }

    //获取非法路由 检测日志
    public JSONObject getIllegalRouteLog(String date1, String date2, String nodeIP, String cpVal,String devid,
            int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_s = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "SELECT dpi.name as dpiname, date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') AS R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') AS R_EndTime , NodeIP , NodeInTraffic , NodeOutTraffic ,\n"
                + " CP_Length , CP FROM illegal_route_detect ana left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp   WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_s + "'";
        String sql1 = "SELECT count(*) AS total_num   FROM illegal_route_detect ana left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp  WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_s + "'";
        if ((nodeIP != null) && !"".equals(nodeIP)) {
            sql = sql + " AND NodeIP LIKE '%" + nodeIP + "%'";
            sql1 = sql1 + " AND NodeIP LIKE '%" + nodeIP + "%'";
        }
        if ((cpVal != null) && !"".equals(cpVal)) {
            sql = sql + " AND CP LIKE '%" + cpVal + "%'";
            sql1 = sql1 + " AND CP LIKE '%" + cpVal + "%'";
        }
        if (!devid.equals("ALL")){
            sql = sql + " and  dpi.id =" + devid + " ";
        }
        sql = sql + " limit " + iDisplayStart + "," + iDisplayLength;
        System.out.println("sql1111=" + sql);

        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("dpiname"));
            arr1_2.add(rs.getString("R_StartTime"));
            arr1_2.add(rs.getString("R_EndTime"));
            arr1_2.add(rs.getString("NodeIP"));
            arr1_2.add(rs.getString("NodeInTraffic"));
            arr1_2.add(rs.getString("NodeOutTraffic"));
            arr1_2.add(rs.getString("CP_Length"));
            arr1_2.add(rs.getString("CP"));
            arr1.add(arr1_2);
        }
        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }
        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数
        System.out.println("DataObj::::::::=" + DataObj.toString());
        super.closeResource();
        return DataObj;
    }                   
    
       //获取Web信息推送结果上报 检测日志
    public JSONObject getWebMsgSendResLog(String date1, String date2, String userType, String userName,String insert_URL,String devid,
            int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//        Date date_1 = sdf.parse(date1);
//        Date date_2 = sdf.parse(date2);
//        Calendar dayEnd = Calendar.getInstance();
//        dayEnd.setTime(date_2);
//        dayEnd.add(dayEnd.DATE, 1);
//        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
//        dayEnd.set(Calendar.MINUTE, 0);
//        dayEnd.set(Calendar.SECOND, 0);
//        String dayEnd_s = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "select dpi.name AS dpiname, UserType,UserName_Length,UserName,Adv_ID, date_format(Adv_Time,'%Y-%m-%d %H:%i:%s') AS Adv_Time ,Insert_URL_Length,Insert_URL \n" +
                "from  webmsgsendres ana left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp   WHERE Adv_Time >= '" + date1 + "' and Adv_Time <= '" + date2 + "' and userType='" + userType +"'";
        String sql1 = "SELECT count(*) AS total_num from  webmsgsendres ana left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp   WHERE Adv_Time >= '" + date1 + "' and Adv_Time <= '" + date2 + "' and userType='" + userType +"'";
        if ((userName != null) && !"".equals(userName)) {
            sql = sql + " AND UserName LIKE '%" + userName + "%'";
            sql1 = sql1 + " AND UserName LIKE '%" + userName + "%'";
        }
        if ((insert_URL != null) && !"".equals(insert_URL)) {
            sql = sql + " AND Insert_URL LIKE '%" + insert_URL + "%'";
            sql1 = sql1 + " AND Insert_URL LIKE '%" + insert_URL + "%'";
        }
        if (!devid.equals("ALL")){
            sql = sql + " and  dpi.id =" + devid + " ";
        }
        sql = sql + " limit " + iDisplayStart + "," + iDisplayLength;
        System.out.println("sql1111=" + sql);

        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("dpiname"));
            arr1_2.add(rs.getString(2));
            arr1_2.add(rs.getString(3));
            arr1_2.add(rs.getString(4));
            arr1_2.add(rs.getString(5));
            arr1_2.add(rs.getString(6));
            arr1_2.add(rs.getString(7));
            arr1_2.add(rs.getString(8));
            arr1.add(arr1_2);
        }
        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }
        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数
        System.out.println("DataObj::::::::=" + DataObj.toString());
        super.closeResource();
        return DataObj;
    }
    
    //获取Wlan 检测日志
    public JSONObject getWlanReportLog(String time1, String time2, String userAccount,
            int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {
        boolean bl = super.getConnection();
        System.out.println("bl=========="+bl);
        String sql = " SELECT  UserAccount , DeviceType ,PhoneNum ,date_format(AccessTime,'%Y-%m-%d %H:%i:%s') FROM WlanReport  WHERE  1=1 "  ;
        String sql1 = " SELECT count(*) AS total_num from  WlanReport ana  WHERE 1=1 " ;
        if ((userAccount != null) && !"".equals(userAccount)) {
            sql = sql + " AND userAccount LIKE '%" + userAccount + "%'";
            sql1 = sql1 + " AND userAccount LIKE '%" + userAccount + "%'";
        }
        if ((time1 != null) && !"".equals(time1)) {
            sql = sql + " AND AccessTime >='" + time1 + "'";
            sql1 = sql1 + " AND AccessTime >='" + time1 + "'";
        }
        if ((time2 != null) && !"".equals(time2)) {
            sql = sql + " AND AccessTime <= '" + time2 + "'";
            sql1 = sql1 + " AND AccessTime <= '" + time2 + "'";
        }
        sql = sql + " limit " + iDisplayStart + "," + iDisplayLength;
        System.out.println("sql1111=" + sql);

        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString(1));
            arr1_2.add(rs.getString(2));
            arr1_2.add(rs.getString(3));
            arr1_2.add(rs.getString(4));      
            arr1.add(arr1_2);
        }
        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }
        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数
        System.out.println("DataObj::::::::=" + DataObj.toString());
        super.closeResource();
        return DataObj;
    }
    
    
    // 非法路由检测 流量趋势
    public JSONArray getIllRouteTrd(String date1, String nodeIP, String cpVal, String traffType, String devid) throws ClassNotFoundException, SQLException, ParseException {

        super.getConnection();
        String sql = "select date_format(R_EndTime,'%Y-%m-%d %H:%i') R_EndTime ,SUM(NodeInTraffic) NodeInTraffic,SUM(NodeOutTraffic) NodeOutTraffic  FROM  illegal_route_detect ana "
                + "left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp  where date_format(R_StartTime,'%Y/%m/%d') ='" + date1 + "'";
        if ((nodeIP != null) && !"".equals(nodeIP)) {
            sql = sql + " AND NodeIP LIKE '%" + nodeIP + "%'";
        }
        if ((cpVal != null) && !"".equals(cpVal)) {
            sql = sql + " AND CP LIKE '%" + cpVal + "%'";
        }
        if (!devid.equals("ALL")) {
            sql = sql + " and  dpi.id =" + devid + " ";
        }
        sql = sql + " group by  date_format(R_EndTime,'%Y-%m-%d %H:%i') order by R_EndTime ;";

        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);
        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        MapOperate mp = new MapOperate();
        String AppName = "接入流量";
        if (traffType.equals("NodeOutTraffic")) {
            AppName = "接出流量";
        }
        while (rs.next()) {
            if (flowDataMap.containsKey(AppName)) {
                Map<String, Double> dataMap = flowDataMap.get(AppName);
                String EndTime = rs.getString("R_EndTime");
                String StdEndTime = getStdMinByTime(EndTime);
                //将时间转换到最近的整5分钟
                if (dataMap.containsKey(StdEndTime)) {
                    double result = mp.add(rs.getDouble(traffType), dataMap.get(StdEndTime));
                    dataMap.put(StdEndTime, result);
                } else {
                    dataMap.put(StdEndTime, rs.getDouble(traffType));
                }
                flowDataMap.put(AppName, dataMap);
            } else {
                Map<String, Double> dataMap2 = new HashMap<String, Double>();
                dataMap2.put(getStdMinByTime(rs.getString("R_EndTime")), rs.getDouble(traffType));
                flowDataMap.put(AppName, dataMap2);
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        Map<String, Map<String, Double>> fillDataMap = mp.fillMapDataByMin(date1, flowDataMap);
        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(entry.getKey());
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            arr1.add(arr1_2);
        }
        System.out.println("arr1::::::::=" + arr1.toString());
        return arr1;
    }

    //非法路由检测 流量趋势 1天粒度
    public JSONArray getIllRouteTrd_D(String date1, String date2, String nodeIP, String cpVal, String traffType, String devid) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "select date_format(R_StartTime,'%Y-%m-%d') R_StatDate ,SUM(NodeInTraffic) NodeInTraffic,SUM(NodeOutTraffic) NodeOutTraffic  FROM  illegal_route_detect ana "
                + " left join dpiendpointbean dpi on dpi.masterIp = ana.EquipIp where date_format(R_StartTime,'%Y/%m/%d') >='" + date1 + "' AND R_EndTime <='" + dayEnd_S + "'";
        if ((nodeIP != null) && !"".equals(nodeIP)) {
            sql = sql + " AND NodeIP LIKE '%" + nodeIP + "%'";
        }
        if ((cpVal != null) && !"".equals(cpVal)) {
            sql = sql + " AND CP LIKE '%" + cpVal + "%'";
        }
        if (!devid.equals("ALL")) {
            sql = sql + " and  dpi.id =" + devid + " ";
        } 
        sql = sql + " group by  date_format(R_StartTime,'%Y-%m-%d') order by  R_StatDate ;";
        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);
        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        String AppName = "接入流量";
        if (traffType.equals("NodeOutTraffic")) {
            AppName = "接出流量";
        }
        while (rs.next()) {

            if (flowDataMap.containsKey(AppName)) {
                Map<String, Double> dataMap = flowDataMap.get(AppName);
                dataMap.put(rs.getString("R_StatDate"), rs.getDouble(traffType));
                flowDataMap.put(AppName, dataMap);
            } else {
                Map<String, Double> dataMap2 = new HashMap<String, Double>();
                dataMap2.put(rs.getString("R_StatDate"), rs.getDouble(traffType));
                flowDataMap.put(AppName, dataMap2);
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        MapOperate mp = new MapOperate();
        Map<String, Map<String, Double>> fillDataMap = mp.fillMapDataBydate(date_1, dayEnd.getTime(), flowDataMap);

        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(entry.getKey());
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            arr1.add(arr1_2);
        }
        System.out.println("arr1::::::::=" + arr1.toString());
        return arr1;
    }

    //Voip协议产生的总流量趋势
    public JSONArray getVoipFlowTrd(String date1, String userGroup, String mediaProt, String sigProt, String traffSrc, String devName) throws ClassNotFoundException, SQLException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_2 = sdf.parse(date1);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());
        super.getConnection();
        String sql = "SELECT date_format(R_EndTime,'%Y-%m-%d %H:%i') R_EndTime ,SUM(Rtp_Media_Traffic) Rtp_Media_Traffic,SUM(Other_Media_Traffic) Other_Media_Traffic,\n"
                + "SUM(H323_Sig_Traffic) H323_Sig_Traffic,SUM(SIP_Sig_Traffic) SIP_Sig_Traffic,SUM(MGCP_Sig_Traffic) MGCP_Sig_Traffic \n"
                + "FROM voip_flow_stat voip LEFT JOIN dpiendpointbean dpi on dpi.masterIp = voip.EquipIp WHERE R_StartTime >='" + date1 + "' AND R_EndTime <='" + dayEnd_S + "' AND UserGroupNo ='" + userGroup + "' " ;
        if (!"ALL".equals(devName)) {
            sql = sql + " AND dpi.name = '" + devName + "' ";
	}        
            sql = sql +	" GROUP BY date_format(R_EndTime,'%Y-%m-%d %H:%i') order by R_EndTime ;";
        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);
        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        MapOperate mp = new MapOperate();
        Map<String, String> fieldNameS = new HashMap<String, String>();
        if (traffSrc.equals("VoIP_Sig_Traffic")) {
            fieldNameS.put("1", "H323_Sig_Traffic");
            fieldNameS.put("2", "SIP_Sig_Traffic");
            fieldNameS.put("3", "MGCP_Sig_Traffic");
            if (!"ALL".equals(sigProt)) {
                String fieldValue = fieldNameS.get(sigProt);
                fieldNameS.clear();
                fieldNameS.put(sigProt, fieldValue);
            }
        } else {
            fieldNameS.put("1", "Rtp_Media_Traffic");
            fieldNameS.put("2", "Other_Media_Traffic");
            if (!"ALL".equals(mediaProt)) {
                String fieldValue = fieldNameS.get(mediaProt);
                fieldNameS.clear();
                fieldNameS.put(mediaProt, fieldValue);
            }
        }

        while (rs.next()) {
            for (Map.Entry<String, String> entry : fieldNameS.entrySet()) {
                String AppName = entry.getKey();
                if (flowDataMap.containsKey(AppName)) {
                    Map<String, Double> dataMap = flowDataMap.get(AppName);
                    String EndTime = rs.getString("R_EndTime");
                    String StdEndTime = getStdMinByTime(EndTime);
                    //将时间转换到最近的整5分钟
                    if (dataMap.containsKey(StdEndTime)) {
                        double result = mp.add(rs.getDouble(entry.getValue()), dataMap.get(StdEndTime));
                        dataMap.put(StdEndTime, result);
                    } else {
                        dataMap.put(StdEndTime, rs.getDouble(entry.getValue()));
                    }
                    flowDataMap.put(AppName, dataMap);
                } else {
                    Map<String, Double> dataMap2 = new HashMap<String, Double>();
                    dataMap2.put(getStdMinByTime(rs.getString("R_EndTime")), rs.getDouble(entry.getValue()));
                    flowDataMap.put(AppName, dataMap2);
                }
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        Map<String, Map<String, Double>> fillDataMap = mp.fillMapDataByMin(date1, flowDataMap);
        String proValue = "";
        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()) {
            JSONArray arr1_2 = new JSONArray();
            if (traffSrc.equals("VoIP_Sig_Traffic")) {
                if ("1".equals(entry.getKey())) {
                    proValue = "H.323";
                } else if ("2".equals(entry.getKey())) {
                    proValue = "SIP";
                } else if ("3".equals(entry.getKey())) {
                    proValue = "MGCP";
                } else {
                    proValue = entry.getKey();
                }
                proValue = proValue + "信令协议";
            } else {
                if ("1".equals(entry.getKey())) {
                    proValue = "RTP";
                } else if ("2".equals(entry.getKey())) {
                    proValue = "其它";
                } else {
                    proValue = entry.getKey();
                }
                proValue = proValue + "媒体协议";
            }
            arr1_2.add(proValue);
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            arr1.add(arr1_2);
        }
        System.out.println("arr1:::==" + arr1.toString());
        return arr1;
    }

    //Voip协议产生的总流量趋势(粒度：天)
    public JSONArray getVoipFlowTrd_D(String date1, String date2, String userGroup, String mediaProt, String sigProt, String traffSrc, String devName) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());
        super.getConnection();

        String sql = "SELECT date_format(R_StartTime,'%Y-%m-%d') R_StatDate,SUM(Rtp_Media_Traffic) Rtp_Media_Traffic,SUM(Other_Media_Traffic) Other_Media_Traffic, \n "
                + " SUM(H323_Sig_Traffic) H323_Sig_Traffic,SUM(SIP_Sig_Traffic) SIP_Sig_Traffic,SUM(MGCP_Sig_Traffic) MGCP_Sig_Traffic  \n"
                + " FROM voip_flow_stat voip LEFT JOIN dpiendpointbean dpi on dpi.masterIp = voip.EquipIp WHERE date_format(R_StartTime,'%Y/%m/%d') >='" + date1 + "' AND R_EndTime <='" + dayEnd_S + "' AND UserGroupNo ='" + userGroup + "' " ;
        if (!"ALL".equals(devName)) {
		sql = sql + " AND dpi.name = '" + devName + "'";
	}
            sql = sql + " GROUP BY date_format(R_StartTime,'%Y-%m-%d') order by R_StatDate ";
        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);
        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        MapOperate mp = new MapOperate();
        Map<String, String> fieldNameS = new HashMap<String, String>();
        if (traffSrc.equals("VoIP_Sig_Traffic")) {
            fieldNameS.put("1", "H323_Sig_Traffic");
            fieldNameS.put("2", "SIP_Sig_Traffic");
            fieldNameS.put("3", "MGCP_Sig_Traffic");
            if (!"ALL".equals(sigProt)) {
                String fieldValue = fieldNameS.get(sigProt);
                fieldNameS.clear();
                fieldNameS.put(sigProt, fieldValue);
            }
        } else {
            fieldNameS.put("1", "Rtp_Media_Traffic");
            fieldNameS.put("2", "Other_Media_Traffic");
            if (!"ALL".equals(mediaProt)) {
                String fieldValue = fieldNameS.get(mediaProt);
                fieldNameS.clear();
                fieldNameS.put(mediaProt, fieldValue);
            }
        }
        while (rs.next()) {
            for (Map.Entry<String, String> entry : fieldNameS.entrySet()) {
                String AppName = entry.getKey();
                if (flowDataMap.containsKey(AppName)) {
                    Map<String, Double> dataMap = flowDataMap.get(AppName);
                    dataMap.put(rs.getString("R_StatDate"), rs.getDouble(entry.getValue()));
                    flowDataMap.put(AppName, dataMap);
                } else {
                    Map<String, Double> dataMap2 = new HashMap<String, Double>();
                    dataMap2.put(rs.getString("R_StatDate"), rs.getDouble(entry.getValue()));
                    flowDataMap.put(AppName, dataMap2);
                }
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        Map<String, Map<String, Double>> fillDataMap = mp.fillMapDataBydate(date_1, dayEnd.getTime(), flowDataMap);
        String proValue = "";
        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()) {
            JSONArray arr1_2 = new JSONArray();
            if (traffSrc.equals("VoIP_Sig_Traffic")) {
                if ("1".equals(entry.getKey())) {
                    proValue = "H.323";
                } else if ("2".equals(entry.getKey())) {
                    proValue = "SIP";
                } else if ("3".equals(entry.getKey())) {
                    proValue = "MGCP";
                } else {
                    proValue = entry.getKey();
                }
                proValue = proValue + "信令协议";
            } else {
                if ("1".equals(entry.getKey())) {
                    proValue = "RTP";
                } else if ("2".equals(entry.getKey())) {
                    proValue = "其它";
                } else {
                    proValue = entry.getKey();
                }
                proValue = proValue + "媒体协议";
            }
            arr1_2.add(proValue);
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            System.out.println("arr1:::==" + arr1.toString());
            arr1.add(arr1_2);
        }
        return arr1;
    }

    //Voip协议产生的总流量占比
    public JSONArray getVoipFlowPie(String date1, String date2, String userGroup, String traffSrc, String gwIP, String gwKeeperIP,String devName ) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());
        super.getConnection();

        String pro_type = "VoIP_Media_Protocol";
        if (traffSrc.equals("VoIP_Sig_Traffic")) {
            pro_type = "VoIP_Sig_Protocol";
        }
        String sql = "SELECT " + pro_type + " , SUM(VoIP_Media_Traffic) VoIP_Media_Traffic,SUM(VoIP_Sig_Traffic) VoIP_Sig_Traffic "
                + "FROM voip_flow_stat voip LEFT JOIN dpiendpointbean dpi on dpi.masterIp = voip.EquipIp  WHERE date_format(R_StartTime,'%Y/%m/%d') >='" + date1 + "' AND R_EndTime <='" + dayEnd_S + "' AND UserGroupNo ='" + userGroup + "' ";
        if ((gwIP != null) && !"".equals(gwIP)) {
            sql = sql + " AND VoIPGW_IP LIKE '%" + gwIP + "%'";
        }
        if ((gwKeeperIP != null) && !"".equals(gwKeeperIP)) {
            sql = sql + " AND VoIPGWKeeper_IP LIKE '%" + gwKeeperIP + "%'";
        }
        if (!"ALL".equals(devName)) {
		sql = sql + " AND dpi.name = '" + devName + "'";
	}        
        sql = sql + "GROUP BY " + pro_type + ";";

        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);
        JSONArray arr1 = new JSONArray();
        String proValue = "";
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            String proNumericValue = rs.getString(pro_type);
            if (traffSrc.equals("VoIP_Sig_Traffic")) {
                if ("1".equals(proNumericValue)) {
                    proValue = "H.323";
                } else if ("2".equals(proNumericValue)) {
                    proValue = "SIP";
                } else if ("3".equals(proNumericValue)) {
                    proValue = "MGCP";
                } else {
                    proValue = proNumericValue;
                }
                proValue = proValue + "信令协议";
            } else {
                if ("1".equals(proNumericValue)) {
                    proValue = "RTP";
                } else if ("2".equals(proNumericValue)) {
                    proValue = "其它";
                } else {
                    proValue = proNumericValue;
                }
                proValue = proValue + "媒体协议";
            }
            arr1_2.add(proValue);
            arr1_2.add(rs.getDouble(traffSrc));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1::::::::=" + arr1.toString());
        return arr1;
    }

    //获取Voip类日志详情
    public JSONObject getVoipFlowLog(String date1, String date2, String userGroup,String devName,  int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "SELECT dpi.name dpiName, date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') AS R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') AS R_EndTime,ugrp.name,Rtp_Media_Traffic,Other_Media_Traffic , H323_Sig_Traffic ,SIP_Sig_Traffic, \n"
                + " MGCP_Sig_Traffic , VoIPGWNum,VoIPGWKeeperNum FROM voip_flow_stat voip left join dpiusergroup ugrp on voip.UserGroupNo=ugrp.id "
                + " LEFT JOIN dpiendpointbean dpi on dpi.masterIp = voip.EquipIp WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_S + "' AND UserGroupNo ='" + userGroup + "' ";
        String sql1 = "SELECT count(*) AS total_num   FROM voip_flow_stat voip LEFT JOIN dpiendpointbean dpi on dpi.masterIp = voip.EquipIp WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_S + "' AND UserGroupNo ='" + userGroup + "' ";
//        if (!"ALL".equals(mediaProt)) {
//            sql = sql + " AND VoIP_Media_Protocol= '" + mediaProt + "'";
//            sql1 = sql1 + " AND VoIP_Media_Protocol= '" + mediaProt + "'";
//        }
//        if (!"ALL".equals(sigProt)) {
//            sql = sql + " AND VoIP_Sig_Protocol= '" + sigProt + "'";
//            sql1 = sql1 + " AND VoIP_Sig_Protocol= '" + sigProt + "'";
//        }
//        if ((gwIP != null) && !"".equals(gwIP)) {
//            sql = sql + " AND VoIPGW_IP LIKE '%" + gwIP + "%'";
//            sql1 = sql1 + " AND VoIPGW_IP LIKE '%" + gwIP + "%'";
//        }
//        if ((gwKeeperIP != null) && !"".equals(gwKeeperIP)) {
//            sql = sql + " AND VoIPGWKeeper_IP LIKE '%" + gwKeeperIP + "%'";
//            sql1 = sql1 + " AND VoIPGWKeeper_IP LIKE '%" + gwKeeperIP + "%'";
//        }
        if (!"ALL".equals(devName)) {
            sql = sql + " AND dpi.name = '" + devName + "'";
            sql1 = sql1 + " AND dpi.name = '" + devName + "'";
        }
        sql = sql + " limit " + iDisplayStart + "," + iDisplayLength;
        System.out.println("sql1111=" + sql);

        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("dpiName"));            
            arr1_2.add(rs.getString("R_StartTime"));
            arr1_2.add(rs.getString("R_EndTime"));
            arr1_2.add(rs.getString(4));
            arr1_2.add(rs.getString(5));
            arr1_2.add(rs.getString(6));
            arr1_2.add(rs.getString(7));
            arr1_2.add(rs.getString(8));
            arr1_2.add(rs.getString(9));
            arr1_2.add(rs.getString(10));
            arr1_2.add(rs.getString(11));
            arr1.add(arr1_2);
        }
        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }
        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数
        System.out.println("DataObj::::::::=" + DataObj.toString());
        super.closeResource();
        return DataObj;
    }

    //获取Voip类网关日志详情
    public JSONObject getVoipGWLog(String date1, String date2, String userGroup,String devName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "SELECT dpi.name dpiName,date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') AS R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') AS R_EndTime,ugrp.name,\n"
                + "gw.VoIPGW_IPLength,gw.VoIPGW_IP,gw.TotalCallSessions,gw.CallSessionsConcurrent,gw.TotalCallDuration FROM voip_flow_stat voip left join dpiusergroup ugrp on voip.UserGroupNo=ugrp.id \n"
                + "LEFT JOIN voipgw_log gw on voip.Voip_Id=gw.Voip_Id LEFT JOIN dpiendpointbean dpi on dpi.masterIp = voip.EquipIp WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_S + "' AND UserGroupNo ='" + userGroup + "' ";
        String sql1 = "SELECT count(*) AS total_num   FROM voip_flow_stat voip LEFT JOIN voipgw_log gw on voip.Voip_Id=gw.Voip_Id LEFT JOIN dpiendpointbean dpi on dpi.masterIp = voip.EquipIp WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_S + "' AND UserGroupNo ='" + userGroup + "' ";
        if (!devName.equals("ALL")) {
            sql = sql + " AND dpi.name = '" + devName + "' ";
            sql1 = sql1 + " AND dpi.name ='" + devName + "' ";
        }

//        if ((gwIP != null) && !"".equals(gwIP)) {
//            sql = sql + " AND VoIPGW_IP LIKE '%" + gwIP + "%'";
//            sql1 = sql1 + " AND VoIPGW_IP LIKE '%" + gwIP + "%'";
//        }
//        if ((gwKeeperIP != null) && !"".equals(gwKeeperIP)) {
//            sql = sql + " AND VoIPGWKeeper_IP LIKE '%" + gwKeeperIP + "%'";
//            sql1 = sql1 + " AND VoIPGWKeeper_IP LIKE '%" + gwKeeperIP + "%'";
//        }

        sql = sql + " limit " + iDisplayStart + "," + iDisplayLength;
        System.out.println("sql1111=" + sql);

        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("dpiName"));
            arr1_2.add(rs.getString("R_StartTime"));
            arr1_2.add(rs.getString("R_EndTime"));
            arr1_2.add(rs.getString(4));
            arr1_2.add(rs.getString(5));
            arr1_2.add(rs.getString(6));
            arr1_2.add(rs.getString(7));
            arr1_2.add(rs.getString(8));
            arr1_2.add(rs.getString(9));
            arr1.add(arr1_2);
        }
        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }
        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数
        System.out.println("DataObj::::::::=" + DataObj.toString());
        super.closeResource();
        System.out.println("DataObj:::==" + DataObj.toString());
        return DataObj;
    }

    //获取Voip类网守被叫日志详情
    public JSONObject getVoipCalledLog(String date1, String date2, String userGroup,String devName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "SELECT dpi.name dpiName,date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') AS R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') AS R_EndTime,ugrp.name,\n"
                + "kp.VoIPGWKeeper_IPLength,kp.VoIPGWKeeper_IP,cl.CalledNumber_Length,cl.CalledNumber,cl.CalledDuration FROM voip_flow_stat voip left join dpiusergroup ugrp on voip.UserGroupNo=ugrp.id \n"
                + "LEFT JOIN voipgwkeeper_log kp on voip.Voip_Id=kp.Voip_Id LEFT JOIN voipCalled_log cl on cl.VoipGWKeeper_Id=kp.VoipGWKeeper_Id "
                + "LEFT JOIN dpiendpointbean dpi on dpi.masterIp = voip.EquipIp  WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_S + "' AND UserGroupNo ='" + userGroup + "' ";
        String sql1 = "SELECT count(*) AS total_num   FROM voip_flow_stat voip LEFT JOIN voipgwkeeper_log kp on voip.Voip_Id=kp.Voip_Id LEFT JOIN voipCalled_log cl on cl.VoipGWKeeper_Id=kp.VoipGWKeeper_Id LEFT JOIN dpiendpointbean dpi on dpi.masterIp = voip.EquipIp WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_S + "' AND UserGroupNo ='" + userGroup + "' ";
//        if (!"ALL".equals(mediaProt)) {
//            sql = sql + " AND VoIP_Media_Protocol= '" + mediaProt + "'";
//            sql1 = sql1 + " AND VoIP_Media_Protocol= '" + mediaProt + "'";
//        }
//        if (!"ALL".equals(sigProt)) {
//            sql = sql + " AND VoIP_Sig_Protocol= '" + sigProt + "'";
//            sql1 = sql1 + " AND VoIP_Sig_Protocol= '" + sigProt + "'";
//        }
//        if ((gwIP != null) && !"".equals(gwIP)) {
//            sql = sql + " AND VoIPGW_IP LIKE '%" + gwIP + "%'";
//            sql1 = sql1 + " AND VoIPGW_IP LIKE '%" + gwIP + "%'";
//        }
//        if ((gwKeeperIP != null) && !"".equals(gwKeeperIP)) {
//            sql = sql + " AND VoIPGWKeeper_IP LIKE '%" + gwKeeperIP + "%'";
//            sql1 = sql1 + " AND VoIPGWKeeper_IP LIKE '%" + gwKeeperIP + "%'";
//        }
        if (!"ALL".equals(devName)) {
            sql = sql + " AND dpi.name = '" + devName + "'";
            sql1 = sql1 + " AND dpi.name = '" + devName + "'";
        }
        sql = sql + " limit " + iDisplayStart + "," + iDisplayLength;
        System.out.println("sql1111=" + sql);

        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("dpiName"));        
            arr1_2.add(rs.getString("R_StartTime"));
            arr1_2.add(rs.getString("R_EndTime"));
            arr1_2.add(rs.getString(4));
            arr1_2.add(rs.getString(5));
            arr1_2.add(rs.getString(6));
            arr1_2.add(rs.getString(7));
            arr1_2.add(rs.getString(8));
            arr1_2.add(rs.getString(9));
            arr1.add(arr1_2);
        }
        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }
        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数
        System.out.println("DataObj::::::::=" + DataObj.toString());
        super.closeResource();
        System.out.println("DataObj:::==" + DataObj.toString());
        return DataObj;
    }

    //获取Voip类网守主叫日志详情
    public JSONObject getVoipCallerLog(String date1, String date2, String userGroup, String devName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "SELECT dpi.name dpiName,date_format(R_StartTime,'%Y-%m-%d %H:%i:%s') AS R_StartTime, date_format(R_EndTime,'%Y-%m-%d %H:%i:%s') AS R_EndTime,ugrp.name,\n"
                + "kp.VoIPGWKeeper_IPLength,kp.VoIPGWKeeper_IP,cl.CallerNumber_Length,cl.CallerNumber,cl.CallerDuration FROM voip_flow_stat voip left join dpiusergroup ugrp on voip.UserGroupNo=ugrp.id \n"
                + "LEFT JOIN voipgwkeeper_log kp on voip.Voip_Id=kp.Voip_Id LEFT JOIN voipCaller_log cl on cl.VoipGWKeeper_Id=kp.VoipGWKeeper_Id "
                + "LEFT JOIN dpiendpointbean dpi on dpi.masterIp = voip.EquipIp WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_S + "' AND UserGroupNo ='" + userGroup + "' ";
        String sql1 = "SELECT count(*) AS total_num   FROM voip_flow_stat voip LEFT JOIN voipgwkeeper_log kp on voip.Voip_Id=kp.Voip_Id LEFT JOIN voipCaller_log cl on cl.VoipGWKeeper_Id=kp.VoipGWKeeper_Id "
                + "LEFT JOIN dpiendpointbean dpi on dpi.masterIp = voip.EquipIp WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_S + "' AND UserGroupNo ='" + userGroup + "' ";
        if (!"ALL".equals(devName)) {
            sql = sql + " AND dpi.name = '" + devName + "'";
            sql1 = sql1 + " AND dpi.name = '" + devName + "'";
        }
        
        sql = sql + " limit " + iDisplayStart + "," + iDisplayLength;
        System.out.println("sql1111=" + sql);

        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("dpiName"));
            arr1_2.add(rs.getString("R_StartTime"));
            arr1_2.add(rs.getString("R_EndTime"));
            arr1_2.add(rs.getString(4));
            arr1_2.add(rs.getString(5));
            arr1_2.add(rs.getString(6));
            arr1_2.add(rs.getString(7));
            arr1_2.add(rs.getString(8));
            arr1_2.add(rs.getString(9));
            arr1.add(arr1_2);
        }
        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }
        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数
        System.out.println("DataObj::::::::=" + DataObj.toString());
        super.closeResource();
        System.out.println("DataObj:::==" + DataObj.toString());
        return DataObj;
    }

    public static void main(String[] agrs) {
        List<String> devList = new ArrayList<String>();
        JSONArray arr1 = new JSONArray();
        JSONArray arr1_2 = new JSONArray();
        arr1_2.add("id1");
        arr1_2.add("name1");
        arr1.add(arr1_2);
        arr1_2 = new JSONArray();
        arr1_2.add("id2");
        arr1_2.add("name3");
        arr1.add(arr1_2);
        for (int i = 0; i < arr1.size(); i++) {
            devList.add(arr1.getJSONArray(i).getString(1));
        }
        System.out.println("devList:::==" + devList.toString());
    }
}
