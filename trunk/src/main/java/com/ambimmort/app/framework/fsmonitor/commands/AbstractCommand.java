package com.ambimmort.app.framework.fsmonitor.commands;

import org.apache.commons.cli.*;
import org.codehaus.plexus.util.cli.CommandLineUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

/**
 * Created by hedingwei on 2015/1/15.
 */
public abstract class AbstractCommand {

    private PrintWriter printWriter = null;

    private Options opt = new Options();

    public abstract String getName();

    public String getSyntax() {
        return "echo";
    }

    public abstract void prepareOption(Options options) throws Throwable;

    public abstract void execute(CommandLine commandLine, Options options) throws Throwable;

    public void start(String argsStr) {
        try {
            printWriter = new PrintWriter(new FileOutputStream(new File("run/command.stdout")));
            String[] args = null;
            args = CommandLineUtils.translateCommandline(argsStr);
            CommandLineParser parser = new PosixParser();
            prepareOption(opt);
            CommandLine cl = parser.parse(opt, args);
            if (cl != null) execute(cl, opt);
        } catch (Throwable e) {
            printHelp();
            if(printWriter!=null){
                printWriter.println(e.toString());
            }
        }finally {
            if(printWriter!=null) printWriter.close();
        }

    }

    protected void printHelp(){
        HelpFormatter f = new HelpFormatter();
        f.printHelp(printWriter,74,getSyntax(),null,opt,1,3,null,false);
        f.printHelp(getSyntax(),opt);
    }

    protected void println(String s) throws Throwable{
        printWriter.println(s);
    }

}
