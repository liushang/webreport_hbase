
package cn.net.greenet.visitapp.servlet;

import cn.net.greenet.service.DataService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "VisitAppUserTrdServlet", urlPatterns = {"/VisitApp/VisitAppUserTrd"})
public class VisitAppUserTrdServlet extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        String date1 = request.getParameter("date1");
        String AppType = request.getParameter("AppType");
        String SM_AppType = request.getParameter("SM_AppType"); 
        System.out.println("SM_AppType:"+SM_AppType);
        String devid = request.getParameter("devid");
        
  // System.out.println("22222222date1="+date1+ "SRC_DEST="+SRC_DEST + "AppType="+AppType+ "SM_AppType="+SM_AppType +"AppTraff_FD="+AppTraff_FD);

        try {
            // 链接数据库获取数据
            JSONArray serial = new DataService().getVisitAppUserTrd(date1,AppType,SM_AppType,devid);
            out.println(serial);
        } catch (Exception ex) {
            Logger.getLogger(VisitAppUserTrdServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
