/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.net.greenet.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Ambimmort
 */
public class MapOperate {
    
    public Map<String,Map<String,Double>> fillMapDataBydate(Date date_1,Date date_2,Map<String,Map<String,Double>> flowDataMap) {
        Map<String,Map<String,Double>> newMap = flowDataMap;
        for (Map.Entry<String,Map<String,Double>> entry : flowDataMap.entrySet()) {
            String key = entry.getKey() ;
            Map<String,Double> map = entry.getValue();
            Date date_tmp = date_1;
            while(date_tmp.before(date_2)){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String dateStr = sdf.format(date_tmp);
                if(!map.containsKey(dateStr)){
                    map.put(dateStr,new Double(0));
                }
                Calendar calendar  = Calendar.getInstance();
                calendar.setTime(date_tmp);
                calendar.add(calendar.DATE,1);
                date_tmp=calendar.getTime();
            }
            newMap.put(key, map);
        }
        return newMap;
    }
    
    public Map<String,Double> fillMapData(Date date_1,Date date_2,Map<String,Double> flowDataMap) {
        
            Date date_tmp = date_1;
            while(date_tmp.before(date_2)){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String dateStr = sdf.format(date_tmp);
                if(!flowDataMap.containsKey(dateStr)){
                    flowDataMap.put(dateStr,new Double(0));
                }
                Calendar calendar  = Calendar.getInstance();
                calendar.setTime(date_tmp);
                calendar.add(calendar.DATE,1);
                date_tmp=calendar.getTime();
            }
        return flowDataMap;
    }
    //填充某天中缺失时间的数据
    public Map<String,Map<String,Double>> fillMapDataByMin(String date_str,Map<String,Map<String,Double>> flowDataMap) throws ParseException {
        Map<String,Map<String,Double>> newMap = flowDataMap;
        for (Map.Entry<String,Map<String,Double>> entry : flowDataMap.entrySet()) {
            String key = entry.getKey() ;
            Map<String,Double> map = entry.getValue();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            SimpleDateFormat sdf_hm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date date_tmp = sdf.parse(date_str);
            int section = 24*60/5;
            for(int i=0;i<section;i++){
                Date afterDate = new Date(date_tmp.getTime()+300000);
                date_tmp = afterDate ;
                String dateStr = sdf_hm.format(date_tmp);
                if(!map.containsKey(dateStr)){
                    map.put(dateStr,new Double(0));
                }
            }
            newMap.put(key, map);
        }
        return newMap;
    }
    
    //填充某天中缺失时间的数据
    public Map<String,Double> fillMapKeyByMin(String date_str,Map<String,Map<String,Double>> flowDataMap,int  sum_avg) throws ParseException {
        Map<String,Double> newMap = new HashMap<String,Double>();
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat sdf_hm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date_tmp = sdf.parse(date_str);
        int section = 24*60/5;
        for(int i=0;i<section;i++){
            Date afterDate = new Date(date_tmp.getTime()+300000);
            date_tmp = afterDate ;
            String dateStr = sdf_hm.format(date_tmp);
            if(!flowDataMap.containsKey(dateStr)){
                Map<String,Double> dataMap2 =  new HashMap<String,Double>();
                dataMap2.put(dateStr,new Double(0));
                flowDataMap.put(dateStr,dataMap2);
            }
        }       
        for (Map.Entry<String,Map<String,Double>> entry : flowDataMap.entrySet()) {
            String key = entry.getKey() ;
            Map<String,Double> map = entry.getValue();
            newMap.put(key, getSumAvgMap(map , sum_avg));
        }

        return newMap;
    }
    
    public double getSumAvgMap(Map<String,Double> dateMap ,int sum_avg){
        double val =0 ;
        Object[] unsort_key = dateMap.keySet().toArray();
        Arrays.sort(unsort_key);
        for (int i = 0; i < unsort_key.length; i++) {
            val = add(val,dateMap.get(unsort_key[i])) ;
        }
        if(sum_avg==2){
            val = val/dateMap.size();
        }
        return val;
    }
    
    public String getStrByMap(Map<String,Double> dateMap){
        Object[] unsort_key = dateMap.keySet().toArray();
        Arrays.sort(unsort_key);
        String str="[" ;
        for (int i = 0; i < unsort_key.length; i++) {
             str = str + dateMap.get(unsort_key[i])+",";
        }
        if(unsort_key.length>0){
            str = str.substring(0, str.length()-1);
        }       
        str = str +"]";
        return str;
    }
    
    public double add(double v1,double v2){

          BigDecimal b1 = new BigDecimal(v1);
          BigDecimal b2 = new BigDecimal(v2);
          DecimalFormat ddf = new DecimalFormat( "#0.0000");        
          return  Double.valueOf(ddf.format(b1.add(b2).doubleValue()));
    }
    
     public static void main(String[] args) throws ParseException {
                
            MapOperate mp = new MapOperate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            Date date_1 = sdf.parse("2014/08/01");
            Date date_2 = sdf.parse("2014/08/10");
            Calendar dayEnd = Calendar.getInstance();
            dayEnd.setTime(date_2);
            dayEnd.set(Calendar.HOUR, 23);
            dayEnd.set(Calendar.MINUTE, 59);
            dayEnd.set(Calendar.SECOND, 59);
            Map<String,Map<String,Double>> flowDataMap = new HashMap<String,Map<String,Double>>();
            Map<String,Double> mapdata = new HashMap<String,Double>();
//            mapdata.put("2014-08-02 01:00", new Float(5.5));
//            mapdata.put("2014-08-02 10:00", new Float(100));
            flowDataMap.put("bbbb", mapdata);
            Map<String,Map<String,Double>> flowDataMap2 =mp.fillMapDataByMin("2014/08/02", flowDataMap);   
            for (Map.Entry<String,Map<String,Double>> entry : flowDataMap2.entrySet()) {
                System.out.println(entry.getKey()+ mp.getStrByMap(entry.getValue()) );
                
            }    
        }
    
}
