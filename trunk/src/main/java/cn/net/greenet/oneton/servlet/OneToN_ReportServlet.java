/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.net.greenet.oneton.servlet;

import cn.net.greenet.ddos.servlet.AttackLogDetailsServlet;
import cn.net.greenet.service.DataService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Ambimmort
 */
@WebServlet(name = "OneToN_ReportServlet", urlPatterns = {"/OneToN/OneToN_Report"})
public class OneToN_ReportServlet extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
//        String UserAcc = new String(Base64.decodeBase64(request.getParameter("UserAcc")),"utf-8");
//        String UserIP = new String(Base64.decodeBase64(request.getParameter("UserIP")),"utf-8"); 
        
        Base64 base64 = new Base64();
        byte[] accBytes = base64.decode(request.getParameter("UserAcc"));
        String UserAcc = new String(accBytes, "UTF-8");
        byte[] ipBytes = base64.decode(request.getParameter("UserIP"));
        String UserIP = new String(ipBytes, "UTF-8");
        
        String ShareTerm_S = request.getParameter("ShareTerm_S");
        String ShareTerm_E = request.getParameter("ShareTerm_E");
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");      
        String iDisplayStart = request.getParameter("iDisplayStart");
        String iDisplayLength = request.getParameter("iDisplayLength");
        String devid =request.getParameter("devid");
        int report_type =Integer.parseInt(request.getParameter("report_type"));

        System.out.println("22222UserAcc:"+UserAcc+"date1:"+date1+"date2:"+date2
                +"ShareTerm_S:"+ShareTerm_S+"ShareTerm_E:"+ShareTerm_E+"UserIP:"+UserIP+"iDisplayStart:"+iDisplayStart+"iDisplayLength:"+iDisplayLength);
   
        System.out.println("report_type:::::"+report_type );

            try {
                // 链接数据库获取数据
                JSONObject serial = new DataService().getOneToN_Report( UserAcc,UserIP,ShareTerm_S,ShareTerm_E,date1,date2,devid, Integer.parseInt(iDisplayStart),Integer.parseInt(iDisplayLength) ,report_type );
                out.println(serial);
            } catch (Exception ex) {
                Logger.getLogger(AttackLogDetailsServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
            }
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
