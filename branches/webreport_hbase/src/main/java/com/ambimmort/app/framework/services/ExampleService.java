/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ambimmort.app.framework.services;

import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;

@Controller
@RequestMapping("/example")
public class ExampleService {

    private static Logger logger = Logger.getLogger(ExampleService.class);

    @RequestMapping(value = "/helloworld", method = RequestMethod.GET)
    @ResponseBody
    public JSONObject helloworld(@RequestParam(value = "name",required = false) String name,@RequestParam(value="age", required = true) int age) throws IOException {
        JSONObject obj = new JSONObject();
        obj.put("msg","hello world!ABC");
        obj.put("name",name);
        obj.put("age",age) ;
        obj.put("p",new File(".").getAbsolutePath());
        return obj;
    }

    @RequestMapping(value = "/helloworld1", method = RequestMethod.GET)
    @ResponseBody
    public JSONObject helloworld1() throws IOException {
        JSONObject obj = new JSONObject();
        obj.put("msg","hello world!ABC");
        obj.put("p",new File(".").getAbsolutePath());
        return obj;
    }

}
