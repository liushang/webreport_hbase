#!/bin/sh
export PROJECT_HOME=`pwd`
export ANT_HOME=${PROJECT_HOME}/application/tools/ant
export MAVEN_HOME=${PROJECT_HOME}/application/tools/maven
export APP_HOME=${PROJECT_HOME}/application
export PATH=.:${ANT_HOME}/bin:${MAVEN_HOME}/bin:${PATH}
export release=${PROJECT_HOME}/application/tools/ant/bin
chmod 755 ${ANT_HOME}/bin/*
chmod 755 ${MAVEN_HOME}/bin/*
chmod 755 ${APP_HOME}/bin/*
${release}/ant release