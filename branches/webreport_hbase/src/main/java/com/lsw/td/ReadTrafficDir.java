package com.lsw.td;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

import com.lsw.other.DayList;

public class ReadTrafficDir {
	private Configuration config;
	private HTable table;
	private HBaseAdmin admin;
	
	public ReadTrafficDir(String masterIp,String masterPort,String zkIp,String zkPort) {
		config = HBaseConfiguration.create();
		config.set("hbase.master", masterIp + ":" + masterPort);
		config.set("hbase.zookeeper.property.clientPort", zkPort);
		config.set("hbase.zookeeper.quorum", zkIp);

		try {
			table = new HTable(config, Bytes.toBytes("traffdir_hour"));
			admin = new HBaseAdmin(config);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public JSONArray TDtraffDirL2Hbase(String date, 
			String src, String dest, String appType,
			List<String> sm_AppTypeList, String updown, List<String> devid)
			throws IOException {
//		DayList dy = new DayList();
		String hourArr[] ={":00",":01",":02",":03",":04",":05",":06",":07",":08",":09",":10",":11",":12",":13",":14",":15",":16",":17",":18",":19",":20",":21",":22",":23"};

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//		List<Date> lDate = dy.findDates(date1, date2);
		JSONArray jsonArray = new JSONArray();
		JSONArray sm_jsonArray = new JSONArray();
		for (String value : sm_AppTypeList) {
			Map<String, Double> versionMap = new HashMap<String, Double>();
			for (int i = 0; i <= hourArr.length-1; i++) {
				System.out.println(hourArr[i]);
				versionMap.put(date+hourArr[i], 0.0);
			}
			String sm_appName = null;
			for (String dev : devid) {
				List<Get> getList = new ArrayList<Get>();
				Result[] rst = null;
				for (String h:hourArr) {
					String rowkey = date+" "+h + "\001"
							+ src + "\001" + dest + "\001" + appType + "\001"
							+ value + "\001" + dev;
					System.out.println("*****rowkey----->" + rowkey);
					Get get = new Get(Bytes.toBytes(rowkey));
					get.addColumn(Bytes.toBytes("cf"),
							Bytes.toBytes(updown));
					getList.add(get);
				}
				rst = table.get(getList);
				System.out.println("*****rst  Length:" + rst.length);
				for (int i = 0; i < rst.length - 1; i++) {
					
					for (KeyValue kv : rst[i].raw()) {
						System.out.println("rowkey is :"
								+ Bytes.toString(kv.getRow()));
						String[] arr2 = Bytes.toString(kv.getRow()).split(
								"\001");
						sm_appName = arr2[4];
						System.out.println("Value:"+Double.parseDouble((Bytes.toString(kv
								.getValue()))));
						versionMap.put(
								arr2[0],
								versionMap.get(arr2[0])
										+ Double.parseDouble((Bytes.toString(kv
												.getValue()))));
						System.out.println("***this is the test output***");
						System.out.print(Bytes.toString(kv.getKey()) + "  ");
						System.out.print(arr2[0] + "  ");
						System.out
								.println(Bytes.toString(kv.getValue()) + "  ");
						System.out.println("***this is the test output***");
					}
				}
			}
			JSONArray jsonarr = new JSONArray();
			if (sm_appName != null) {
				jsonarr.add(sm_appName);
				for (int i = 0; i <= hourArr.length - 1; i++) {
					sm_jsonArray.add(versionMap.get(hourArr[i]));
				}
				jsonarr.add(sm_jsonArray);
				sm_jsonArray.clear();
				jsonArray.add(jsonarr);
			}

		}

		return jsonArray;
	}
	
	public JSONArray TDtraffDirL2_SumHbase(String date,
			String src, String dest, String appType,
			List<String> sm_AppTypeList, String appTraffic_Field,
			List<String> devid) throws IOException {
		String hourArr[] ={":00",":01",":02",":03",":04",":05",":06",":07",":08",":09",":10",":11",":12",":13",":14",":15",":16",":17",":18",":19",":20",":21",":22",":23"};

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, Double> versionMap = new HashMap<String, Double>();
		for (int i = 0; i <= hourArr.length- 1; i++) {
			versionMap.put(date+hourArr[i], 0.0);
		}
		JSONArray jsonArray = new JSONArray();
		JSONArray sm_jsonArray = new JSONArray();
		// JSONObject jsonObject = new JSONObject();
		Result[] rst = null;
		String appNameCurrent = null;
		for (String value : sm_AppTypeList) {
			for (String dev : devid) {
				List<Get> getList = new ArrayList<Get>();
				for (String h : hourArr) {
					// concat the rowkey with the input field
					String rowkey = date+" "+h+ "\001"
							+ src + "\001" + dest + "\001" + appType + "\001"
							+ value + "\001" + dev;
					Get get = new Get(Bytes.toBytes(rowkey));
					get.addColumn(Bytes.toBytes("cf"),
							Bytes.toBytes(appTraffic_Field));
					getList.add(get);
				}
				rst = table.get(getList);

				if (rst != null) {
					for (int i = 0; i < rst.length - 1; i++) {
						for (KeyValue kv : rst[i].raw()) {
							String arr2[] = Bytes.toString(kv.getRow()).split(
									"\001");
							appNameCurrent = arr2[3];
							versionMap.put(
									arr2[0],
									versionMap.get(arr2[0])
											+ Double.parseDouble((Bytes
													.toString(kv.getValue()))));
							System.out.println("###this is the test output###");
							System.out
									.print(Bytes.toString(kv.getKey()) + "  ");
							System.out.print(arr2[0] + "  ");
							System.out.println(Bytes.toString(kv.getValue())
									+ "  ");
							System.out
									.println("######this is the test output########");
						}
					}
				}

			}
		}
		// jsonObject.put("appName", appType);
		if (appNameCurrent != null) {
			for (int i = 0; i <= hourArr.length - 1; i++) {
				sm_jsonArray.add(versionMap.get(hourArr[i]));
			}
			// jsonObject.put(appType, sm_jsonArray);
			// jsonArray.add(appTypeName);
			jsonArray.add(appNameCurrent);
			jsonArray.add(sm_jsonArray);
		}
		return jsonArray;
	}

	
	public JSONArray TDtraffDirPieHbase(String date,
			String src, String dest, String appType, List<String> appNames,
			String appTraffic_Field, List<String> devid) throws IOException {
		JSONArray jsonArrayPie = new JSONArray();
		String hourArr[] ={":00",":01",":02",":03",":04",":05",":06",":07",":08",":09",":10",":11",":12",":13",":14",":15",":16",":17",":18",":19",":20",":21",":22",":23"};


		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		for (String appName : appNames) {
			JSONArray sm_jsonArrayPie = new JSONArray();
			System.out.println(appName);
			double sum = 0.0;
			String sm_appName = null;
			for (String dev : devid) {
				Result[] rs = null;
				List<Get> getList = new ArrayList<Get>();
				for (String h : hourArr) {
					String rowkey = date+" "+h + "\001"
							+ src + "\001" + dest + "\001" + appType + "\001"
							+ appName + "\001" + dev;
					System.out.println("rowkey" + rowkey);
					Get get = new Get(Bytes.toBytes(rowkey));

					get.addColumn(Bytes.toBytes("cf"),
							Bytes.toBytes(appTraffic_Field));
					getList.add(get);
				}
				rs = table.get(getList);

				System.out.println("PieHbase  :" + rs.toString());
				for (int i = 0; i < rs.length - 1; i++) {
					for (KeyValue kv : rs[i].raw()) {
						String[] arr2 = Bytes.toString(kv.getRow()).split(
								"\001");
						sm_appName = arr2[4];
						sum += Double
								.parseDouble(Bytes.toString(kv.getValue()));
					}
				}

				System.out.print(appName + ":" + sum + "    ");

			}
			if (sm_appName != null) {
				sm_jsonArrayPie.add(sm_appName);
				sm_jsonArrayPie.add(sum);
				jsonArrayPie.add(sm_jsonArrayPie);
			}

		}

		return jsonArrayPie;
	}
	

	public JSONArray  TDtraffDirPie_SumHbase(String date, 
			String src, String dest, Map<String, String> appMap,
			Map<String, List<String>> appTypeMap, String appTraffic_Field,
			List<String> devid) throws IOException {
		JSONArray jsonArrayPie = new JSONArray();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String hourArr[] ={":00",":01",":02",":03",":04",":05",":06",":07",":08",":09",":10",":11",":12",":13",":14",":15",":16",":17",":18",":19",":20",":21",":22",":23"};

		for (Map.Entry<String, List<String>> appEntry : appTypeMap.entrySet()) {
			double sum = 0.0;
			String appType = null;
			JSONArray sm_jsonArrayPie = new JSONArray();
			for (String appName : appEntry.getValue()) {
				for (String dev : devid) {
					Result[] rs = null;
					List<Get> getList = new ArrayList<Get>();
					for (String h:hourArr) {
						String rowkey = date+" "+h+ "\001" + src + "\001" + dest + "\001"
								+ appEntry.getKey() + "\001" + appName + "\001"
								+ dev;
						System.out.println("SumPierowkey:" + rowkey);
						Get get = new Get(Bytes.toBytes(rowkey));
						// get.getMaxVersions();
						get.addColumn(Bytes.toBytes("cf"),
								Bytes.toBytes(appTraffic_Field));
						// get.setTimeRange(0L, Long.MAX_VALUE);
						getList.add(get);
					}
					rs = table.get(getList);

					for (int i = 0; i < rs.length - 1; i++) {
						for (KeyValue kv : rs[i].raw()) {
							String[] arr2 = Bytes.toString(kv.getRow()).split(
									"\001");
							
							appType = arr2[3];
							sum += Double.parseDouble(Bytes.toString(kv
									.getValue()));
						}
					}

					System.out.print(appType + ":" + sum + "    ");

				}
			}
			if (appType != null) {
				sm_jsonArrayPie.add(appMap.get(appType));
				sm_jsonArrayPie.add(sum);
				jsonArrayPie.add(sm_jsonArrayPie);

			}

		}
		return jsonArrayPie;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

	}

}
