
package cn.net.greenet.voipflow.servlet;

import cn.net.greenet.service.DataService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "VoipFlowPieServlet", urlPatterns = {"/VoipFlow/VoipFlowPie"})
public class VoipFlowPieServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");
        String userGroup = request.getParameter("userGroup");
//        String mediaProt = java.net.URLDecoder.decode(request.getParameter("mediaProt"),"UTF-8");
//        String sigProt = java.net.URLDecoder.decode(request.getParameter("sigProt"),"UTF-8");
        String traffSrc = java.net.URLDecoder.decode(request.getParameter("traffSrc"),"UTF-8");
        String gwIP = java.net.URLDecoder.decode(request.getParameter("gwIP"),"UTF-8");
        String gwKeeperIP = java.net.URLDecoder.decode(request.getParameter("gwKeeperIP"),"UTF-8");
        String devName = request.getParameter("devName");
        try {
            // 链接数据库获取数据
            JSONArray serial = new DataService().getVoipFlowPie(date1,date2,userGroup,traffSrc, gwIP, gwKeeperIP,devName);
            out.println(serial);
        } catch (Exception ex) {
            Logger.getLogger(VoipFlowTrd_DServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
