package com.ambimmort.app.framework.fsmonitor;


import com.ambimmort.app.framework.fsmonitor.commands.AbstractCommand;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * Created by hedingwei on 2015/1/14.
 */
public class DefaultCommandActionListener extends FileAlterationListenerAdaptor {

    private SimpleDateFormat sdf  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private Map<String, AbstractCommand> commands = null;

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public void setCommands(Map<String, AbstractCommand> commands) {
        this.commands = commands;
    }

    @Override
    public void onFileChange(File file) {
        String name = file.getName();
        if(name.equals("command.action")){
            try {
                String var = FileUtils.readFileToString(file).trim();
                String[] m = var.split(" ");
                String command = m[0];
                String args = m[1]==null?"":m[1];
                if(commands.containsKey(command)){
                    commands.get(command).start(args);
                }else{
                    FileUtils.write(new File("run/command.stdout"),"WARNING: No Such Command:"+command,"utf-8",true);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
