package cn.net.greenet.util;

import java.io.FileInputStream;
import java.util.Properties;

public class Config {
	private static Config config;
	//properties.load(InputStream);读取属性文件
	private static Properties properties; 
	
	private Config(){
		String configFile="config/database.properties";
		properties=new Properties();
        try {
                properties.load(new FileInputStream(configFile));
                } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
	}
	
	public static Config getInstance(){
		if(config==null){
			config=new Config();
		}
		return config;
	}
	
	public String getString(String key) {
		return properties.getProperty(key);
	}
	
        public static void main(String []args){
                System.out.println(Config.getInstance().getString("jdbc.connection.url"));
        }


}
