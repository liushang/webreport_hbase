/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.net.greenet.generalflow.servlet;

import cn.net.greenet.service.DataService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;

/**
 *
 * @author Ambimmort
 */
@WebServlet(name = "GenFlowL3Servlet", urlPatterns = {"/GeneralFlow/GenFlowL3"})
public class GenFlowL3Servlet extends HttpServlet {


   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String userGroupNo = request.getParameter("userGroupNo");
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");
        String timeType = request.getParameter("timeType");
        
        String appType = java.net.URLDecoder.decode(request.getParameter("appType"),"UTF-8");
        String sm_AppType = java.net.URLDecoder.decode(request.getParameter("sm_AppType"),"UTF-8");
        String devName = java.net.URLDecoder.decode(request.getParameter("devName"),"UTF-8");

        String appTypeISO = new String(request.getParameter("appType").getBytes("ISO-8859-1"), "utf-8");
        String sm_AppTypeISO = new String(request.getParameter("sm_AppType").getBytes("ISO-8859-1"), "utf-8");
        String devNameISO = new String(request.getParameter("devName").getBytes("ISO-8859-1"), "utf-8");
        
        String appTypeOld = request.getParameter("appType");
        String sm_AppTypeOld = request.getParameter("sm_AppType");
        String devNameOld = request.getParameter("devName");      
        String filed = request.getParameter("filed");
        
 System.out.println("appType:="+appType);       
 System.out.println("sm_AppType:="+sm_AppType);        
 System.out.println("devName:="+devName);      
 System.out.println("appTypeOld:="+appTypeOld);       
 System.out.println("sm_AppTypeOld:="+sm_AppTypeOld);        
 System.out.println("devNameOld:="+devNameOld); 
  System.out.println("appTypeISO:="+appTypeISO);       
 System.out.println("sm_AppTypeISO:="+sm_AppTypeISO);        
 System.out.println("devNameISO:="+devNameISO); 

        if ((userGroupNo != null) && ( date1 !=null ) && ( date2 !=null ) && ( timeType !=null )) {
            try {
                // 链接数据库获取数据
                JSONArray serial = new DataService().getGenFlowL2Data(date1,date2,timeType,userGroupNo,appType,sm_AppType,filed,devName);
                out.println(serial);
            } catch (Exception ex) {
                Logger.getLogger(GenFlowL3Servlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
            }
        } else {
            try {
                // 链接数据库获取数据
                JSONArray serial = new JSONArray();
                out.println(serial);
            } catch (Exception ex) {
                Logger.getLogger(GenFlowL3Servlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
