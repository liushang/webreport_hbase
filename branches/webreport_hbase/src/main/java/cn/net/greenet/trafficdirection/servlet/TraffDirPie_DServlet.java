/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.net.greenet.trafficdirection.servlet;

import cn.net.greenet.service.DataService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;

/**
 *
 * @author Ambimmort
 */
@WebServlet(name = "TraffDirPie_DServlet", urlPatterns = {"/TrafficDirection/TraffDirPie_D"})
public class TraffDirPie_DServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");
        String src = request.getParameter("src");
        String dst = request.getParameter("dst");
        String appType = request.getParameter("appType");
        String sm_AppType = java.net.URLDecoder.decode(request.getParameter("sm_AppType"),"UTF-8"); 
        String devid = request.getParameter("devid");
        String filed = request.getParameter("filed");
        
      // System.out.println("========date1="+date1+"date2="+date2+ "SRC_DEST="+SRC_DEST + "AppType="+AppType +"AppTraff_FD="+AppTraff_FD);
            try {
                // 链接数据库获取数据
                JSONArray serial = new DataService().getTraffDirPie_DData(date1,date2,src,dst,appType,sm_AppType,filed,devid);
                out.println(serial);
            } catch (Exception ex) {
                Logger.getLogger(TraffDirPie_DServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
            }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
