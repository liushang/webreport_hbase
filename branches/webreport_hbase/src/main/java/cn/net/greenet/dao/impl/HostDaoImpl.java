package cn.net.greenet.dao.impl;

import cn.net.greenet.dao.BaseDao;
import cn.net.greenet.dao.NewsDao;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.PreparedStatement;

public class HostDaoImpl extends BaseDao implements NewsDao  {
        

        @Override
        public boolean addHostTRANS(BufferedReader bfr) {
		try {
			
			long count = 0;
			String sql = "INSERT INTO HttpData (times,host) VALUES(?,?)";
			this.getConnection();
			PreparedStatement psts = this.conn.prepareStatement(sql);
			String line = null;
			while (null != (line = bfr.readLine())) {  
                                String[] infos = line.replace("\t", ",").split(",");  
                                psts.setString(1, infos[0]); 
                                psts.setString(2, infos[1]);
                                psts.addBatch();
				count++;
				if(count%1000==0){
					psts.executeBatch();
					System.out.println("count=========="+count);
				}
			}
			
			psts.executeBatch();
			
		} catch (Exception e) {
			return false;
		} finally {
			// 释放资源
			this.closeResource();
		}
		return true;
	}
	
//	// 测试
	public static void main(String[] args) {

		HostDaoImpl hostDao = new HostDaoImpl();
		
		BufferedReader reader;
                File dataDir = new File("E:\\data\\");
                File file[] = dataDir.listFiles();
                
                for (int i = 0; i < file.length; i++) {
                    if (file[i].isFile() && file[i].getName().endsWith(".txt")) {
                        try {
                                reader = new BufferedReader(new FileReader(file[i]));
                                boolean result= hostDao.addHostTRANS(reader);
                                System.out.println("1111111111111111"+result);
                        } catch (FileNotFoundException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                        }
                    }
                }
	}

}
