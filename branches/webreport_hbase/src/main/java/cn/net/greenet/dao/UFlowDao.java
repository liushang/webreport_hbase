package cn.net.greenet.dao;

import java.io.BufferedReader;
import java.util.Date;




public interface UFlowDao {

	//批量新增Web流数据
	public boolean addWebFlowTran(BufferedReader bfr);
	
	//批量新增通用流数据
	public boolean addGeneralFlowTran(BufferedReader bfr);
}
