/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.net.greenet.oneton.servlet;

import cn.net.greenet.service.DataService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;

/**
 *
 * @author Ambimmort
 */
@WebServlet(name = "OneToN_UserServlet", urlPatterns = {"/OneToN/OneToN_User"})
public class OneToN_UserServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");
        String devid = request.getParameter("devid");
        
        if (( date1 !=null ) &&( date2 !=null ) ) {
            try {
                // 链接数据库获取数据
                JSONArray serial = new DataService().getOneToN_User(date1,date2,devid);
                out.println(serial);
            } catch (Exception ex) {
                Logger.getLogger(OneToN_UserServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
            }
        } else {
            try {
                // 链接数据库获取数据
                JSONArray serial = new JSONArray();
                out.println(serial);
            } catch (Exception ex) {
                Logger.getLogger(OneToN_UserServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
