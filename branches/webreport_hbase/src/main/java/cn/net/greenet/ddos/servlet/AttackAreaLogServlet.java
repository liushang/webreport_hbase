
package cn.net.greenet.ddos.servlet;

import cn.net.greenet.service.DataService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
/**
 *
 * @author asus-pc
 */
@WebServlet(name = "AttackAreaLogServlet", urlPatterns = {"/Ddos/AttackAreaLog"})
public class AttackAreaLogServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
   //     request.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        String PUserGrougNo = request.getParameter("PUserGrougNo");
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");
        String AppAttType = request.getParameter("AppAttType"); 
        
//        String devName = java.net.URLDecoder.decode(request.getParameter("devName"),"UTF-8");
//        String areaName = java.net.URLDecoder.decode(request.getParameter("areaName"),"UTF-8");
//        String devName1 = java.net.URLDecoder.decode(request.getParameter("devName"));
//        String areaName1 = java.net.URLDecoder.decode(request.getParameter("areaName"));
//        String devName = java.net.URLDecoder.decode(request.getParameter("devName"),"UTF-8");
//        String areaName = java.net.URLDecoder.decode(request.getParameter("areaName"),"UTF-8");
//        String devName = new String(request.getParameter("devName").getBytes("ISO-8859-1"), "UTF-8");
//        String areaName = new String(request.getParameter("areaName").getBytes("ISO-8859-1"), "UTF-8");        
//        String devName = new String(Base64.decodeBase64(request.getParameter("devName")),"utf-8");
         
        Base64 base64 = new Base64();
        byte[] decodeBytes = base64.decode(request.getParameter("areaName"));
        String areaName = new String(decodeBytes, "UTF-8");
        byte[] devBytes = base64.decode(request.getParameter("devName"));
        String devName = new String(devBytes, "UTF-8");
        
        String iDisplayStart = request.getParameter("iDisplayStart");
        String iDisplayLength = request.getParameter("iDisplayLength");
        System.out.println("request  Base64 ");      
        System.out.println("request  Basebytes ");      
        System.out.println("devName=====" + devName ); 
        System.out.println("areaName=====" + areaName );  
        System.out.println("request devName=====" + request.getParameter("devName") ); 
        System.out.println("request areaName=====" + request.getParameter("areaName") );
    
        if ((PUserGrougNo != null) && ( date1 !=null ) && ( date2 !=null )) {
            try {
                // 链接数据库获取数据
                JSONObject serial = new DataService().getAttackAreaLogData( PUserGrougNo, date1,date2,AppAttType,devName,areaName, Integer.parseInt(iDisplayStart),Integer.parseInt(iDisplayLength));
                out.println(serial);
            } catch (Exception ex) {
                Logger.getLogger(AttackLogDetailsServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
            }
        } else {
            try {
                // 链接数据库获取数据
                JSONObject serial = new JSONObject();
                out.println(serial);
            } catch (Exception ex) {
                Logger.getLogger(AttackLogDetailsServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
            }
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

