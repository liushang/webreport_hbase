package com.lsw.td;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.PageFilter;
import org.apache.hadoop.hbase.util.Bytes;

import com.lsw.other.DayList;

public class ReadGenFlow {
	private Configuration config;
	private HTable table, tableDay;
	private HBaseAdmin admin;

	public ReadGenFlow(String masterIp, String masterPort, String zkIp,
			String zkPort) {
		config = HBaseConfiguration.create();
		config.set("hbase.master", masterIp + ":" + masterPort);
		config.set("hbase.zookeeper.property.clientPort", zkPort);
		config.set("hbase.zookeeper.quorum", zkIp);

		try {
			table = new HTable(config, Bytes.toBytes("genetraffic_5minute"));
			tableDay = new HTable(config, Bytes.toBytes("genetraffic_day"));
			admin = new HBaseAdmin(config);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public JSONArray genFlowL2Hbase_day(String date1, String date2,
			Map<String, List<String>> appTypeMap, String appTraffic_Field,
			List<String> devNameList) throws IOException {
		Map<String, Map<Integer, Double>> aptimeMap = new HashMap<String, Map<Integer, Double>>();
		JSONArray jsonArray = new JSONArray();
		JSONArray sm_jsonArray = new JSONArray();
		// JSONObject jsonObject = new JSONObject();
		DayList dy = new DayList();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<Date> lDate = dy.findDates(date1, date2);
		for (Map.Entry<String, List<String>> typeEntry : appTypeMap.entrySet()) {
			JSONArray sm_jsonArray2 = new JSONArray();
			for (String appName : typeEntry.getValue()) {
				Result[] rst = null;
				Map<String, Double> versionMap = new HashMap<String, Double>();
				for (int i = 0; i <= lDate.size() - 1; i++) {
					System.out.println(sdf.format(lDate.get(i)));
					versionMap.put(sdf.format(lDate.get(i)).replace('-', '/'),
							0.0);
				}
				String sm_appName = null;
				for (String devName : devNameList) {
					List<Get> getList = new ArrayList<Get>();
					for (Date day : lDate) {
						// concat the rowkey with the input field
						String rowkey = sdf.format(day).replace('-', '/')
								+ "\001" + typeEntry.getKey() + "\001"
								+ appName + "\001" + devName;
						Get get = new Get(Bytes.toBytes(rowkey));
						get.addColumn(Bytes.toBytes("cf"),
								Bytes.toBytes(appTraffic_Field));
						getList.add(get);

					}
					rst = tableDay.get(getList);

					for (int i = 0; i < rst.length - 1; i++) {
						for (KeyValue kv : rst[i].raw()) {
							String[] arr = Bytes.toString(kv.getRow()).split(
									"\001");
							sm_appName = arr[3];
							versionMap.put(
									arr[0],
									versionMap.get(arr[0])
											+ Double.parseDouble((Bytes
													.toString(kv.getValue()))));
							// System.out
							// .println("###this is the optional test output###");
							// System.out
							// .print(Bytes.toString(kv.getKey()) + "  ");
							// System.out.print(arr[0] + "  ");
							// System.out.println(Bytes.toString(kv.getValue())
							// + "  ");
							// System.out
							// .println("######this is the optional test output########");
						}
					}

				}

				// for (Map.Entry<Integer, Double> entry :
				// versionMap.entrySet()) {
				// sm_jsonArray.clear();
				// sm_jsonArray.add(entry.getValue());
				// }
				// jsonObject.put(appType, sm_jsonArray);
				if (sm_appName != null) {
					sm_jsonArray2.add(sm_appName);
					for (int i = 0; i <= lDate.size() - 1; i++) {
						sm_jsonArray.add(versionMap.get(sdf
								.format(lDate.get(i)).replace('-', '/')));
					}
					sm_jsonArray2.add(sm_jsonArray);
					jsonArray.add(sm_jsonArray2);
					sm_jsonArray.clear();
				}
			}

		}
		// jsonObject.put("appName", appType);

		return jsonArray;

	}

	public JSONArray genFlowL2_SumHbase_day(String date1, String date2,
			Map<String, List<String>> appNamesMap, String appTraffic_Field,
			List<String> devNameList) throws IOException {
		JSONArray jsonArray = new JSONArray();
		JSONArray sm_jsonArray = null;
		// JSONObject jsonObject = new JSONObject();
		Result[] rst = null;
		JSONArray jsonArraysm = new JSONArray();
		DayList dy = new DayList();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<Date> lDate = dy.findDates(date1, date2);
		for (Map.Entry<String, List<String>> appEntry : appNamesMap.entrySet()) {
			sm_jsonArray = new JSONArray();
			Map<String, Double> versionMap = new HashMap<String, Double>();
			for (int i = 0; i <= lDate.size() - 1; i++) {
				versionMap.put(sdf.format(lDate.get(i)).replace('-', '/'), 0.0);
			}

			String sm_appType = null;
			for (String appName : appEntry.getValue()) {
				for (String devName : devNameList) {
					List<Get> getList = new ArrayList<Get>();
					for (Date day : lDate) {
						// concat the rowkey with the input field
						String rowkey = sdf.format(day).replace('-', '/')
								+ "\001" + appEntry.getKey() + "\001" + appName
								+ "\001" + devName;
						Get get = new Get(Bytes.toBytes(rowkey));
						get.addColumn(Bytes.toBytes("cf"),
								Bytes.toBytes(appTraffic_Field));
						getList.add(get);
					}
					rst = tableDay.get(getList);

					for (int i = 0; i < rst.length - 1; i++) {
						for (KeyValue kv : rst[i].raw()) {
							String arr2[] = Bytes.toString(kv.getRow()).split(
									"\001");
							sm_appType = arr2[2];
							versionMap.put(
									arr2[0],
									versionMap.get(arr2[0])
											+ Double.parseDouble((Bytes
													.toString(kv.getValue()))));
							// System.out.println("###this is the test output###");
							// System.out
							// .print(Bytes.toString(kv.getKey()) + "  ");
							// System.out.print(arr2[0] + "  ");
							// System.out.println(Bytes.toString(kv.getValue())
							// + "  ");
							// System.out
							// .println("######this is the test output########");
						}
					}
				}
			}
			if (sm_appType != null) {
				jsonArray.add(sm_appType);
				for (int i = 0; i <= lDate.size() - 1; i++) {
					sm_jsonArray.add(versionMap.get(sdf.format(lDate.get(i))
							.replace('-', '/')));
				}

				jsonArray.add(sm_jsonArray);
				jsonArraysm.add(jsonArray);
				sm_jsonArray.clear();
			}
		}

		// jsonObject.put("appName", appType);
		return jsonArraysm;
	}

	public JSONArray genFlowPieHbase_day(String date1, String date2,
			Map<String, List<String>> appTypeMap, String appTraffic_Field,
			List<String> devNameList) throws IOException {
		// public JSONArray genFlowPieHbase() throws IOException {
		JSONArray jsonArrayPie = new JSONArray();
		DayList dy = new DayList();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<Date> lDate = dy.findDates(date1, date2);
		// System.out.println();
		for (Map.Entry<String, List<String>> appEntry : appTypeMap.entrySet()) {
			System.out.println(appEntry.getKey());
			for (String appName : appEntry.getValue()) {
				JSONArray sm_jsonArrayPie = new JSONArray();
				System.out.println(appName);
				double sum = 0.0;
				String sm_appName = null;
				for (String devName : devNameList) {
					Result[] rs = null;
					List<Get> getList = new ArrayList<Get>();
					for (Date day : lDate) {
						String rowkey = sdf.format(day).replace('-', '/')
								+ "\001" + appEntry.getKey() + "\001" + appName
								+ "\001" + devName;
						System.out.println("rowkey" + rowkey);
						Get get = new Get(Bytes.toBytes(rowkey));

						get.addColumn(Bytes.toBytes("cf"),
								Bytes.toBytes(appTraffic_Field));
						getList.add(get);
					}
					rs = tableDay.get(getList);
					if (rs != null) {
						System.out.println("PieHbase  :" + rs.toString());
						for (int i = 0; i < rs.length - 1; i++) {
							for (KeyValue kv : rs[i].raw()) {
								String[] arr2 = Bytes.toString(kv.getRow())
										.split("\001");
								sm_appName = arr2[3];
								sum += Double.parseDouble(Bytes.toString(kv
										.getValue()));
							}
						}
					}

					System.out.print(appName + ":" + sum + "    ");

				}
				if (sm_appName != null) {
					sm_jsonArrayPie.add(sm_appName);
					sm_jsonArrayPie.add(sum);
					jsonArrayPie.add(sm_jsonArrayPie);
				}

			}

		}
		return jsonArrayPie;
	}

	public JSONArray genFlowPie_SumHbase_day(String date1, String date2,
			Map<String, List<String>> appTypeMap, String appTraffic_Field,
			List<String> devNameList) throws IOException {
		JSONArray jsonArrayPie = new JSONArray();
		DayList dy = new DayList();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<Date> lDate = dy.findDates(date1, date2);
		for (Map.Entry<String, List<String>> appEntry : appTypeMap.entrySet()) {
			double sum = 0.0;
			String appType = null;
			JSONArray sm_jsonArrayPie = new JSONArray();
			for (String appName : appEntry.getValue()) {
				for (String devName : devNameList) {
					Result[] rs = null;
					List<Get> getList = new ArrayList<Get>();
					for (Date day : lDate) {
						String rowkey = sdf.format(day).replace('-', '/')
								+ "\001" + appEntry.getKey() + "\001" + appName
								+ "\001" + devName;
						System.out.println("rowkey:" + rowkey);
						Get get = new Get(Bytes.toBytes(rowkey));
						// get.getMaxVersions();
						get.addColumn(Bytes.toBytes("cf"),
								Bytes.toBytes(appTraffic_Field));
						// get.setTimeRange(0L, Long.MAX_VALUE);
						getList.add(get);
					}
					rs = tableDay.get(getList);
					if (rs != null) {
						System.out.println("PieHbase  :" + rs.toString());
						for (int i = 0; i < rs.length - 1; i++) {
							for (KeyValue kv : rs[i].raw()) {
								String[] arr2 = Bytes.toString(kv.getRow())
										.split("\001");
								appType = arr2[2];
								sum += Double.parseDouble(Bytes.toString(kv
										.getValue()));
							}
						}
					}

					System.out.print(appName + ":" + sum + "    ");

				}
			}
			if (appType != null) {
				sm_jsonArrayPie.add(appType);
				sm_jsonArrayPie.add(sum);
				jsonArrayPie.add(sm_jsonArrayPie);

			}

		}
		return jsonArrayPie;
	}

	public JSONArray genFlowTop10Hbase_day(String date1, String date2,
			Map<String, List<String>> appMap, String appTraffic_Field,
			List<String> devNameList) throws IOException {
		Map<Double, ArrayList<JSONArray>> appName_numMap = new HashMap<Double, ArrayList<JSONArray>>();
		// ArrayList<JSONArray> appList = new ArrayList<JSONArray>();
		JSONArray top_jsonArray = new JSONArray();
		DayList dy = new DayList();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<Date> lDate = dy.findDates(date1, date2);
		for (Map.Entry<String, List<String>> appEntry : appMap.entrySet()) {
			System.out.println(appEntry.getKey());
			for (String appName : appEntry.getValue()) {
				JSONArray one_jsonArray = new JSONArray();
				System.out.println(appName);
				System.out.println("&&&" + one_jsonArray);
				Map<JSONArray, Double> tmpMap = new HashMap<JSONArray, Double>();
				for (String devName : devNameList) {

					Result rst[] = null;
					List<Get> getList = new ArrayList<Get>();
					for (Date day : lDate) {
						double sum = 0.0;
						// one_jsonArray = new JSONArray();
						String rowkey = sdf.format(day).replace('-', '/')
								+ "\001" + appEntry.getKey() + "\001" + appName
								+ "\001" + devName;
						System.out.println("rowkey:" + rowkey);
						Get get = new Get(Bytes.toBytes(rowkey));
						// get.getMaxVersions();
						get.addColumn(Bytes.toBytes("cf"),
								Bytes.toBytes(appTraffic_Field));
						// get.setTimeRange(0L, Long.MAX_VALUE);
						getList.add(get);
					}
					rst = tableDay.get(getList);
					if (rst != null) {
						one_jsonArray.clear();
						one_jsonArray.add(appEntry.getKey());
						one_jsonArray.add(appName);
						if (!tmpMap.containsKey(one_jsonArray)) {
							for (int i = 0; i < rst.length - 1; i++) {
								for (KeyValue kv : rst[i].raw()) {
									tmpMap.put(one_jsonArray, Double
											.parseDouble(Bytes.toString(kv
													.getValue())));
								}
							}
						} else {
							for (int i = 0; i < rst.length - 1; i++) {
								for (KeyValue kv : rst[i].raw()) {
									tmpMap.put(
											one_jsonArray,
											Double.parseDouble(Bytes
													.toString(kv.getValue()))
													+ tmpMap.get(one_jsonArray));
								}
							}
						}

					}

					// appList.add(one_jsonArray);
				}
				for (Map.Entry<JSONArray, Double> e : tmpMap.entrySet()) {
					ArrayList<JSONArray> tmpList = new ArrayList<JSONArray>();
					if (appName_numMap.get(e.getValue()) != null) {
						ArrayList<JSONArray> tmpList2 = appName_numMap.get(e
								.getValue());
						tmpList2.add(e.getKey());
						appName_numMap.put(e.getValue(), tmpList2);
					} else {
						tmpList.add(e.getKey());
						appName_numMap.put(e.getValue(), tmpList);
					}
				}
			}
		}
		// sort the sum of appName ,and get the top 10
		Double[] sm_value = appName_numMap.keySet().toArray(
				new Double[appName_numMap.size()]);
		Arrays.sort(sm_value);
		JSONArray tmpjArray = new JSONArray();
		// System.out.println(",,,,," + sm_value.length);
		// System.out.println(",,,,," + appName_numMap.get(0.0).get(0));

		int count = 1;
		for (int i = sm_value.length - 1; i >= 0; i--) {
			if (count <= 10) {
				// System.out.println("the key of the map is: " + i);
				for (JSONArray oneAppName : appName_numMap.get(sm_value[i])) {
					oneAppName.add(sm_value[i]);
					top_jsonArray.add(oneAppName);
				}
			}
			count++;
		}

		return top_jsonArray;
	}

	public JSONObject genFlowDetailsHbase_day(String date1, String date2,

	Map<String, List<String>> appTypeMap, List<String> devNameList,
			int iDisplayStart, int iDisplayLength) throws IOException {
		DayList dy = new DayList();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<Date> lDate = dy.findDates(date1, date2);
		Filter filter = new PageFilter(iDisplayLength);
		int localRows = iDisplayStart;
		int total = 0;

		byte[] lastRow = Bytes.toBytes(iDisplayStart);
		System.out.println(lastRow.length);
		String[] column = { "R_StartTime", "R_EndTime", "userGroupId",
				"AppUserNum", "AppTraffic_UP", "AppTraffic_DN",
				"AppPacketsNum", "AppSessionNum", "AppNewSessionNum" };
		Result[] result = new Result[column.length];
		JSONObject allObject = new JSONObject();
		JSONArray outArray = new JSONArray();
		for (Map.Entry<String, List<String>> appEntry : appTypeMap.entrySet()) {
			for (String appName : appEntry.getValue()) {
				for (String devName : devNameList) {
					for (Date day : lDate) {
						JSONArray propertyArray = new JSONArray();
						System.out.println("....." + devName);
						String rowkey = sdf.format(day).replace('-', '/')
								+ "\001" + appEntry.getKey() + "\001" + appName
								+ "\001" + devName;
						System.out.println("rowkey" + rowkey);
						for (int no = 0; no <= column.length - 1; no++) {
							System.out.println("column " + column[no]);
							Get get = new Get(Bytes.toBytes(rowkey));
							get.setMaxVersions();
							get.setTimeRange(0L, Long.MAX_VALUE);
							get.addColumn(Bytes.toBytes("cf"),
									Bytes.toBytes(column[no]));
							result[no] = table.get(get);
							// System.out.println("resultlength"
							// + result.length);
						}

						if (result[1].getRow() != null) {

							int realLength = iDisplayLength + iDisplayStart - 1;
							if (realLength >= result[1].size()) {
								realLength = result[1].size();
							}
							for (int kno = iDisplayStart - 1; kno <= realLength - 1; kno++) {
								total = result[1].size();
								Map<Integer, String> resultMap = new HashMap<Integer, String>();
								propertyArray.add(devName);
								localRows++;
								for (int i = 0; i <= result.length - 1; i++) {
									String[] rowkeyarr = Bytes.toString(
											result[i].getRow()).split("\001");
									if (i < 2) {
										resultMap.put(
												i,
												Bytes.toString(result[i].list()
														.get(kno).getValue()));
									}
									if (i == 2) {
										resultMap.put(
												2,
												Bytes.toString(result[i].list()
														.get(kno).getValue()));
										resultMap.put(
												3,
												Bytes.toString(result[i].list()
														.get(kno).getValue()));
										resultMap.put(4, rowkeyarr[2]);
										resultMap.put(5, rowkeyarr[3]);
									}
									if (i > 3) {
										resultMap.put(
												i + 3,
												Bytes.toString(result[i].list()
														.get(kno).getValue()));
									}
									// System.out.println("##resultMap:" + i
									// + "-->" + resultMap.get(i));
								}
								for (int mno = 0; mno <= resultMap.size() - 1; mno++) {
									System.out.println("*****resultMap:" + mno
											+ "-->" + resultMap.get(mno));
									propertyArray.add(resultMap.get(mno));
								}

								outArray.add(propertyArray);
								propertyArray.clear();

							}
						}
					}
				}

			}
		}

		allObject.put("aaData", outArray);
		allObject.put("iTotalDisplayRecords", localRows - 1);
		allObject.put("iTotalRecords", total);
		return allObject;
	}

}
