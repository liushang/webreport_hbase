package com.ambimmort.app.framework.fsmonitor.commands;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;

/**
 * Created by hedingwei on 2015/1/15.
 */
public class EchoCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "echo";
    }

    @Override
    public void prepareOption(Options options) throws Throwable {
        options.addOption("v", false, "version");

        options.addOption("h", false, "Print the help");
    }

    @Override
    public void execute(CommandLine commandLine, Options options) throws Throwable {
        if (commandLine.hasOption("v")) {
            println(" Echo version 1.0");
        } else if (commandLine.hasOption("h")) {
            printHelp();
        } else {
            StringBuilder sb = new StringBuilder();
            for (String s : commandLine.getArgs()) {
                sb.append(s).append(" ");
            }
            println(sb.toString());
        }
    }
}
