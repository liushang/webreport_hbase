package cn.net.greenet.cpsp.servlet;

import cn.net.greenet.service.DataService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;

@WebServlet(name = "CpSpResL2_DServlet", urlPatterns = {"/CpSp/CpSpResL2_D"})
public class CpSpResL2_DServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");
        String ProtType = request.getParameter("ProtType");
        String AppType = request.getParameter("AppType");
        String SM_AppType = request.getParameter("SM_AppType");
        String devid = request.getParameter("devid");
        
System.out.println("date1="+date1+"date2="+date2+ "ProtType="+ProtType + "AppType="+AppType+ "SM_AppType="+SM_AppType );    

        try {
            // 链接数据库获取数据
            JSONArray serial = new DataService().getCpSpResL2_DData(date1,date2,ProtType,AppType,SM_AppType,devid);
            out.println(serial);
        } catch (Exception ex) {
            Logger.getLogger(CpSpResL2_DServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
