/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.net.greenet.service;

import cn.net.greenet.dao.BaseDao;
import cn.net.greenet.util.ConfigManager;
import cn.net.greenet.util.MapOperate;
import com.lsw.day0916.ReadHBaseAttackTable;
import com.lsw.day0916.ReadHBaseCPSPTable;
import com.lsw.day0916.ReadHBaseGenTable;
import com.lsw.day0916.ReadHBaseIllegalRouteTable;
import com.lsw.day0916.ReadHBaseVoipTable;
import com.lsw.day0916.ReadHBaseTrafficTable;
import com.lsw.day0916.ReadHBaseVisitAppTable;
import com.lsw.day0916.ReadHBaseWebTable;
import com.lsw.other.FileProperties;
import com.lsw.other.RemoteFileClient;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 *
 * @author Ambimmort
 */
public class DataService extends BaseDao {

    //获取某个网站某天的点击率  WEB类流量统计
//    private ReadHBaseTrafficTable rbTraffic = new ReadHBaseTrafficTable("10.10.2.41", "60000", "10.10.2.41", "2181");
//    private ReadHBaseAttackTable rbAttack = new ReadHBaseAttackTable("10.10.2.41","60000", "10.10.2.41", "2181");
//    private ReadHBaseVoipTable rbVoip = new ReadHBaseVoipTable("10.10.2.41","60000", "10.10.2.41", "2181");
//    private ReadHBaseCPSPTable rbCpSp = new ReadHBaseCPSPTable("10.10.2.41","60000", "10.10.2.41", "2181");
//    private ReadHBaseGenTable rbGen = new ReadHBaseGenTable("10.10.2.41","60000", "10.10.2.41", "2181");
//    private ReadHBaseWebTable rbWeb = new ReadHBaseWebTable("10.10.2.41","60000", "10.10.2.41", "2181");
//    private ReadHBaseVisitAppTable rbVisit = new ReadHBaseVisitAppTable("10.10.2.41","60000", "10.10.2.41", "2181");
//    private ReadHBaseIllegalRouteTable rbIllRoute = new ReadHBaseIllegalRouteTable("10.10.2.41","60000", "10.10.2.41", "2181");
    private <T> T getHBaseTable(Class<T> clazz) {

        try {
//            Constructor<T> constructor = clazz.getConstructor(String.class, String.class, String.class, String.class);
            Constructor<T> constructor = clazz.getConstructor();
//            String masterIP = ConfigManager.getInstance().getString("masterIP");
//            String masterPort = ConfigManager.getInstance().getString("masterPort");
//            String zkIp = ConfigManager.getInstance().getString("zkIp");
//            String zkPort = ConfigManager.getInstance().getString("zkPort");
//            return constructor.newInstance("172.21.3.4", "16030", "172.21.3.1,172.21.3.2,172.21.3.3", "2181");
//            return constructor.newInstance(masterIP,masterPort,zkIp,zkPort);
            return constructor.newInstance();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONArray getWebFlowData(String day, String siteName, String userGroupNo) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date = sdf.parse(day);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);

        super.getConnection();
        String sql = "select  R_StartTime , R_EndTime ,sum(Site_Num_Hit)  as  Site_Num_Hit  from web_flow_stat  "
                + "where  R_StartTime >=?   and R_EndTime <=?  and  SiteName = ? and UserGroupNo =?  "
                + "group by   R_StartTime , R_EndTime order by  R_StartTime , R_EndTime ";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setTimestamp(1, new Timestamp(date.getTime()));
        pst.setTimestamp(2, new Timestamp(dayEnd.getTime().getTime()));
        pst.setString(3, siteName);
        pst.setString(4, userGroupNo);

        ResultSet rs = pst.executeQuery();

        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("R_StartTime").substring(11, 16) + "--" + rs.getString("R_EndTime").substring(11, 16));
//            arr1_2.add(rs.getTimestamp("R_StartTime").getHours());
            arr1_2.add(rs.getLong("Site_Num_Hit"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1===" + arr1.toString());
        return arr1;
    }

    //获取WEB类流向分析趋势 5分钟粒度
    public JSONArray getWebFlowL2Data(String date1, String userGNo, String siteType, String AppTraffic_filed, String devid) throws ClassNotFoundException, SQLException, ParseException {

        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        Map<String, String> tmpMap = new HashMap<String, String>();
        Map<String, String> webTypeMap = getWebTypeMap();
        if (siteType.equals("ALL")) {
            tmpMap = webTypeMap;
        } else {
            tmpMap.put(siteType, webTypeMap.get(siteType));
        }
        ReadHBaseWebTable rbWeb = getHBaseTable(ReadHBaseWebTable.class);
        try {
            System.out.println("======web类流量统计========\ndate1:" + date1 + "\nuserGNo:" + userGNo + "\ntmpMap:" + tmpMap + "\nAppTraffic_filed:" + AppTraffic_filed + "\ndevList:" + devList);
            arr1 = rbWeb.webFlowL2Hbase(date1, userGNo, tmpMap, AppTraffic_filed, devList);
//            System.out.println("结果：" + arr1);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("DataArr::::::::=" + arr1.toString());
        System.out.println("date1::::::::=" + date1);
        System.out.println("devList::::::::=" + devList.toString());
        return arr1;
    }

    //获取WEB类流向分析趋势 天粒度
    public JSONArray getWebFlowL2_DData(String date1, String date2, String userGNo, String siteType, String AppTraffic_filed, String devid) throws ClassNotFoundException, SQLException {
        System.out.println("date1=" + date1 + " date2=" + date2 + " userGNo=" + userGNo + " siteType=" + siteType + " AppTraffic_filed=" + AppTraffic_filed + " devid=" + devid);
        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        Map<String, String> tmpMap = new HashMap<String, String>();
        Map<String, String> webTypeMap = getWebTypeMap();
        if (siteType.equals("ALL")) {
            tmpMap = webTypeMap;
        } else {
            tmpMap.put(siteType, webTypeMap.get(siteType));
        }
        ReadHBaseWebTable rbWeb = getHBaseTable(ReadHBaseWebTable.class);
        try {
            arr1 = rbWeb.webFlowL2Hbase_day(date1, date2, userGNo, tmpMap, AppTraffic_filed, devList);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arr1;
    }

    //获取WEB类流向分析占比 5分钟粒度
    public JSONArray getWebFlowPieData(String date1, String userGNo, String siteType, String AppTraffic_filed, String devid) throws ClassNotFoundException, SQLException, ParseException {
        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        Map<String, String> tmpMap = new HashMap<String, String>();
        Map<String, String> webTypeMap = getWebTypeMap();
        if (siteType.equals("ALL")) {
            tmpMap = webTypeMap;
        } else {
            tmpMap.put(siteType, webTypeMap.get(siteType));
        }
        ReadHBaseWebTable rbWeb = getHBaseTable(ReadHBaseWebTable.class);
        try {
            arr1 = rbWeb.webFlowPieHbase(date1, userGNo, tmpMap, AppTraffic_filed, devList);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("DataArr::::::::=" + arr1.toString());
        System.out.println("date1::::::::=" + date1);
        System.out.println("devList::::::::=" + devList.toString());
        return arr1;
    }

    //获取WEB类流向分析占比 一天粒度
    public JSONArray getWebFlowPie_DData(String date1, String date2, String userGNo, String siteType, String AppTraffic_filed, String devid) throws ClassNotFoundException, SQLException {

        System.out.println("date1=" + date1 + " date2=" + date2 + " userGNo=" + userGNo + " siteType=" + siteType + " AppTraffic_filed=" + AppTraffic_filed + " devid=" + devid);
        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        Map<String, String> tmpMap = new HashMap<String, String>();
        Map<String, String> webTypeMap = getWebTypeMap();
        if (siteType.equals("ALL")) {
            tmpMap = webTypeMap;
        } else {
            tmpMap.put(siteType, webTypeMap.get(siteType));
        }
        ReadHBaseWebTable rbWeb = getHBaseTable(ReadHBaseWebTable.class);
        try {
            arr1 = rbWeb.webFlowPieHbase_day(date1, date2, userGNo, tmpMap, AppTraffic_filed, devList);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arr1;
    }

    //获取某天某个应用的用户数  通用类流量统计
    public JSONArray getGeneralFlowData(String day, String appId, String userGroupNo) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date = sdf.parse(day);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);

        super.getConnection();
        String sql = "select  R_StartTime , R_EndTime ,sum(AppUserNum)  as  AppUserNum  from general_flow_stat "
                + "where  R_StartTime >=?   and R_EndTime <=?  and  appId = ?  and UserGroupNo =? "
                + "group by  R_StartTime , R_EndTime order by  R_StartTime , R_EndTime ";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setTimestamp(1, new Timestamp(date.getTime()));
        pst.setTimestamp(2, new Timestamp(dayEnd.getTime().getTime()));
        pst.setString(3, appId);
        pst.setString(4, userGroupNo);

        ResultSet rs = pst.executeQuery();

        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("R_StartTime").substring(11, 16) + "--" + rs.getString("R_EndTime").substring(11, 16));
            arr1_2.add(rs.getLong("AppUserNum"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;
    }

    //获取App_Attack 总信息
    public JSONArray getAppAttackTotalData(String PUserGrougNo, String date1, String date2, String AppAttType, String devName) throws ClassNotFoundException, SQLException, ParseException {
        System.out.println("getAppAttackTotalData:::::" + PUserGrougNo + date1 + date2 + AppAttType + devName);
        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        ReadHBaseAttackTable rbAttack = getHBaseTable(ReadHBaseAttackTable.class);
        try {
            arr1 = rbAttack.attackTotalHbase(PUserGrougNo, date1, date2, AppAttType, devList);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return arr1;
    }

    //获取 dpiusergroup 用户组列表
    public JSONArray getPUserGrougListData() throws ClassNotFoundException, SQLException {
        super.getConnection();
        String sql = " select id ,name from dpiusergroup  PG  order by  id;";
        PreparedStatement pst;
        pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("id"));
            arr1_2.add(rs.getString("name"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;
    }

    //获取 dpiusergroup 用户组Map
    private Map<String, String> userGroupMap() throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "select id ,name from dpiusergroup order by  id;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        Map<String, String> userMap = new HashMap<String, String>();
        while (rs.next()) {
            userMap.put(rs.getString("id"), rs.getString("name"));
        }
        super.closeResource();
        return userMap;
    }

    //获取 应用层攻击类型 列表
    public JSONArray getAppAttTypeListData() throws ClassNotFoundException, SQLException {
        super.getConnection();
        String sql = "select distinct AttackType_Code , AttackType_Name  \n"
                + "from  AttackType_List order by AttackType_Code ;";
        PreparedStatement pst;
        pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("AttackType_Code"));
            arr1_2.add(rs.getString("AttackType_Name"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;
    }

    //获取某个时间段内攻击流量趋势
    public JSONArray getAttackChartData(String userGrop, String attackFiled, String date1, String date2, String appAttType, String devName) throws ClassNotFoundException, SQLException, ParseException {

        System.out.println("date1=" + date1 + " date2=" + date2 + " userGrop=" + userGrop + " appAttType=" + appAttType + " attackFiled=" + attackFiled + " devName=" + devName);
        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
//        try {
//            arr1 = rbAttack.attackChartHbase_day(userGrop,attackFiled,date1,date2,appAttType, devList) ;
//        } catch (IOException ex) {
//            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
//        }
        System.out.println("devList::::" + devList.toString());
        System.out.println(arr1.toString());
        return arr1;
    }

    //获取某个时间段内攻击流量趋势  5分钟一个段落
    public JSONArray getAttackChart5MData(String PUserGrougNo, String attackFiled, String date1, String date2, String appAttType, String devName) throws ClassNotFoundException, SQLException, ParseException {
        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        ReadHBaseAttackTable rbAttack = getHBaseTable(ReadHBaseAttackTable.class);
        try {
            arr1 = rbAttack.attackChart5MHbase(PUserGrougNo, attackFiled, appAttType, date1, devList);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("devList::::" + devList.toString());
        System.out.println(arr1.toString());
        return arr1;
    }

    //获取某个时间段内每个源区域攻击次数
    public JSONObject getAttackAreaData(String PUserGrougNo, String date1, String date2, String AppAttType, String devName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        JSONObject DataObj = new JSONObject();
        ArrayList<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        ReadHBaseAttackTable rbAttack = getHBaseTable(ReadHBaseAttackTable.class);
        try {
            DataObj = rbAttack.attackAreaHbase(PUserGrougNo, date1, date2, AppAttType, devList, iDisplayStart + 1, iDisplayLength);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("devList::::" + devList.toString());
        System.out.println(DataObj);
        return DataObj;
    }

    //获取某个时间段内攻击源日志详情
    public JSONObject getAttackAreaLogData(String userGroupNo, String date1, String date2, String AppAttType, String devName, String areaName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        JSONObject DataObj = new JSONObject();
        ArrayList<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        Map<String, String> userMap = new HashMap<String, String>();
        Map<String, String> userGpMap = userGroupMap();
        userMap.put(userGroupNo, userGpMap.get(userGroupNo));

        Map<String, String> appAttTypeMap = new HashMap<String, String>();
        Map<String, String> appAttListMap = appAttTypeMap();
        appAttTypeMap.put(AppAttType, appAttListMap.get(AppAttType));
        ReadHBaseAttackTable rbAttack = getHBaseTable(ReadHBaseAttackTable.class);
        try {
            DataObj = rbAttack.attackAreaLogHbase(userMap, date1, date2, appAttTypeMap, devList, areaName, iDisplayStart + 1, iDisplayLength);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("userMap::::" + userMap.toString());
        System.out.println("appAttTypeMap::::" + appAttTypeMap.toString());
        System.out.println("devList::::" + devList.toString());
        System.out.println("areaName::::" + areaName);

        System.out.println(DataObj);
        return DataObj;
    }

    //获取某个时间段内攻击日志详情
    public JSONObject getAttackLogDetailsData(String PUserGrougNo, String date1, String date2, String AppAttType, String devName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException {

        JSONObject DataObj = new JSONObject();
        ArrayList<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        Map<String, String> userMap = new HashMap<String, String>();
        Map<String, String> userGpMap = userGroupMap();
        userMap.put(PUserGrougNo, userGpMap.get(PUserGrougNo));

        Map<String, String> appAttTypeMap = new HashMap<String, String>();
        Map<String, String> appAttListMap = appAttTypeMap();
        appAttTypeMap.put(AppAttType, appAttListMap.get(AppAttType));
        ReadHBaseAttackTable rbAttack = getHBaseTable(ReadHBaseAttackTable.class);
        try {
            DataObj = rbAttack.attackLogDetailsHbase(userMap, date1, date2, appAttTypeMap, devList, iDisplayStart + 1, iDisplayLength);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("userMap::::" + userMap.toString());
        System.out.println("appAttTypeMap::::" + appAttTypeMap.toString());
        System.out.println("devList::::" + devList.toString());

        System.out.println(DataObj);
        return DataObj;
    }

    //获取某个时间段内通用流量详情
    public JSONObject getGeneralFlowDetailsData(String PUserGrougNo, String date1, String date2, String appType, String sm_AppType, String devName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException {

        System.out.println("date1=" + date1 + " date2=" + date2 + " PUserGrougNo=" + PUserGrougNo + " appType=" + appType + " sm_AppType=" + sm_AppType + " devName=" + devName + " iDisplayStart=" + iDisplayStart + " iDisplayLength=" + iDisplayLength);

        JSONObject DataObj = new JSONObject();
        List<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        Map<String, List<String>> appNameMap = appNameMap();
        Map<String, String> appTypeMap = appTypeMap();
        Map<String, List<String>> tmpMap = new HashMap<String, List<String>>();
        if (appType.equals("ALL")) {
            for (String typeId : appTypeMap.keySet()) {
                tmpMap.put(appTypeMap.get(typeId), appNameMap.get(typeId));
            }
        } else {
            if (sm_AppType.equals("ALL")) {
                tmpMap.put(appTypeMap.get(appType), appNameMap.get(appType));
            } else {
                ArrayList<String> appNamesL = new ArrayList();
                appNamesL.add(sm_AppType);
                tmpMap.put(appTypeMap.get(appType), appNamesL);
            }
        }
        Map<String, String> userMap = new HashMap<String, String>();
        Map<String, String> userGpMap = userGroupMap();
        userMap.put(PUserGrougNo, userGpMap.get(PUserGrougNo));

        System.out.println("getGeneralFlowDetailsData tmpMap:::==" + tmpMap.toString());
        System.out.println("getGeneralFlowDetailsData userMap:::==" + userMap.toString());
        System.out.println("getGeneralFlowDetailsData devList:::==" + devList.toString());
        System.out.println("getGeneralFlowDetailsData iDisplayStart:::==" + iDisplayStart);
        System.out.println("getGeneralFlowDetailsData iDisplayLength:::==" + iDisplayLength);
        System.out.println("getGeneralFlowDetailsData date1:::==" + date1);
        ReadHBaseGenTable rbGen = getHBaseTable(ReadHBaseGenTable.class);
        try {
            if (date1.equals(date2)) {
                DataObj = rbGen.genFlowDetailsHbase_day(date1, date2, userMap, tmpMap, devList, iDisplayStart + 1, iDisplayLength);
            } else {
                DataObj = rbGen.genFlowDetailsHbase_day(date1, date2, userMap, tmpMap, devList, iDisplayStart + 1, iDisplayLength);
            }
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return DataObj;
    }

    //获取某个时间段内某个组各类应用流量占比
    public JSONArray getGenFlowPieData(String date1, String date2, String userGroupNo, String appType, String sm_AppType, String filed, String devName) throws ClassNotFoundException, SQLException, ParseException {

        System.out.println("获取某个时间段内某个组各类应用流量占比:date1=" + date1 + " date2=" + date2 + " userGNo=" + userGroupNo + " appType=" + appType + " sm_AppType=" + sm_AppType + " filed=" + filed + " devName=" + devName);
        JSONArray arr1 = new JSONArray();
        List<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        Map<String, List<String>> appNameMap = appNameMap();
        Map<String, String> appTypeMap = appTypeMap();
        Map<String, List<String>> tmpMap = new HashMap<String, List<String>>();
        ReadHBaseGenTable rbGen = getHBaseTable(ReadHBaseGenTable.class);
        if (appType.equals("ALL")) {
            for (String typeId : appTypeMap.keySet()) {
                tmpMap.put(appTypeMap.get(typeId), appNameMap.get(typeId));
            }
            try {
                if (date1.equals(date2)) {
                    arr1 = rbGen.genFlowPie_SumHbase(date1, userGroupNo, tmpMap, filed, devList);
                } else {
                    arr1 = rbGen.genFlowPie_SumHbase_day(date1, date2, userGroupNo, tmpMap, filed, devList);
                }
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            tmpMap.put(appTypeMap.get(appType), appNameMap.get(appType));
            try {
                if (date1.equals(date2)) {
                    arr1 = rbGen.genFlowPieHbase(date1, userGroupNo, tmpMap, filed, devList);
                } else {
                    arr1 = rbGen.genFlowPieHbase_day(date1, date2, userGroupNo, tmpMap, filed, devList);
                }
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("getGenFlowPieData tmpMap:::==" + tmpMap.toString());
        System.out.println("getGenFlowPieData devList:::==" + devList.toString());

        return arr1;
    }

    public JSONArray getGenFlowTop10Data(String date1, String date2, String userGroupNo, String appType, String sm_AppType, String updown, String devName) throws ClassNotFoundException, SQLException, ParseException {
        System.out.println("date1=" + date1 + " date2=" + date2 + " userGNo=" + userGroupNo + " appType=" + appType + " sm_AppType=" + sm_AppType + " updown=" + updown + " devName=" + devName);
        JSONArray arr1 = new JSONArray();
        List<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        Map<String, List<String>> appNameMap = appNameMap();
        Map<String, String> appTypeMap = appTypeMap();
        Map<String, List<String>> tmpMap = new HashMap<String, List<String>>();

        if (appType.equals("ALL")) {
            for (String typeId : appTypeMap.keySet()) {
                tmpMap.put(appTypeMap.get(typeId), appNameMap.get(typeId));
            }
        } else {
            tmpMap.put(appTypeMap.get(appType), appNameMap.get(appType));
        }
        ReadHBaseGenTable rbGen = getHBaseTable(ReadHBaseGenTable.class);
        try {
            if (date1.equals(date2)) {
                arr1 = rbGen.genFlowTop10Hbase(date1, userGroupNo, tmpMap, updown, devList);
            } else {
                arr1 = rbGen.genFlowTop10Hbase_day(date1, date2, userGroupNo, tmpMap, updown, devList);
            }
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("getGenFlowTop10Data tmpMap:::==" + tmpMap.toString());
        System.out.println("getGenFlowTop10Data devList:::==" + devList.toString());
        System.out.println("getGenFlowTop10Data arr1:::==" + arr1.toString());
        return arr1;
    }

    //获取某个时间段内某个组各类应用流量趋势
    public JSONArray getGenFlowL2Data(String date1, String date2, String timeType, String userGroupNo, String appType, String appNames, String updown, String devName) throws ClassNotFoundException, SQLException {
        System.out.println("获取某个时间段内某个组各类应用流量趋势\ndate1=" + date1 + " date2=" + date2 + " userGNo=" + userGroupNo + " appType=" + appType + " appNames=" + appNames + " filed=" + updown + " devName=" + devName);
        JSONArray arr1 = new JSONArray();
        List<String> devList = new ArrayList<>();

        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        Map<String, List<String>> appNameMap = appNameMap();
        Map<String, String> appTypeMap = appTypeMap();
        Map<String, List<String>> tmpMap = new HashMap<>();

        ReadHBaseGenTable rbGen = getHBaseTable(ReadHBaseGenTable.class);
        //粒度为5分钟
        if (timeType.equals("0")) {
            if (appType.equals("ALL")) {
                for (String typeId : appTypeMap.keySet()) {
                    if (!appNameMap.isEmpty()) {
                        tmpMap.put(appTypeMap.get(typeId), appNameMap.get(typeId));
                        System.out.println("=====获取list所有小类======start\n" + typeId + " : " + appNameMap.get(typeId));
                        System.out.println("\n=====获取list所有小类======end");
                    }
                }
                for (String s : tmpMap.keySet()) {
                    System.out.println("=====获取list所有小类名称======start\n");
                    System.out.println(s + " : " + tmpMap.get(s));
                    System.out.println("\n=====获取list所有小类名称======end");
                }
                try {
                    System.out.println("arr1:" + arr1.size() + "\tdevList:" + devList.size() + "\tappNameMap:" + appNameMap.size() + "\tappTypeMap:" + appTypeMap.size() + "\ttmpMap:" + tmpMap.size());

                    arr1 = rbGen.genFlowL2_SumHbase(date1, userGroupNo, tmpMap, updown, devList);
                } catch (IOException ex) {
                    Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                if (appNames.equals("ALL")) {
                    tmpMap.put(appTypeMap.get(appType), appNameMap.get(appType));
                } else {
                    ArrayList<String> appNamesL = new ArrayList();
                    appNamesL.add(appNames);
                    tmpMap.put(appTypeMap.get(appType), appNamesL);
                }
                try {
                    arr1 = rbGen.genFlowL2Hbase(date1, userGroupNo, tmpMap, updown, devList);
                } catch (IOException ex) {
                    Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else if (timeType.equals("1")) {         //粒度为天
            if (appType.equals("ALL")) {
                for (String typeId : appTypeMap.keySet()) {
                    tmpMap.put(appTypeMap.get(typeId), appNameMap.get(typeId));
                }
                try {
                    arr1 = rbGen.genFlowL2_SumHbase_day(date1, date2, userGroupNo, tmpMap, updown, devList);
                } catch (IOException ex) {
                    Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                if (appNames.equals("ALL")) {
                    tmpMap.put(appTypeMap.get(appType), appNameMap.get(appType));
                } else {
                    ArrayList<String> appNamesL = new ArrayList();
                    appNamesL.add(appNames);
                    tmpMap.put(appTypeMap.get(appType), appNamesL);
                }
                try {
                    arr1 = rbGen.genFlowL2Hbase_day(date1, date2, userGroupNo, tmpMap, updown, devList);
                } catch (IOException ex) {
                    Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        System.out.println("getGenFlowL2Data tmpMap:::==" + tmpMap.toString());
        System.out.println("getGenFlowL2Data devList:::==" + devList.toString());
        System.out.println("getGenFlowL2Data arr1:::==" + arr1.toString());
        return arr1;
    }

    //将时间转换到最近的整5分钟
    public String getStdMinByTime(String EndTime) throws ParseException {
        String StdEndTime = EndTime;
        SimpleDateFormat sdf_hm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date_tmp = sdf_hm.parse(EndTime);
        int addmin = 5 - date_tmp.getMinutes() % 5;
        if (addmin < 5) {
            Date afterDate = new Date(date_tmp.getTime() + addmin * 1000 * 60);
            StdEndTime = sdf_hm.format(afterDate);
        }
        return StdEndTime;
    }

    //获取CP/SP资源服务器分析趋势 天粒度
    public JSONArray getCpSpResL2_DData(String date1, String date2, String protType, String appType, String appNames, String devid) throws ClassNotFoundException, SQLException {
        System.out.println("date1=" + date1 + "date2=" + date2 + "protType=" + protType + "appType=" + appType + "appNames=" + appNames + "devid=" + devid);

        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        Map<String, List<String>> appNameMap = appNameMap();
        Map<String, String> appTypeMap = appTypeMap();
        Map<String, List<String>> tmpMap = new HashMap<String, List<String>>();
        ReadHBaseCPSPTable rbCpSp = getHBaseTable(ReadHBaseCPSPTable.class);
        if (appType.equals("ALL")) {
            for (String typeId : appTypeMap.keySet()) {
                tmpMap.put(appTypeMap.get(typeId), appNameMap.get(typeId));
            }
            try {
                arr1 = rbCpSp.cpSpResL2_SumHbase_day(date1, date2, protType, tmpMap, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (appNames.equals("ALL")) {
                tmpMap.put(appTypeMap.get(appType), appNameMap.get(appType));
            } else {
                ArrayList<String> appNamesL = new ArrayList();
                appNamesL.add(appNames);
                tmpMap.put(appTypeMap.get(appType), appNamesL);
            }
            try {
                arr1 = rbCpSp.cpSpResL2Hbase_day(date1, date2, protType, tmpMap, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("DataArr::::::::=" + arr1.toString());
        System.out.println("devList::::::::=" + devList.toString());
        return arr1;
    }

    //获取CP/SP资源服务器分析趋势 5分钟粒度
    public JSONArray getCpSpResL2Data(String date1, String protType, String appType, String appNames, String devid) throws ClassNotFoundException, SQLException {
        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        Map<String, List<String>> appNameMap = appNameMap();
        Map<String, String> appTypeMap = appTypeMap();
        Map<String, List<String>> tmpMap = new HashMap<String, List<String>>();
        ReadHBaseCPSPTable rbCpSp = getHBaseTable(ReadHBaseCPSPTable.class);
        if (appType.equals("ALL")) {
            for (String typeId : appTypeMap.keySet()) {
                tmpMap.put(appTypeMap.get(typeId), appNameMap.get(typeId));
            }
            try {
                arr1 = rbCpSp.cpSpResL2_SumHbase(date1, protType, tmpMap, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (appNames.equals("ALL")) {
                tmpMap.put(appTypeMap.get(appType), appNameMap.get(appType));
            } else {
                ArrayList<String> appNamesL = new ArrayList();
                appNamesL.add(appNames);
                tmpMap.put(appTypeMap.get(appType), appNamesL);
            }
            try {
                arr1 = rbCpSp.cpSpResL2Hbase(date1, protType, tmpMap, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("DataArr::::::::=" + arr1.toString());
        System.out.println("date1::::::::=" + date1);
        System.out.println("devList::::::::=" + devList.toString());

        return arr1;
    }

    //获取CP/SP资源服务器请求占比 5分钟粒度
    public JSONArray getCpSpResPieData(String date1, String protType, String appType, String appNames, String devid) throws ClassNotFoundException, SQLException, ParseException {

        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        Map<String, List<String>> appNameMap = appNameMap();
        Map<String, String> appTypeMap = appTypeMap();
        Map<String, List<String>> tmpMap = new HashMap<String, List<String>>();
        ReadHBaseCPSPTable rbCpSp = getHBaseTable(ReadHBaseCPSPTable.class);
        if (appType.equals("ALL")) {
            for (String typeId : appTypeMap.keySet()) {
                tmpMap.put(appTypeMap.get(typeId), appNameMap.get(typeId));
            }
            try {
                arr1 = rbCpSp.cpSpResPieSUMHbase(date1, protType, tmpMap, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (appNames.equals("ALL")) {
                tmpMap.put(appTypeMap.get(appType), appNameMap.get(appType));
            } else {
                ArrayList<String> appNamesL = new ArrayList();
                appNamesL.add(appNames);
                tmpMap.put(appTypeMap.get(appType), appNamesL);
            }
            try {
                arr1 = rbCpSp.cpSpResPieHbase(date1, protType, tmpMap, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("DataArr::::::::=" + arr1.toString());
        System.out.println("date1::::::::=" + date1);
        System.out.println("devList::::::::=" + devList.toString());
        return arr1;
    }

    //获取CP/SP资源服务器请求占比 天粒度
    public JSONArray getCpSpResPie_DData(String date1, String date2, String protType, String appType, String appNames, String devid) throws ClassNotFoundException, SQLException {
        System.out.println("获取CP/SP资源服务器请求占比 天粒度:date1=" + date1 + " date2=" + date2 + " protType=" + protType + " appType=" + appType + " sm_AppType=" + appNames + " devid=" + devid);
        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        Map<String, List<String>> appNameMap = appNameMap();
        Map<String, String> appTypeMap = appTypeMap();
        Map<String, List<String>> tmpMap = new HashMap<String, List<String>>();
        ReadHBaseCPSPTable rbCpSp = getHBaseTable(ReadHBaseCPSPTable.class);
        if (appType.equals("ALL")) {
            for (String typeId : appTypeMap.keySet()) {
                tmpMap.put(appTypeMap.get(typeId), appNameMap.get(typeId));
            }
            try {
                arr1 = rbCpSp.cpSpResPieSUMHbase_day(date1, date2, protType, tmpMap, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (appNames.equals("ALL")) {
                tmpMap.put(appTypeMap.get(appType), appNameMap.get(appType));
            } else {
                ArrayList<String> appNamesL = new ArrayList();
                appNamesL.add(appNames);
                tmpMap.put(appTypeMap.get(appType), appNamesL);
            }
            try {
                arr1 = rbCpSp.cpSpResPieHbase_day(date1, date2, protType, tmpMap, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return arr1;
    }

    //获取流量流向分析流量趋势 5分钟粒度
    public JSONArray getTraffDirL2Data(String date, String src, String dst, String appType, String appName, String appTraffic_filed, String devid) throws ClassNotFoundException, SQLException {

        /**
         * 1. date 2. src dst 3. appType 4. appNames list 5. up/down 6. ip
         */
        System.out.println("获取流量流向分析流量趋势 5分钟粒度 getTraffDirL2Data:::::" + date + src + dst + appType + appName + appTraffic_filed + devid);

        JSONArray arr1 = new JSONArray();
        Map<String, List<String>> appNameMap = appNameMap(); //
        Map<String, String> appTypeId = appTypeMap();

        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        ReadHBaseTrafficTable rbTraffic = getHBaseTable(ReadHBaseTrafficTable.class);

        if (appType.equals("ALL")) {
            for (String typeId : appTypeId.keySet()) {
                JSONArray arr_tmp = new JSONArray();
                try {
                    arr_tmp = rbTraffic.traffDirL2_SumHbase(date, src, dst, typeId, appNameMap.get(typeId), appTraffic_filed, devList);
                    System.out.println("getTraffDirL2Data arr_tmp  :::==" + arr_tmp.toString());
                    if (arr_tmp.size() > 0) {
                        arr_tmp.set(0, appTypeId.get(typeId));
                        arr1.add(arr_tmp);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            if (appName.equals("ALL")) {
                try {
                    arr1 = rbTraffic.traffDirL2Hbase(date, src, dst, appType, appNameMap.get(appType), appTraffic_filed, devList);
                } catch (IOException ex) {
                    Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                ArrayList<String> appNames = new ArrayList();
                appNames.add(appName);
                try {
                    arr1 = rbTraffic.traffDirL2Hbase(date, src, dst, appType, appNames, appTraffic_filed, devList);
                } catch (IOException ex) {
                    Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        System.out.println("getTraffDirL2Data arr1:::==" + arr1.toString());
        return arr1;
    }

    //获取流量流向分析流量趋势 1天粒度
    public JSONArray getTraffDirL2_DData(String date1, String date2, String src, String dst, String appType, String appName, String appTraffic_filed, String devid) throws ClassNotFoundException, SQLException {
        /**
         * 1. date1 date2 2. src dst 3. appType 4. appNames list 5. up/down 6.
         * ip
         */
        System.out.println("获取流量流向分析流量趋势 1天粒度:date1=" + date1 + " date2=" + date2 + " src=" + src + " dst=" + dst + " appType=" + appType + " appName=" + appName + " appTraffic_filed=" + appTraffic_filed + " devid=" + devid);
        JSONArray arr1 = new JSONArray();
        Map<String, List<String>> appNameMap = appNameMap(); //
        Map<String, String> appTypeId = appTypeMap();
        for (String s : appTypeId.keySet()) {
            System.err.println(s + " : " + appTypeId.get(s));
        }
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        System.out.println("devList  :::==" + devList);
        ReadHBaseTrafficTable rbTraffic = getHBaseTable(ReadHBaseTrafficTable.class);
        List<String> l = new ArrayList<String>();
        String s = "CC视频,HLS,iptv,iv影音加速器,i土豆,MySee,PPGou,PPLive ,PPStream ,PPS中HTTP流,PPTV中HTTP流,QQLive ,QVoD,RealLink,SopCast,TVKoo ,uusee中http流,VJBASE(西瓜影视),久久影音,优朋影视,优视TV,优酷HTTP流,奇艺HTTP流,奇艺P2P流,奇艺加速器,搜狐电视直播,教育网络电视,暴风盒子,百度影音,皮皮影视,腾讯视频HTTP流,芒果tv中HTTP流,芒果tv中P2P流,蚂蚁电视,迅雷,迅雷看看,非主流P2P视频应用,风行HTTP流,风行播放器,飞速土豆";
        String[] ss = s.split(",");
        for (int i = 0; i < ss.length; i++) {
            l.add(ss[i]);
        }
        if (appType.equals("ALL")) {
            JSONArray arr_tmp = new JSONArray();
            for (String typeId : appTypeId.keySet()) {
                arr_tmp = new JSONArray();
                try {
                    System.out.println("appType ALL traffDirL2Hbase_Day:date1" + date1 + " date2=" + date2 + " src=" + src + " dst=" + dst + " typeId=3 appNameMap.get(typeId)=" + l + " appTraffic_filed=" + appTraffic_filed + " devid=" + devid);
                    arr_tmp = rbTraffic.traffDirL2_SumHbase_Day(date1, date2, src, dst, typeId, appNameMap.get(typeId), appTraffic_filed, devList);
                    System.out.println("getTraffDirL2_DData arr_tmp  :::==" + arr_tmp);
                    if (arr_tmp.size() > 0) {
                        arr_tmp.set(0, typeId);
                        arr1.add(arr_tmp);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            if (appName.equals("ALL")) {
                try {
                    System.out.println("if appName ALL traffDirL2Hbase_Day");
                    arr1 = rbTraffic.traffDirL2Hbase_Day(date1, date2, src, dst, appType, appNameMap.get(appType), appTraffic_filed, devList);
                    System.out.println("if appName.equals(\"ALL\")  :::==" + arr1.toString());
                } catch (IOException ex) {
                    Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                ArrayList<String> appNames = new ArrayList();
                appNames.add(appName);
                try {
                    System.out.println("else appName ALL traffDirL2Hbase_Day");
                    arr1 = rbTraffic.traffDirL2Hbase_Day(date1, date2, src, dst, appType, appNames, appTraffic_filed, devList);
                    System.out.println("else appName.equals(\"ALL\")  :::==" + arr1.toString());
                } catch (IOException ex) {
                    Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        System.out.println("getTraffDirL2_DData arr1:::==" + arr1.toString());
        return arr1;
    }

    //获取流量流向分析各类应用流量占比
    public JSONArray getTraffDirPieData(String date, String src, String dst, String appType, String appName, String appTraffic_filed, String devid) throws ClassNotFoundException, SQLException {

        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        Map<String, List<String>> appNameMap = appNameMap();
        Map<String, String> appTypeMap = appTypeMap();
        Map<String, List<String>> tmpMap = new HashMap<String, List<String>>();
        ReadHBaseTrafficTable rbTraffic = getHBaseTable(ReadHBaseTrafficTable.class);
        if (appType.equals("ALL")) {
            for (String typeId : appTypeMap.keySet()) {
                tmpMap.put(typeId, appNameMap.get(typeId));
            }
            try {
                arr1 = rbTraffic.traffDirPie_SumHbase(date, src, dst, appTypeMap, tmpMap, appTraffic_filed, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                arr1 = rbTraffic.traffDirPieHbase(date, src, dst, appType, appNameMap.get(appType), appTraffic_filed, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("DataArr::::::::=" + arr1.toString());
        System.out.println("date::::::::=" + date);
        System.out.println("devList::::::::=" + devList.toString());
        return arr1;
    }

    //获取流量流向分析各类应用流量占比_按天汇总
    public JSONArray getTraffDirPie_DData(String date1, String date2, String src, String dst, String appType, String appName, String appTraffic_filed, String devid) throws ClassNotFoundException, SQLException {
        System.out.println("date1=" + date1 + " date2=" + date2 + " src=" + src + " dst=" + dst + " appType=" + appType + " appName=" + appName + " appTraffic_filed=" + appTraffic_filed + " devid=" + devid);
        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        Map<String, List<String>> appNameMap = appNameMap();
        Map<String, String> appTypeMap = appTypeMap();
        Map<String, List<String>> tmpMap = new HashMap<String, List<String>>();
        ReadHBaseTrafficTable rbTraffic = getHBaseTable(ReadHBaseTrafficTable.class);
        if (appType.equals("ALL")) {
            for (String typeId : appTypeMap.keySet()) {
                tmpMap.put(typeId, appNameMap.get(typeId));
            }
            try {
                arr1 = rbTraffic.traffDirPie_SumHbase_Day(date1, date2, src, dst, appTypeMap, tmpMap, appTraffic_filed, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                arr1 = rbTraffic.traffDirPieHbase_Day(date1, date2, src, dst, appType, appNameMap.get(appType), appTraffic_filed, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("DataArr::::::::=" + arr1.toString());
        System.out.println("devList::::::::=" + devList.toString());
        return arr1;
    }

    //获取general_flow 总信息
    public JSONArray getGenFlowTotalInfoData() throws ClassNotFoundException, SQLException {

        JSONArray arr1 = new JSONArray();
        ReadHBaseGenTable rbGen = getHBaseTable(ReadHBaseGenTable.class);
        try {
            arr1 = rbGen.genFlowTotalInfoHbase();
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arr1;
    }

    //获取源区域组ID-目的区域组ID
    public JSONArray getSRC_DESTData() throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "select id,areaName from  iprangeinfo ORDER BY id ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString(1));
            arr1_2.add(rs.getString(2));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;

    }

//    //获取源区域组ID-目的区域组ID 按天
//    public JSONArray getSRC_DEST_DData(String date1, String date2) throws ClassNotFoundException, SQLException, ParseException {
//
////        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
////        Date date_1 = sdf.parse(date1);
////        Date date_2 = sdf.parse(date2);
//        super.getConnection();
//        String sql = "select distinct concat(SRC_AREAGROUP_ID,'-',DEST_AREAGROUP_ID) src_dest from  traffdir_ana_day ana  where R_StatDate >= ? and  R_StatDate <= ? ; ";
//        PreparedStatement pst = super.conn.prepareStatement(sql);
//        pst.setString(1, date1);
//        pst.setString(2, date2);
//        ResultSet rs = pst.executeQuery();
//        JSONArray arr1 = new JSONArray();
//        while (rs.next()) {
//            JSONArray arr1_2 = new JSONArray();
//            arr1_2.add(rs.getString("src_dest"));
//            arr1.add(arr1_2);
//        }
//        super.closeResource();
//        return arr1;
//
//    }
    //获取所有DevId
    public JSONArray getDevId() throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "SELECT id ,device_name FROM dpiud_device order by id ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("id"));
            arr1_2.add(rs.getString("device_name"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;
    }

    //获取所有DevId DevName
    private Map<String, String> getDevMap() throws ClassNotFoundException, SQLException {
        super.getConnection();
        String sql = "SELECT id ,device_name FROM dpiud_device ;";
        PreparedStatement pst;
        pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        Map<String, String> appMap = new HashMap<String, String>();
        while (rs.next()) {
            appMap.put(rs.getString(1), rs.getString(2));
        }
        super.closeResource();
        return appMap;
    }

    //获取所有应用类型大类
    public JSONArray getBigAppType() throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "SELECT id ,typeName FROM apptype ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("id"));
            arr1_2.add(rs.getString("typeName"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;
    }

    //获取所有应用类型大类
    private Map<String, String> appTypeMap() throws ClassNotFoundException, SQLException {
//        System.out.println("获取所有应用类型大类");
        super.getConnection();
        String sql = "SELECT id ,typeName FROM apptype ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        Map<String, String> appMap = new HashMap<String, String>();
        while (rs.next()) {
            String id = rs.getString("id");
            String typeName = rs.getString("typeName");
            appMap.put(id, typeName);
//            System.out.println(id + " : " + typeName);
        }
        super.closeResource();
        return appMap;
    }

    //获取某个大类的所有小类
    public JSONArray getSMAppType(String BigType) throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "SELECT id, name FROM appdef WHERE AppType_ID =  ? ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        pst.setString(1, BigType);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getInt("id"));
            arr1_2.add(rs.getString("name"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        return arr1;
    }

    //获取所有应用小类
    private Map<String, List<String>> appNameMap() throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "SELECT AppType_ID ,name FROM appdef ORDER BY AppType_ID ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        Map<String, List<String>> appMap = new HashMap<String, List<String>>();
        while (rs.next()) {
            String appID = rs.getString("AppType_ID");
            if (appMap.containsKey(appID)) {
                List<String> dataList = appMap.get(appID);
                dataList.add(rs.getString("name"));
                appMap.put(appID, dataList);
            } else {
                List<String> dataList2 = new ArrayList<String>();
                dataList2.add(rs.getString("name"));
                appMap.put(appID, dataList2);
            }
        }
        super.closeResource();
        return appMap;
    }

    //获取所有Web类型
    public JSONArray getWebType() throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "SELECT Code ,Name FROM webtype_list ORDER BY Code ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("Code"));
            arr1_2.add(rs.getString("Name"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1::::::::=" + arr1.toString());
        return arr1;
    }

    //获取所有Web类型
    private Map<String, String> getWebTypeMap() throws ClassNotFoundException, SQLException {

        super.getConnection();
        String sql = "SELECT Code,Name FROM webtype_list ORDER BY Code ;";
        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        Map<String, String> appMap = new HashMap<String, String>();
        while (rs.next()) {
            appMap.put(rs.getString(1), rs.getString(2));
        }
        super.closeResource();
        return appMap;
    }

    //获取 应用层攻击类型 Map
    private Map<String, String> appAttTypeMap() throws ClassNotFoundException, SQLException {
        super.getConnection();
        String sql = "select distinct AttackType_Code , AttackType_Name  \n"
                + " from  AttackType_List order by AttackType_Code ;";
        PreparedStatement pst;
        pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        Map<String, String> appMap = new HashMap<String, String>();
        while (rs.next()) {
            appMap.put(rs.getString(1), rs.getString(2));
        }
        super.closeResource();
        return appMap;
    }

    //获取一ToN 综合报告
    public JSONArray getOneToN_GenCht(String date1, String date2, String devid) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_s = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "SELECT date_format(R_StartTime,'%Y-%m-%d') R_StatDate,COUNT(DISTINCT UserAccount) UserNum FROM oneton_det_kf_res  \n"
                + " where ShareTerminal_Num >=2 AND R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_s + "' \n"
                + " group by date_format(R_StartTime,'%Y-%m-%d') order by  R_StatDate ; ";

        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);

        Map<String, Double> dataMap2 = new HashMap<String, Double>();
        while (rs.next()) {
            dataMap2.put(rs.getString("R_StatDate"), rs.getDouble("UserNum"));
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        MapOperate mp = new MapOperate();
        Map<String, Double> fillDataMap = mp.fillMapData(date_1, dayEnd.getTime(), dataMap2);

        JSONArray arr1_2 = new JSONArray();
        arr1_2.add("共享用户趋势图");
        arr1_2.add(mp.getStrByMap(fillDataMap));
        arr1.add(arr1_2);
        System.out.println("arr1::::::::=" + arr1.toString());
        return arr1;
    }

    //获取一ToN 用户数统计
    public JSONArray getOneToN_User(String date1, String date2, String devid) throws ClassNotFoundException, SQLException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_s = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "SELECT '活跃用户数'  stat_item ,   date_format(R_StartTime,'%Y-%m-%d') R_StatDate,COUNT(DISTINCT UserAccount) UserNum FROM oneton_det_kf_res \n"
                + " WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_s + "' group by date_format(R_StartTime,'%Y-%m-%d') \n"
                + " UNION ALL SELECT  '共享用户数'  stat_item , date_format(R_StartTime,'%Y-%m-%d') R_StatDate,COUNT(DISTINCT UserAccount) UserNum FROM oneton_det_kf_res  t2\n"
                + " WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_s + "'AND t2.ShareTerminal_Num>1  group by date_format(R_StartTime,'%Y-%m-%d')  ;";

        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);

        Map<String, Map<String, Double>> flowDataMap = new HashMap<String, Map<String, Double>>();
        while (rs.next()) {
            String AppName = rs.getString("stat_item");
            if (flowDataMap.containsKey(AppName)) {
                Map<String, Double> dataMap = flowDataMap.get(AppName);
                dataMap.put(rs.getString("R_StatDate"), rs.getDouble("UserNum"));
                flowDataMap.put(AppName, dataMap);
            } else {
                Map<String, Double> dataMap2 = new HashMap<String, Double>();
                dataMap2.put(rs.getString("R_StatDate"), rs.getDouble("UserNum"));
                flowDataMap.put(AppName, dataMap2);
            }
        }
        super.closeResource();
        JSONArray arr1 = new JSONArray();
        MapOperate mp = new MapOperate();
        Map<String, Map<String, Double>> fillDataMap = mp.fillMapDataBydate(date_1, dayEnd.getTime(), flowDataMap);
        for (Map.Entry<String, Map<String, Double>> entry : fillDataMap.entrySet()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(entry.getKey());
            arr1_2.add(mp.getStrByMap(entry.getValue()));
            arr1.add(arr1_2);
        }
        System.out.println("arr1::::::::=" + arr1.toString());
        return arr1;
    }

    //获取一ToN 共享分布
    public JSONArray getOneToN_Share(String date1, int minutes, String devid) throws ClassNotFoundException, SQLException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date1);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_s = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "SELECT  Share_Num ,COUNT(UserAccount) UserNum FROM \n"
                + "( SELECT  CASE WHEN  MAX(ShareTerminal_Num) <=5 THEN  MAX(ShareTerminal_Num) WHEN   MAX(ShareTerminal_Num) >5  AND  MAX(ShareTerminal_Num) <=10  THEN '6-10' \n"
                + "               WHEN MAX(ShareTerminal_Num) >10   THEN 'OVER_10'  END Share_Num ,  UserAccount  FROM oneton_det_kf_res  \n"
                + " WHERE R_StartTime >= '" + date1 + "' AND R_EndTime <= '" + dayEnd_s + "' AND ShareTerminal_Num>1 GROUP BY UserAccount)  t2 group by  Share_Num ORDER BY  Share_Num ;";
        if (minutes > 0) {
            sql = "SELECT  Share_Num ,COUNT(UserAccount) UserNum FROM \n"
                    + "( SELECT  CASE WHEN  MAX(ShareTerminal_Num) <=5 THEN  MAX(ShareTerminal_Num) WHEN   MAX(ShareTerminal_Num) >5  AND  MAX(ShareTerminal_Num) <=10  THEN '6-10' \n"
                    + "               WHEN MAX(ShareTerminal_Num) >10   THEN 'OVER_10'  END Share_Num ,  UserAccount  FROM oneton_det_kf_res  \n"
                    + " WHERE R_EndTime > date_add(NOW(), interval -" + minutes + " minute) AND R_EndTime <= NOW() AND ShareTerminal_Num>1 GROUP BY UserAccount)  t2 group by  Share_Num ORDER BY  Share_Num ;";
        }
        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("Share_Num") + "台");
            arr1_2.add(rs.getString("UserNum"));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1::::::::=" + arr1.toString());
        return arr1;
    }

    //获取一ToN 检测日报
    public JSONObject getOneToN_Report(String UserAcc, String UserIP, String ShareTerm_S, String ShareTerm_E,
            String date1, String date2, String devid, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_s = sdf.format(dayEnd.getTime());

        super.getConnection();
        String sql = "SELECT date_format(R_StartTime,'%Y-%m-%d %h:%i:%s') AS R_StartTime, date_format(R_EndTime,'%Y-%m-%d %h:%i:%s') AS R_EndTime , UserAccount , UserIP , ShareTerminal_Num ,\n"
                + " QQidcnt , IPNATcnt, Cookiecnt, Devcnt , OScnt FROM OneToN_Det_KF_Res WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_s + "'";
        String sql1 = "SELECT count(*) AS total_num   FROM OneToN_Det_KF_Res WHERE R_StartTime >= '" + date1 + "' and  R_EndTime <= '" + dayEnd_s + "'";
        if ((UserAcc != null) && !"".equals(UserAcc)) {
            sql = sql + " AND UserAccount LIKE '%" + UserAcc + "%'";
            sql1 = sql1 + " AND UserAccount LIKE '%" + UserAcc + "%'";
        }
        if ((UserIP != null) && !"".equals(UserIP)) {
            sql = sql + " AND UserIP LIKE '%" + UserIP + "%'";
            sql1 = sql1 + " AND UserIP LIKE '%" + UserIP + "%'";
        }
        if ((ShareTerm_S != null) && !"".equals(ShareTerm_S)) {
            int share_s = Integer.parseInt(ShareTerm_S);
            sql = sql + " AND ShareTerminal_Num >=" + share_s;
            sql1 = sql1 + " AND ShareTerminal_Num >=" + share_s;
        }
        if ((ShareTerm_E != null) && !"".equals(ShareTerm_E)) {
            int share_e = Integer.parseInt(ShareTerm_E);
            sql = sql + " AND ShareTerminal_Num <=" + share_e;
            sql1 = sql1 + " AND ShareTerminal_Num <=" + share_e;
        }
        sql = sql + " limit " + iDisplayStart + "," + iDisplayLength;
        //System.out.println("sql1111="+sql);

        PreparedStatement pst = super.conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        JSONArray arr1 = new JSONArray();
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            arr1_2.add(rs.getString("R_StartTime"));
            arr1_2.add(rs.getString("R_EndTime"));
            arr1_2.add(rs.getString("UserAccount"));
            arr1_2.add(rs.getString("UserIP"));
            arr1_2.add(rs.getString("ShareTerminal_Num"));
            arr1_2.add(rs.getString("QQidcnt"));
            arr1_2.add(rs.getString("IPNATcnt"));
            arr1_2.add(rs.getString("Cookiecnt"));
            arr1_2.add(rs.getString("Devcnt"));
            arr1_2.add(rs.getString("OScnt"));
            arr1.add(arr1_2);
        }
        PreparedStatement pst1 = super.conn.prepareStatement(sql1);
        ResultSet rs1 = pst1.executeQuery();
        int total_count = 0;
        while (rs1.next()) {
            total_count = rs1.getInt("total_num");
        }

        JSONObject DataObj = new JSONObject();
        DataObj.put("aaData", arr1);
        DataObj.put("iTotalDisplayRecords", total_count);//当前页
        DataObj.put("iTotalRecords", total_count); //总条数
        System.out.println(DataObj);
        super.closeResource();
        System.out.println("DataObj::::::::=" + DataObj.toString());
        return DataObj;
    }

    //访问指定应用的用户趋势
    public JSONArray getVisitAppUserTrd(String date1, String appType, String appNames, String devid) throws ClassNotFoundException, SQLException, ParseException {
        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        Map<String, List<String>> appNameMap = appNameMap();
        Map<String, String> appTypeMap = appTypeMap();
        Map<String, List<String>> tmpMap = new HashMap<String, List<String>>();
        ReadHBaseVisitAppTable rbVisit = getHBaseTable(ReadHBaseVisitAppTable.class);
        if (appType.equals("ALL")) {
            for (String typeId : appTypeMap.keySet()) {
                tmpMap.put(appTypeMap.get(typeId), appNameMap.get(typeId));
            }
            try {
                arr1 = rbVisit.visitAppUserTrd_SumHbase(date1, tmpMap, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (appNames.equals("ALL")) {
                tmpMap.put(appTypeMap.get(appType), appNameMap.get(appType));
            } else {
                ArrayList<String> appNamesL = new ArrayList();
                appNamesL.add(appNames);
                tmpMap.put(appTypeMap.get(appType), appNamesL);
            }
            try {
                arr1 = rbVisit.VisitAppUserTrdHbase(date1, tmpMap, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("DataArr::::::::=" + arr1.toString());
        System.out.println("date1::::::::=" + date1);
        System.out.println("devList::::::::=" + devList.toString());
        return arr1;
    }

    //访问指定应用的用户趋势 1天粒度
    public JSONArray getVisitAppUserTrd_D(String date1, String date2, String appType, String appNames, String devid) throws ClassNotFoundException, SQLException {

        System.out.println("date1=" + date1 + " date2=" + date2 + " appType=" + appType + " appNames=" + appNames + " devid=" + devid);
        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        Map<String, List<String>> appNameMap = appNameMap();
        Map<String, String> appTypeMap = appTypeMap();
        Map<String, List<String>> tmpMap = new HashMap<String, List<String>>();
        ReadHBaseVisitAppTable rbVisit = getHBaseTable(ReadHBaseVisitAppTable.class);
        if (appType.equals("ALL")) {
            for (String typeId : appTypeMap.keySet()) {
                tmpMap.put(appTypeMap.get(typeId), appNameMap.get(typeId));
            }
            try {
                arr1 = rbVisit.visitAppUserTrd_SumHbase_day(date1, date2, tmpMap, devList);

            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (appNames.equals("ALL")) {
                tmpMap.put(appTypeMap.get(appType), appNameMap.get(appType));
            } else {
                ArrayList<String> appNamesL = new ArrayList();
                appNamesL.add(appNames);
                tmpMap.put(appTypeMap.get(appType), appNamesL);
            }
            try {
                arr1 = rbVisit.VisitAppUserTrdHbase_day(date1, date2, tmpMap, devList);
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("DataArr::::::::=" + arr1.toString());
        System.out.println("date1::::::::=" + date1);
        System.out.println("devList::::::::=" + devList.toString());
        return arr1;
    }

    ////访问指定应用的用户占比
    public JSONArray getVisitAppUserPie(String date1, String date2, String appType, String devid) throws ClassNotFoundException, SQLException, ParseException {

        System.out.println("date1=" + date1 + " date2=" + date2 + " appType=" + appType + " devid=" + devid);
        JSONArray arr1 = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(0));
            }
        } else {
            devList.add(devid);
        }
        Map<String, List<String>> appNameMap = appNameMap();
        Map<String, String> appTypeMap = appTypeMap();
        Map<String, List<String>> tmpMap = new HashMap<String, List<String>>();
        ReadHBaseVisitAppTable rbVisit = getHBaseTable(ReadHBaseVisitAppTable.class);
        if (appType.equals("ALL")) {
            for (String typeId : appTypeMap.keySet()) {
                tmpMap.put(appTypeMap.get(typeId), appNameMap.get(typeId));
            }
            try {
                if (date1.equals(date2)) {
                    arr1 = rbVisit.visitAppUserPie_SUMHbase(date1, tmpMap, devList);
                } else {
                    arr1 = rbVisit.visitAppUserPie_SUMHbase_day(date1, date2, tmpMap, devList);
                }
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            tmpMap.put(appTypeMap.get(appType), appNameMap.get(appType));
            try {
                if (date1.equals(date2)) {
                    arr1 = rbVisit.visitAppUserPieHbase(date1, tmpMap, devList);
                } else {
                    arr1 = rbVisit.visitAppUserPieHbase_day(date1, date2, tmpMap, devList);
                }
            } catch (IOException ex) {
                Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("getVisitAppUserPie DataArr::::::::=" + arr1.toString());
        System.out.println("date1::::::::=" + date1);
        System.out.println("devList::::::::=" + devList.toString());
        return arr1;
    }

    //获取访问指定应用 日志详情
    public JSONObject getVisitAppDetails(String date1, String date2, String appType, String appNames, String devid,
            int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException {
        System.out.println("date1=" + date1 + " date2=" + date2 + " appType=" + appType + " appNames=" + appNames + " devid=" + devid + " iDisplayStart=" + iDisplayStart + " iDisplayLength=" + iDisplayLength);

        JSONObject DataObj = new JSONObject();
        Map<String, String> dpiMap = getDevMap();
        Map<String, String> devMap = new HashMap<String, String>();
        if (devid.equals("ALL")) {
            devMap = dpiMap;
        } else {
            devMap.put(devid, dpiMap.get(devid));
        }
        Map<String, List<String>> appNameMap = appNameMap();
        Map<String, String> appTypeMap = appTypeMap();
        Map<String, List<String>> tmpMap = new HashMap<String, List<String>>();
        ReadHBaseVisitAppTable rbVisit = getHBaseTable(ReadHBaseVisitAppTable.class);
        if (appType.equals("ALL")) {
            for (String typeId : appTypeMap.keySet()) {
                tmpMap.put(appTypeMap.get(typeId), appNameMap.get(typeId));
            }
        } else {
            if (appNames.equals("ALL")) {
                tmpMap.put(appTypeMap.get(appType), appNameMap.get(appType));
            } else {
                ArrayList<String> appNamesL = new ArrayList();
                appNamesL.add(appNames);
                tmpMap.put(appTypeMap.get(appType), appNamesL);
            }
        }
        try {
            if (date1.equals(date2)) {
                DataObj = rbVisit.visitAppLog(date1, date2, tmpMap, devMap, iDisplayStart + 1, iDisplayLength);
            } else {
                DataObj = rbVisit.visitAppLog(date1, date2, tmpMap, devMap, iDisplayStart + 1, iDisplayLength);
            }
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("getIllegalRouteLog DataObj::::::::=" + DataObj.toString());
        System.out.println("date1::::::::=" + date1);
        System.out.println("devMap::::::::=" + devMap.toString());
        return DataObj;
    }

    //获取非法路由 检测日志
    public JSONObject getIllegalRouteLog(String date1, String date2, String nodeIP, String cpVal, String devid,
            int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        System.out.println("date1=" + date1 + " date2=" + date2 + " nodeIP=" + nodeIP + " cpVal=" + cpVal + " devid=" + devid + " iDisplayStart=" + iDisplayStart + " iDisplayLength=" + iDisplayLength);
        JSONObject DataObj = new JSONObject();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devid);
        }
        ReadHBaseIllegalRouteTable rbIllRoute = getHBaseTable(ReadHBaseIllegalRouteTable.class);
        try {
            DataObj = rbIllRoute.illegalRouteLogHbase(date1, date2, nodeIP, cpVal, devList, iDisplayStart + 1, iDisplayLength);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("getIllegalRouteLog DataObj::::::::=" + DataObj.toString());
        System.out.println("date1::::::::=" + date1);
        System.out.println("devList::::::::=" + devList.toString());
        return DataObj;
    }

    // 非法路由检测 流量趋势
    public JSONArray getIllRouteTrd(String date1, String nodeIP, String cpVal, String traffType, String devid) throws ClassNotFoundException, SQLException, ParseException {

        JSONArray DataArr = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devid);
        }
        ReadHBaseIllegalRouteTable rbIllRoute = getHBaseTable(ReadHBaseIllegalRouteTable.class);
        try {
            DataArr = rbIllRoute.illRouteTrdHbase(date1, nodeIP, cpVal, traffType, devList);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("DataObj::::::::=" + DataArr.toString());
        return DataArr;
    }

    //非法路由检测 流量趋势 1天粒度
    public JSONArray getIllRouteTrd_D(String date1, String date2, String nodeIP, String cpVal, String traffType, String devid) throws ClassNotFoundException, SQLException {

        System.out.println("date1=" + date1 + " date2=" + date2 + " nodeIP=" + nodeIP + " cpVal=" + cpVal + " traffType=" + traffType + " devid=" + devid);
        JSONArray DataArr = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devid.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devid);
        }
        ReadHBaseIllegalRouteTable rbIllRoute = getHBaseTable(ReadHBaseIllegalRouteTable.class);
        try {
            DataArr = rbIllRoute.illRouteTrdHbase_day(date1, date2, nodeIP, cpVal, traffType, devList);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("DataObj::::::::=" + DataArr.toString());
        return DataArr;
    }

    //Voip协议产生的总流量趋势
    public JSONArray getVoipFlowTrd(String date1, String userGroup, String mediaProt, String sigProt, String traffSrc, String devName) throws ClassNotFoundException, SQLException {

        JSONArray DataArr = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        ArrayList<String> proList = new ArrayList<String>();
        if (traffSrc.equals("VoIP_Sig_Traffic")) {
            if ("1".equals(sigProt)) {
                proList.add("H.323");
            } else if ("2".equals(sigProt)) {
                proList.add("SIP");
            } else if ("3".equals(sigProt)) {
                proList.add("MGCP");
            } else if ("ALL".equals(sigProt)) {
                proList.add("H.323");
                proList.add("SIP");
                proList.add("MGCP");
            }
        } else {
            if ("1".equals(mediaProt)) {
                proList.add("RTP");
            } else if ("2".equals(mediaProt)) {
                proList.add("其它");
            } else if ("ALL".equals(mediaProt)) {
                proList.add("RTP");
                proList.add("其它");
            }
        }
        ReadHBaseVoipTable rbVoip = getHBaseTable(ReadHBaseVoipTable.class);
        try {
            DataArr = rbVoip.voipFlowTrdHbase(date1, userGroup, proList, devList);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("DataArr::::::::=" + DataArr.toString());
        System.out.println("date1::::::::=" + date1);
        System.out.println("userGroup::::::::=" + userGroup.toString());
        System.out.println("proList::::::::=" + proList.toString());
        System.out.println("devList::::::::=" + devList.toString());
        return DataArr;
    }

    //Voip协议产生的总流量趋势(粒度：天)
    public JSONArray getVoipFlowTrd_D(String date1, String date2, String userGroup, String mediaProt, String sigProt, String traffSrc, String devName) throws ClassNotFoundException, SQLException {

        System.out.println("date1=" + date1 + " date2=" + date2 + " userGrop=" + userGroup + " mediaProt=" + mediaProt + " sigProt=" + sigProt + " mediaProt=" + mediaProt + " traffSrc=" + traffSrc + " devName=" + devName);
        JSONArray DataArr = new JSONArray();
        ArrayList<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        ArrayList<String> proList = new ArrayList<String>();
        if (traffSrc.equals("VoIP_Sig_Traffic")) {
            if ("1".equals(sigProt)) {
                proList.add("H.323");
            } else if ("2".equals(sigProt)) {
                proList.add("SIP");
            } else if ("3".equals(sigProt)) {
                proList.add("MGCP");
            } else if ("ALL".equals(sigProt)) {
                proList.add("H.323");
                proList.add("SIP");
                proList.add("MGCP");
            }
        } else {
            if ("1".equals(mediaProt)) {
                proList.add("RTP");
            } else if ("2".equals(mediaProt)) {
                proList.add("其它");
            } else if ("ALL".equals(mediaProt)) {
                proList.add("RTP");
                proList.add("其它");
            }
        }
        ReadHBaseVoipTable rbVoip = getHBaseTable(ReadHBaseVoipTable.class);
        try {
            DataArr = rbVoip.voipFlowTrdHbase_day(date1, date2, userGroup, proList, devList);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("DataArr::::::::=" + DataArr.toString());
        System.out.println("date1::::::::=" + date1);
        System.out.println("userGroup::::::::=" + userGroup.toString());
        System.out.println("proList::::::::=" + proList.toString());
        System.out.println("devList::::::::=" + devList.toString());
        return DataArr;
    }

    //Voip协议产生的总流量占比
    public JSONArray getVoipFlowPie(String date1, String date2, String userGroup, String traffSrc, String gwIP, String gwKeeperIP, String devName) throws ClassNotFoundException, SQLException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date_1 = sdf.parse(date1);
        Date date_2 = sdf.parse(date2);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(date_2);
        dayEnd.add(dayEnd.DATE, 1);
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        dayEnd.set(Calendar.SECOND, 0);
        String dayEnd_S = sdf.format(dayEnd.getTime());
        super.getConnection();

        String pro_type = "VoIP_Media_Protocol";
        if (traffSrc.equals("VoIP_Sig_Traffic")) {
            pro_type = "VoIP_Sig_Protocol";
        }
        String sql = "SELECT " + pro_type + " , SUM(VoIP_Media_Traffic) VoIP_Media_Traffic,SUM(VoIP_Sig_Traffic) VoIP_Sig_Traffic "
                + "FROM voip_flow_stat voip LEFT JOIN dpiendpointbean dpi on dpi.masterIp = voip.EquipIp  WHERE date_format(R_StartTime,'%Y/%m/%d') >='" + date1 + "' AND R_EndTime <='" + dayEnd_S + "' AND UserGroupNo ='" + userGroup + "' ";
        if ((gwIP != null) && !"".equals(gwIP)) {
            sql = sql + " AND VoIPGW_IP LIKE '%" + gwIP + "%'";
        }
        if ((gwKeeperIP != null) && !"".equals(gwKeeperIP)) {
            sql = sql + " AND VoIPGWKeeper_IP LIKE '%" + gwKeeperIP + "%'";
        }
        if (!"ALL".equals(devName)) {
            sql = sql + " AND dpi.name = '" + devName + "'";
        }
        sql = sql + "GROUP BY " + pro_type + ";";

        System.out.println(sql);
        Statement s = super.conn.createStatement();
        ResultSet rs = s.executeQuery(sql);
        JSONArray arr1 = new JSONArray();
        String proValue = "";
        while (rs.next()) {
            JSONArray arr1_2 = new JSONArray();
            String proNumericValue = rs.getString(pro_type);
            if (traffSrc.equals("VoIP_Sig_Traffic")) {
                if ("1".equals(proNumericValue)) {
                    proValue = "H.323";
                } else if ("2".equals(proNumericValue)) {
                    proValue = "SIP";
                } else if ("3".equals(proNumericValue)) {
                    proValue = "MGCP";
                } else {
                    proValue = proNumericValue;
                }
                proValue = proValue + "信令协议";
            } else {
                if ("1".equals(proNumericValue)) {
                    proValue = "RTP";
                } else if ("2".equals(proNumericValue)) {
                    proValue = "其它";
                } else {
                    proValue = proNumericValue;
                }
                proValue = proValue + "媒体协议";
            }
            arr1_2.add(proValue);
            arr1_2.add(rs.getDouble(traffSrc));
            arr1.add(arr1_2);
        }
        super.closeResource();
        System.out.println("arr1::::::::=" + arr1.toString());
        return arr1;
    }

    //获取Voip类日志详情
    public JSONObject getVoipFlowLog(String date1, String date2, String userGroup, String devName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {
//        ReadHBaseVoipTable rbv = new ReadHBaseVoipTable("10.10.2.41", "60000",
//                "10.10.2.41", "2181");
        ReadHBaseVoipTable rbv = new ReadHBaseVoipTable();
        JSONObject DataObj = new JSONObject();
        ArrayList<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        Map<String, String> userMap = new HashMap<String, String>();
        Map<String, String> userGpMap = userGroupMap();
        userMap.put(userGroup, userGpMap.get(userGroup));
        try {
            System.out.println("start search");
            DataObj = rbv.voipFlowLogHbase_day(date1, date2, userMap, devList, iDisplayStart + 1, iDisplayLength);
//            System.out.println("usermapsize:"+userMap.size());
//            for(Map.Entry<String,String> e : userMap.entrySet()){
//                System.out.println(e.getKey()+e.getValue());
//            }
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("getVoipFlowLog userMap::::" + userMap.toString());
        System.out.println("devList::::" + devList.toString());
        System.out.println("date1::::" + date1);
        System.out.println("date2::::" + date2);
        System.out.println("iDisplayStart::::" + iDisplayStart);
        System.out.println("iDisplayLength::::" + iDisplayLength);
        System.out.println(DataObj);
        return DataObj;
    }

    //获取Voip类网关日志详情
    public JSONObject getVoipGWLog(String date1, String date2, String userGroup, String devName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {
        JSONObject DataObj = new JSONObject();
        ArrayList<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        Map<String, String> userMap = new HashMap<String, String>();
        Map<String, String> userGpMap = userGroupMap();
        userMap.put(userGroup, userGpMap.get(userGroup));
        ReadHBaseVoipTable rbVoip = getHBaseTable(ReadHBaseVoipTable.class);
        try {
            DataObj = rbVoip.voipGWLogHbase_day(date1, date2, userMap, devList, iDisplayStart + 1, iDisplayLength);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("getVoipGWLog userMap::::" + userMap.toString());
        System.out.println("devList::::" + devList.toString());
        System.out.println("date1::::" + date1);
        System.out.println("date2::::" + date2);
        System.out.println("iDisplayStart::::" + iDisplayStart);
        System.out.println("iDisplayLength::::" + iDisplayLength);
        System.out.println(DataObj);
        return DataObj;
    }

    //获取Voip类网守被叫日志详情
    public JSONObject getVoipCalledLog(String date1, String date2, String userGroup, String devName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {
        JSONObject DataObj = new JSONObject();
        ArrayList<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        Map<String, String> userMap = new HashMap<String, String>();
        Map<String, String> userGpMap = userGroupMap();
        userMap.put(userGroup, userGpMap.get(userGroup));
        ReadHBaseVoipTable rbVoip = getHBaseTable(ReadHBaseVoipTable.class);
        try {
            DataObj = rbVoip.voipCalledLogHbase_day(date1, date2, userMap, devList, iDisplayStart + 1, iDisplayLength);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("getVoipCalledLog userMap::::" + userMap.toString());
        System.out.println("devList::::" + devList.toString());
        System.out.println("date1::::" + date1);
        System.out.println("date2::::" + date2);
        System.out.println("iDisplayStart::::" + iDisplayStart);
        System.out.println("iDisplayLength::::" + iDisplayLength);
        System.out.println(DataObj);
        return DataObj;
    }

    //获取Voip类网守主叫日志详情
    public JSONObject getVoipCallerLog(String date1, String date2, String userGroup, String devName, int iDisplayStart, int iDisplayLength) throws ClassNotFoundException, SQLException, ParseException {

        JSONObject DataObj = new JSONObject();
        ArrayList<String> devList = new ArrayList<String>();
        if (devName.equals("ALL")) {
            JSONArray devArr1 = getDevId();
            for (int i = 0; i < devArr1.size(); i++) {
                devList.add(devArr1.getJSONArray(i).getString(1));
            }
        } else {
            devList.add(devName);
        }
        Map<String, String> userMap = new HashMap<String, String>();
        Map<String, String> userGpMap = userGroupMap();
        userMap.put(userGroup, userGpMap.get(userGroup));
        ReadHBaseVoipTable rbVoip = getHBaseTable(ReadHBaseVoipTable.class);
        try {
            DataObj = rbVoip.voipCallerLogHbase_day(date1, date2, userMap, devList, iDisplayStart + 1, iDisplayLength);
        } catch (IOException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("getVoipCallerLog userMap::::" + userMap.toString());
        System.out.println("devList::::" + devList.toString());
        System.out.println("date1::::" + date1);
        System.out.println("date2::::" + date2);
        System.out.println("iDisplayStart::::" + iDisplayStart);
        System.out.println("iDisplayLength::::" + iDisplayLength);
        System.out.println(DataObj);
        return DataObj;
    }

    public static void main(String[] agrs) {

//        ReadHBaseVoipTable rbv = new ReadHBaseVoipTable("10.10.2.41", "60000",
//                "10.10.2.41", "2181");
        ReadHBaseVoipTable rbv = new ReadHBaseVoipTable();
        Map<String, String> userMap = new HashMap<String, String>();
        userMap.put("0", "哈哈哈哈");
        List<String> devList = new ArrayList<String>();
        devList.add("测试");
        devList.add("ooo");
        List<String> protList = new ArrayList<String>();
        protList.add("RTP");
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject1 = new JSONObject();
        JSONObject jsonObject3 = new JSONObject();
        try {

            jsonObject1 = rbv.voipGWLogHbase_day("2014/11/18", "2014/11/18",
                    userMap, devList, 0, 10);
            jsonObject = rbv.voipFlowLogHbase_day("2014/11/18", "2014/11/18",
                    userMap, devList, 0, 10);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        DataService ds = new DataService();

        try {
            jsonObject3 = ds.getVoipFlowLog("2014/11/18", "2014/11/18", "0", "ALL", 0, 10);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(DataService.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("****jsonObject****");
        System.out.println(jsonObject.toString());
        System.out.println("****jsonObject1****");
        System.out.println(jsonObject1.toString());
        System.out.println("****jsonObject3****");
        System.out.println(jsonObject3.toString());
    }
}
