/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cn.net.greenet.illegalroute.servlet;

import cn.net.greenet.service.DataService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;

@WebServlet(name = "IllegalRouteLogServlet", urlPatterns = {"/IllegalRoute/IllegalRouteLog"})
public class IllegalRouteLogServlet extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");
//        String nodeIP = new String(Base64.decodeBase64(request.getParameter("nodeIP")),"utf-8");
//        String cpVal = new String(Base64.decodeBase64(request.getParameter("cpVal")),"utf-8");
        Base64 base64 = new Base64();
        byte[] nodeBytes = base64.decode(request.getParameter("nodeIP"));
        String nodeIP = new String(nodeBytes, "UTF-8");
        byte[] cpBytes = base64.decode(request.getParameter("cpVal"));
        String cpVal = new String(cpBytes, "UTF-8");
        
        byte[] devBytes = base64.decode(request.getParameter("devid"));
        String devid = new String(devBytes, "UTF-8");
        String iDisplayStart = request.getParameter("iDisplayStart");
        String iDisplayLength = request.getParameter("iDisplayLength");
        
    System.out.println("cpVal::::"+cpVal);
    System.out.println("devid::::"+devid);
    System.out.println("nodeIP::::"+nodeIP);
        try {
            // 链接数据库获取数据
            JSONObject serial = new DataService().getIllegalRouteLog(date1,date2,nodeIP,cpVal,devid,Integer.parseInt(iDisplayStart),Integer.parseInt(iDisplayLength));
            out.println(serial);
        } catch (Exception ex) {
            Logger.getLogger(IllegalRouteLogServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
