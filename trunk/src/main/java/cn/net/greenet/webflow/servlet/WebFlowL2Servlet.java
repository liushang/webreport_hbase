/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cn.net.greenet.webflow.servlet;

import cn.net.greenet.service.DataService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "WebFlowL2Servlet", urlPatterns = {"/WebFlow/WebFlowL2"})
public class WebFlowL2Servlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        String date1 = request.getParameter("date1");
        String UserGNo = request.getParameter("UserGNo");
        String AppType = request.getParameter("AppType");
        String AppTraff_FD = request.getParameter("AppTraff_FD");
        String devid = request.getParameter("devid");
        
        //System.out.println("date1="+date1+ "UserGNo="+UserGNo + "AppType="+AppType+ "AppTraff_FD="+AppTraff_FD);     
        
        if ((UserGNo != null) && ( date1 !=null ) && ( AppType !=null ) &&( AppTraff_FD !=null )) {
            try {
                // 链接数据库获取数据
                JSONArray serial = new DataService().getWebFlowL2Data(date1,UserGNo,AppType,AppTraff_FD,devid);
                out.println(serial);
            } catch (Exception ex) {
                Logger.getLogger(WebFlowL2Servlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
            }
        } else {
            try {
                // 链接数据库获取数据
                JSONArray serial = new JSONArray();
                out.println(serial);
            } catch (Exception ex) {
                Logger.getLogger(WebFlowL2Servlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
