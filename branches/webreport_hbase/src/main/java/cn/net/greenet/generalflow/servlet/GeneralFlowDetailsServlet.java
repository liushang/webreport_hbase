/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.net.greenet.generalflow.servlet;

import cn.net.greenet.service.DataService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Ambimmort
 */
@WebServlet(name = "GeneralFlowDetailsServlet", urlPatterns = {"/GeneralFlow/generalflowdetails"})
public class GeneralFlowDetailsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String PUserGrougNo = request.getParameter("PUserGrougNo");
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");
 
        String appType = request.getParameter("appType");
        
        Base64 base64 = new Base64();
        byte[] devBytes = base64.decode(request.getParameter("devName"));
        String devName = new String(devBytes, "UTF-8");
        byte[] sm_Bytes = base64.decode(request.getParameter("sm_AppType"));
        String sm_AppType = new String(sm_Bytes, "UTF-8");
        
//        String sm_AppType = new String(Base64.decodeBase64(request.getParameter("sm_AppType")),"utf-8");
//        String devName = new String(Base64.decodeBase64(request.getParameter("devName")),"utf-8");
        
        String iDisplayStart = request.getParameter("iDisplayStart");
        String iDisplayLength = request.getParameter("iDisplayLength");
        System.out.println("appType::::"+appType);
        System.out.println("sm_AppType::::"+sm_AppType);
        System.out.println("devName::::"+devName);  
        
        System.out.println("request appType::::"+request.getParameter("appType"));
        System.out.println("request sm_AppType::::"+request.getParameter("sm_AppType"));
        System.out.println("request devName::::"+request.getParameter("devName"));

        if ((PUserGrougNo != null) && ( date1 !=null ) && ( date2 !=null )) {
            try {
                // 链接数据库获取数据
                JSONObject serial = new DataService().getGeneralFlowDetailsData(PUserGrougNo,date1,date2,appType,sm_AppType,devName,Integer.parseInt(iDisplayStart),Integer.parseInt(iDisplayLength));           
                out.println(serial);
            } catch (Exception ex) {
                Logger.getLogger(GeneralFlowDetailsServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
            }
        } else {
            try {
                // 链接数据库获取数据
                JSONObject serial = new JSONObject();
                out.println(serial);
            } catch (Exception ex) {
                Logger.getLogger(GeneralFlowDetailsServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
