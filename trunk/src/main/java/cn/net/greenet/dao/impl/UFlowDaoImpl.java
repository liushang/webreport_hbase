package cn.net.greenet.dao.impl;

import cn.net.greenet.dao.BaseDao;
import cn.net.greenet.dao.UFlowDao;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.PreparedStatement;




public class UFlowDaoImpl extends BaseDao implements UFlowDao  {
        

        @Override
        public boolean addWebFlowTran(BufferedReader bfr) {
		try {
			
			long count = 0;
			String sql = "INSERT INTO web_flow_stat (R_StartTime,R_EndTime,UserGroupNo,Site_Num_Hit,SiteNameLength,SiteName,SiteType,SiteHitFreq,"
                                + "SiteTraffic_Up,SiteTraffic_Dn) VALUES(?,?,?,?,?, ?,?,?,?,?)";
			this.getConnection();
			PreparedStatement psts = this.conn.prepareStatement(sql);
			String line = null;
			while (null != (line = bfr.readLine())) {  
                                String[] infos = line.replace("\t", ",").split(",");  
                                if (infos.length < 6)  break;  
                                int host_time = infos[0].lastIndexOf("_");
                                int host_time_l = infos[0].length() ;
                                psts.setLong(1, count);  
                                psts.setString(2, infos[0].substring(0,host_time));  
                                psts.setString(3, infos[0].substring(host_time+1,host_time_l).replace("|", " ").concat(":00:00"));  
                                psts.setLong(4, Long.parseLong(infos[1]));
                                psts.setLong(5, Long.parseLong(infos[2])); 
                                psts.setLong(6, Long.parseLong(infos[3])); 
                                psts.setString(7, infos[4]); 
                                psts.setString(8, infos[5]);
                                psts.addBatch();
				count++;
				if(count%1000==0){
					psts.executeBatch();
					System.out.println("count=========="+count);
				}
			}
			
			psts.executeBatch();
			
		} catch (Exception e) {
			return false;
		} finally {
			// 释放资源
			this.closeResource();
		}
		return true;
	}
	
        @Override
	public boolean addGeneralFlowTran(BufferedReader bfr) {
		try {
			
			long count = 0;
			String sql = "INSERT INTO general_flow_stat(R_StartTime,R_EndTime,UserGroupNo,AppType,AppID,AppNameLength,AppName,AppUserNum,AppTraffic_Up,AppTraffic_Dn,"
                                + "AppPacketsNum,AppSessionsNum,AppNewSessionNum) VALUES(?,?,?,?,?, ?,?,?,?,?, ?,?,?)";
			this.getConnection();
			PreparedStatement psts = this.conn.prepareStatement(sql);
			String line = null;
			while (null != (line = bfr.readLine())) {  
                                String[] infos = line.replace("\t", ",").split(",");  
                                if (infos.length < 6)  break;  
                                int host_time = infos[0].lastIndexOf("_");
                                int host_time_l = infos[0].length() ;
                                psts.setLong(1, count);  
                                psts.setString(2, infos[0].substring(0,host_time));  
                                psts.setString(3, infos[0].substring(host_time+1,host_time_l).replace("|", " ").concat(":00:00"));  
                                psts.setLong(4, Long.parseLong(infos[1]));
                                psts.setLong(5, Long.parseLong(infos[2])); 
                                psts.setLong(6, Long.parseLong(infos[3])); 
                                psts.setString(7, infos[4]); 
                                psts.setString(8, infos[5]);
                                psts.addBatch();
				count++;
				if(count%1000==0){
					psts.executeBatch();
					System.out.println("count=========="+count);
				}
			}
			
			psts.executeBatch();
			
		} catch (Exception e) {
			return false;
		} finally {
			// 释放资源
			this.closeResource();
		}
		return true;
	}
	
	
	
//	// 测试
	public static void main(String[] args) {

		UFlowDao flowDao = new UFlowDaoImpl();
		
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(new File("file_"+1)));
			flowDao.addGeneralFlowTran(reader);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
